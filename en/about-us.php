<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Best Technologies">
    <meta name="author" content="">
    <title>About Us | InterlinkTelecom</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
     <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
 
</head><!--/head-->

<body>

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->
    
<!-- mody 11-12-2014 --> 
 
	<section id="aboutus">
        <div class="container">	
			<div class="row">
			   
				<div class="col-xs-12 col-sm-6 wow fadeInDown">
					<h2>About Interlink Telecom</h2>
					<p class="lead"   align="justify">
						Interlink Telecom Public Company Limited is a wholly owned subsidiary of Interlink Communications Public Company Limited.
						  Since its establishment in 2007, Interlink Telecom Public Company Limited has many track records on building and operating fiber optic networks, 
						  for instance, TOT, CAT, PEA and MEA as well as receiving the approval for the award of the Domestic and International 
						  Telecom Network Provider Type III License. Interlink Telecom Public Company Limited has fully committed to build a fiber optical network for both 
						  Domestic and International Transmission service to better serve our customers’ needs with the vision of Best connectivity, 
						  Best Service and Best Price.</p>
					
				</div><!--/.col-xs-12 -->
				<!--
				<div class="col-xs-12 col-sm-6 wow fadeInDown"
					onclick="thevid=document.getElementById('thevideo'); 
					thevid.style.display='block'; this.style.display='none'">
					<img style="cursor: pointer; " src="images/client3.png" alt="" align="right" />
				</div>
				
				<div id="thevideo" style="display: none;">-->
					<div class="col-xs-12 col-sm-6 wow fadeInDown" >
						<h2><br></h2>
						<iframe width="400" height="269" align="right" 
						src="//www.youtube.com/embed/2fizuamYads?rel=0&autohide=1&showinfo=0"" frameborder=0
allowfullscreen="true" ></iframe>
					</div><!--/.col-xs-12 col-sm-4-->
					
					<div class="col-xs-12 col-sm-12 wow fadeInDown" >
						<h2>Our Fiber Optical Network</h2>
					<p class="lead" align="justify" >
						Today, Interlink Telecom  Public Company Limited owns and operates nationwide Core Network and the fully 
						fiber optical network in Thailand with the state of the art mixing between expertise in fiber optical cable to 
						deploy all armor fiber optic cable as well as transmission and switching technology with the latest IP network 
						equipment’s.  Riding on the advanced fiber-optic technology, MPLS, SDH and DWDM, core backbone via Railway 
						combining with redundancy via Highway route, customers are able to enjoy its leading-edge services such as 
						Redundancy, Ring Topology access, corporate data transmission solutions, International Private Leased Circuit 
						Services, Internet Protocol Transit Services, MPLS-based IP-VPN Service with total quality control and management. </p>
					<p class="lead" align="justify" >In addition, Interlink Telecom  Public Company Limited also provides nationwide 
					services with 100% asset ownership as well as the operation and maintenance teams which can guarantee on 
					service and mean time to restore the service in time in order to maintain customer satisfaction and best quality 
					as commit at the first stage.</p>
					</div><!--/.col-xs-12 col-sm-4-->
					<!--
				</div>-->
				
         </div><!--/.row-->   
		</div><!--/.container-->
	</section><!--/#aboutus-->

<div class="container">	       
 <div class="row">
                <div class="features">

                <div class="col-sm-12" align="middle">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-center">
                            <img class="img-responsive" src="images/aboutus/qualityNetwork.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Quality Network Provider</span></h3>
                            <p align="center">Interlink Telecom is a network provider with Telecoms Licenses Type III.</p>
                        </div>
					</div>
                </div><!--/.col-sm-12-->

                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon3.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Technologies</span></h3>
                            <p align="justify">Interlink Telecom uses Best in class Technologies, MPLS and DWDM to provide Best in class services.</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->
                
                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/lastMile.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Last Mile</span></h3>
                            <p align="justify">Interlink Telecom provides Fiber Optic Last mile for all types of bandwidth.</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->
                
                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon2.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Fiber Optic Routing</span></h3>
                            <p align="justify">Provides services on “Interlink Fiber Optic Network” which obtain EXCLUSIVE right of way from State Railway of Thailand.</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->
                
                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon6.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Support</span></h3>
                            <p align="justify">Interlink Telecom operates Nationwide Network with OMCs to service customer 24X7</p>
                            <p><br></p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->

                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon7.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best SLA</span></h3>
                            <p align="justify">Interlink support customer under the service level agreement to recover network failure within or less than 4 hours standard nationwide to guarantee best customer experience.</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->

                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon1.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Maintenace Center</span></h3>
                            <p align="justify"> Interlink provide 28 Operation and Maintenance Centers 
                            nationwide to guarantee on service and ready to support customer whenever its’ need.</p>
                            <p><br></p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->

                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon4.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Product Offering</span></h3>
                            <p align="justify">Interlink Telecom provides various types of services and Customize to match customer needs.</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->

                 <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/bestVision.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Vision</span></h3>
                            <p>Interlink Telecom Provides The Best Quality Connectivity, Best Customer Services and Best Price</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6--> 
                                                                         
                </div><!--/.features-->
            </div><!--/.row--> 			
			</div><!--section-->
</div><!--/.container-->
	
   
<!-- include footer.php -->
		<?php	
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/footer.php";
		include_once($path) 
	?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
