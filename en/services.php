<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premier Fiber Optical Network">
    <meta name="author" content="">
    <title>Services | InterlinkTelecom</title>
    
    <!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
 
<!-- script scrollToTop --> 
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
<script>
$(function(){
	$(document).on( 'scroll', function(){
 
		if ($(window).scrollTop() > 100) {
			$('.scroll-top-wrapper').addClass('show');
		} else {
			$('.scroll-top-wrapper').removeClass('show');
		}
	});
 
	$('.scroll-top-wrapper').on('click', scrollToTop);
});
 
function scrollToTop() {
	verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	element = $('body');
	offset = element.offset();
	offsetTop = offset.top;
	$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}
</script>
<!-- /script scrollToTop -->    
 
  <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
    
</head><!--/head-->

<body>
	
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->

  <section id="service_top">
        <div class="container">
            <div class="row">
								
                <div class="col-sm-6 wow fadeInDown" data-wow-duration="300ms" data-wow-delay="300ms">
                    <div class="center">
						<a href="#domestic" ><img src="images/service/domestic.png" class="img-responsive center-block" alt=""></a>
                        <h2><span class="orangetext">D</span>omestic</h2>
					</div><!--/.center-->
                </div><!--/.col-sm-6-->

                <div class="col-sm-6 wow fadeInDown" data-wow-duration="300ms" data-wow-delay="300ms">
                    <div class="center">
						<a href="#internatinal" ><img src="images/service/international.png" class="img-responsive center-block" alt=""></a>
                        <h2><span class="orangetext">I</span>nternational</h2>
                         
                    </div><!--/.center-->
                </div><!--/.col-sm-6-->

            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#service_top-->


    <section id="domestic" class="transparent-bg">
        <div class="container">
			
           <div class="center wow fadeInDown" >
                <h2><span class="orangetext">Domestic Services</span></h2>
                <p class="lead" align="justify">Interlink Telecom offers its Domestic data communication service to customer that, 
                enabling reliable, high performance connectivity and one of the best customer service for excellent customer experience. 
                Apart from that Interlink Telecom also offers a differentiate solution by fully diversified fiber optic routing via Railway route, 
                also as to cater for your future expansions and achieve cost savings in the future
                </p>
            </div><!--/.center wow-->

            <div class="row">
                <div class="features">
				 <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="clients-comments text-center">
						<a href="whatwedo_domestic.php" ><h4><span>Interlink MPLS IP-VPN</span></h4></a>
                        <a href="whatwedo_domestic.php" ><img src="images/client1.png" class="img-circle" alt=""></a>
                        <h3>Increase your Network Performance and Efficiencies with Interlink MPLS IP-VPN</h3>
                        
                    </div>
                </div><!--/.col-md-4-->
                
                 <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="700ms">
                    <div class="clients-comments text-center">
						<a href="whatwedo_domestic.php" ><h4><span>Interlink Wavelength</span></h4></a>
                        <a href="whatwedo_domestic.php" ><img src="images/client3.png" class="img-circle" alt=""></a>
                        <h3>Securely Transmit “Big Bandwidth” to Ensure Maximum Performance for Low Latency</h3>
                        
                    </div>
                </div><!--/.col-md-4-->
                
                 <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="800ms">
                    <div class="clients-comments text-center">
						<a href="whatwedo_domestic.php" ><h4><span>Interlink Dark Fiber</span></h4></a>
                        <a href="whatwedo_domestic.php" ><img src="images/client2.png" class="img-circle" alt=""></a>
                        <h3>Fully Control and Flexibility with Unlimited on the Technology and Bandwidth on Demand</h3>
                        
                    </div>
                </div><!--/.col-md-4-->
                                
                </div><!--/.features-->
            </div><!--/.row--> 
            
        </div><!--/.container-->
    </section><!--/#domestic-->

            
    <section id="internatinal" class="transparent-bg">
        <div class="container">
			
            <div class="clients-area center wow fadeInDown">
                <h2><span class="orangetext">International Services</span></h2>
                <p class="lead" align="justify">Interlink Telecom offers International data communication service to customer by connecting the Fiber Optic Network to international carrier providers in order to extend the connectivity to the world.</p>
            </div>

            <div class="row">
				<div class="col-md-6 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="clients-comments text-center">
						<a href="whatwedo_international.php" ><h4><span>Interlink IPLC</span></h4></a>
                        <a href="whatwedo_international.php" ><img src="images/client4.png" class="img-circle" alt=""></a>
                        <h3>Connected to Any Location on Request with Maximum Service Availability</h3>
                        
                    </div>
                </div><!--/.col-md-4-->
				<div class="col-md-6 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <div class="clients-comments text-center">
						<a href="whatwedo_international.php" ><h4><span>Interlink Wavelength</span></h4></a>
                        <a href="whatwedo_international.php" ><img src="images/client3.png" class="img-circle" alt=""></a>
                        <h3>A single point of contact for An International transmission access service</h3>
                        
                    </div>
                </div><!--/.col-md-4-->
                
           </div><!--/.row--> 
        </div><!--/.container-->
    </section><!--/#internatinal-->

 <!-- include footer.php -->
		<?php	
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/footer.php";
		include_once($path) 
	?>
<!--/end  php -->
    
 <!-- 14-12-17 -->  
<!--.scroll-top-wrapper-->
<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<i class="fa fa-2x fa-arrow-circle-up"></i>
	</span>
</div>
<!--/.scroll-top-wrapper-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
