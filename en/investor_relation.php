<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Investor Relation | InterlinkTelecom</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
    <!-- TimeLine -->
	<script src="js/modernizr.js"></script> <!-- Modernizr -->
	<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:700,900|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>
 
	   
    <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic-->
 
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
		
<script>
$(function(){
	$(document).on( 'scroll', function(){
 
		if ($(window).scrollTop() > 100) {
			$('.scroll-top-wrapper').addClass('show');
		} else {
			$('.scroll-top-wrapper').removeClass('show');
		}
	});
 
	$('.scroll-top-wrapper').on('click', scrollToTop);
});
 
function scrollToTop() {
	verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	element = $('body');
	offset = element.offset();
	offsetTop = offset.top;
	$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}
</script>
<!-- /script scrollToTop -->	
	 
</head><!--/head-->

<body>
	
	<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/header.php";
		include_once($path) ; 
		?>
	<!--/end  php -->

	<!--    <section id="ir_home">
        <div class="container">
			<div class="center wow fadeInDown">
				<h2>Invertor Relation</h2>
			</div>
<!--
            <div class="row">
                <div class="features">
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <a href="#" class="scroll-link" data-id="history"><i class="fa fa-bullhorn"></i></a>
                            <h2>Corporate Info</h2>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                        </div>
                    </div><!--/.col-md-4-->
<!--
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <a href="#" class="scroll-link" data-id="financial"><i class="fa fa-signal"></i></a>
                            <h2>Financial / Operation</h2>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                        </div>
                    </div><!--/.col-md-4-->
<!--
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <a href="#" class="scroll-link" data-id="governance"><i class="fa fa-star"></i></a>
                            <h2>Coporate Governance</h2>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                        </div>
                    </div><!--/.col-md-4-->
                
<!--                </div><!--/.features-->
<!--            </div><!--/.row--> 
            
<!--			<div class="row">
                <div class="features">
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <a href="#" class="scroll-link" data-id="shareholder"><i class="fa fa-lock"></i></a>
                            <h2>Shareholder</h2>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                        </div>
                    </div><!--/.col-md-4-->
<!--
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <a href="#" class="scroll-link" data-id="share_price"><i class="fa fa-btc"></i></a>
                            <h2>Share Price</h2>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                        </div>
                    </div><!--/.col-md-4-->
<!--
                    <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <a href="#" class="scroll-link" data-id="history"><i class="fa fa-cloud-download"></i></a>
                            <h2>Glossary</h2>
                            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                        </div>
                    </div><!--/.col-md-4-->
 <!--               
                </div><!--/.features-->
<!--            </div><!--/.row--> 
	<!--		</div><!--/.container-->
 	<!--   </section><!--/ir_home-->
  <!-- Section-Bar- -->
 						
	<section class="cd-horizontal-timeline  wow fadeInDown">
	<div class="timeline">
		<div class="events-wrapper">
			<div class="events">
				<ol>
					<li><a href="#0" data-date="01/01/2007" class="selected">2007</a></li>
					<li ><a href="#0" data-date="01/01/2012">2012</a></li>
					<li><a href="#0" data-date="01/01/2013">2013</a></li>
					<li><a href="#0" data-date="01/01/2014">2014</a></li>
					<li><a href="#0" data-date="01/01/2015">2015</a></li>
					<li><a href="#0" data-date="01/01/2016">2016</a></li>
				</ol>

				<span class="filling-line" aria-hidden="true"></span>
			</div> <!-- .events -->
		</div> <!-- .events-wrapper -->
			
		<ul class="cd-timeline-navigation">
			<li><a href="#0" class="prev inactive">Prev</a></li>
			<li><a href="#0" class="next">Next</a></li>
		</ul> <!-- .cd-timeline-navigation -->
	</div> <!-- .timeline -->

	<div class="events-content">
		<ol>
			<li class="selected" data-date="01/01/2007">
				<h2>2007</h2>
				<em>Establishment of Interlink Telecom as a limited company, holding 100% by Interlink Communication with paid-up share capital of Bath 30 million.</em>
				<em>Operating in construction business for broadband and mobile operatiors</em>
			</li>

			<li data-date="01/01/2012">
				<h2>2012</h2>
				<em>Obtaining 15 years license level 3 from NBTC</em>
				<em>Obtaining 30 years right of way to install fiber optic network along track pole from State Railway of Thailand</em>
				<em>Starting the installation of INTERLINK fiber optic network by focusing on Bangkok and 10 provinces around Bangkok</em>
			</li>

			<li data-date="01/01/2013">
				<h2>2013</h2>
				<em>40 provinces fiber optic network coverage</em>
				<em>Rendering network service through INTERLINK fiber optic network</em>
				<em>Establishment 18 branches of surveillance and maintenance center</em>
				<em>Starting construction and implementation of Data Center Building</em>

			</li>

			<li data-date="01/01/2014">
				<h2>2014</h2>
				<em>Baht 270 million share capital increasing to raise fund to invest in INTERLINK fiber optic network</em>
				<em>Rendering data center service for the first time</em>
				<em>Obtaining ISO27001 for data center business</em>
				<em>Obtaining IPLC license</em>
				<em>58 provinces fiber optic coverage</em>
				<em>Engaging in interconnection agreements with international network service providers</em>
			</li>

			<li data-date="01/01/2015">
				<h2>2015</h2>
				<em>Transforming to Public Limited Company as "Interlink Telecom Public Company Limited" on 11 May 2015</em>
				<em>Decreasing the par value from 100 each to Baht 1 each</em>
				<em>Increasing in share register of Baht 200 million</em>
				<em>Fill in Data Center utilization to 85%</em>
				<em>64 provinces fiber optic network coverage</em>
			</li>

			<li data-date="01/01/2016">
				<h2>2016</h2>
				<em>Fill in Data Center utilization to 95%</em>
				<em>Expand for 2nd Data Center in Q2, 2016</em>
				<em>72 provinces fiber optic network coverage in March 2016 and plan to add more coverage to 75 provinces in April 2016</em>
			</li>

		</ol>
	</div> <!-- .events-content -->
</section>	

				
 
 
 
 <!-- Section-Bar- -->
	<div class="row team-bar">
				<div class="first-one-arrow hidden-xs">
						<hr>
				</div>
				<div class="first-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
				</div>
				<div class="second-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
				</div>
				<div class="third-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
				</div>
				<div class="fourth-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
				</div>
			</div> <!--section-bar--> 

	<!-- Board of Directors -->
	<section id="board">
		<div class="container">
			<div class="center wow fadeInDown">
					<h2>Board of Directors </h2>
					<p class="lead">The board of directors is charged with maintaining the highest 
						standards of corporate governance because it believes that an effective board 
						will positively influence shareholder value</p>
			
					<div class="item active">
							<img src="images/ir/boardOfDirectors.jpg" class="img-responsive" alt=""> 
					 </div>
			</div>
				    
		</div><!--/.container-->
    </section><!--/overview-->
 
  <!-- Section-Bar- -->
	<div class="row team-bar">
				<div class="first-one-arrow hidden-xs">
						<hr>
				</div>
				<div class="first-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
				</div>
				<div class="second-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
				</div>
				<div class="third-arrow hidden-xs">
						<hr> <i class="fa fa-angle-up"></i>
				</div>
				<div class="fourth-arrow hidden-xs">
						<hr> <i class="fa fa-angle-down"></i>
				</div>
	</div> <!--section-bar--> 
            
	<!-- Organizational Structure -->
	<section id="organizationalStructure">
        <div class="container">
			
				<div class="center wow fadeInDown">
					<h2>Organizational Structure</h2>				
					<div class="item active">
							<img src="images/ir/organizationChart.jpg" class="img-responsive" alt=""> 
					</div>
				
				</div>
				
				
		</div><!--/.container-->
    </section><!--/organizationalStructure-->
			
					
  			
	<section id="partner">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Group Structure</h2>
            </div>    

            <div class="partners">
                <ul>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="images/ir/commu.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="images/ir/telecom.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="images/ir/holding.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" src="images/ir/datacenter.png"></a></li>
                    <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1500ms" src="images/ir/power.png"></a></li>
                </ul>
            </div>        
        
				
				<div class="row">
                <div class="col-sm-9 wow fadeInDown">
                    <div class="skill">
                        <div class="progress-wrap">
                            <h3>Holding to Interlink Communication</h3>
                            <div class="progress">
                              <div class="progress-bar  color1" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 26.32%">
                                <span class="bar-width">26.32%</span>
                              </div>

                            </div>
                        </div>

                        <div class="progress-wrap">
                            <h3>Interlink Communication to Interlink Telecom</h3>
                            <div class="progress">
                              <div 	class="progress-bar color2" 
										role="progressbar" 
										aria-valuenow="99" 
										aria-valuemin="0" 
										aria-valuemax="100" 
										style="width: 99.99%">
                               <span class="bar-width">99.99%</span>
                              </div>
                            </div>
                        </div>

                        <div class="progress-wrap">
                            <h3>Interlink Communication to Interlink Power and Energy</h3>
                            <div class="progress">
                              <div class="progress-bar color3" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 95.07%">
                                <span class="bar-width">95.07%</span>
                              </div>
                            </div>
                        </div>

                        <div class="progress-wrap">
                            <h3>Interlink Communication to Interlink Data Center </h3>
                            <div class="progress">
                              <div class="progress-bar color4" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="bar-width">100%</span>
                              </div>
                            </div>
                        </div>
                        
                    </div><!--/.skill-->
                </div><!--/.col-sm-6-->

            </div><!--/.row-->
        
        </div><!--/.container-->
    </section><!--/#partner--> 
    		
	
<!-- include footer.php -->
	<?php	
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/footer.php";
		include_once($path) 
	?>
<!--/end  php -->
  
  <!--.scroll-top-wrapper-->
<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<i class="fa fa-2x fa-arrow-circle-up"></i>
	</span>
</div>
<!--/.scroll-top-wrapper-->  

    <script src="js/jquery.js"></script>
    <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
    
    <script src="js/jquery-2.1.4.js"></script>
	<script src="js/jquery.mobile.custom.min.js"></script>
	<script src="js/main_time.js"></script> <!-- Resource jQuery -->
</body>
</html>
