<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>What We Do | InterlinkTelecom</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
	<link href="css/item_hover.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="fonts/stylesheet.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->
<body>

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->
    
    <section id="content" class="shortcode-item">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <h2>What We Do ?</h2> 
                    <div class="tab-wrap">
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Interlink Domestic MPLS IP-VPN</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">Interlink  Domestic Wavelength</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Interlink Domestic Dark Fiber</a></li>
                                    <li class=""><a href="#tab4" data-toggle="tab" class="tehnical">Interlink International IPLC</a></li>
                                    <li class=""><a href="#tab5" data-toggle="tab" class="tehnical">Interlink International Wavelength</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane active in" id="tab1">
                                                 <h4><span class="orangetext">Interlink Domestic MPLS IP-VPN</span></h4>
													<p>Interlink MPLS IP-VPN is a data connection service between user’s route through Interlink Fiber Optic Network with MPLS IP-VPN technology that offer ability to classify the priority of data (Class of Service) and also compatible to different data types including image, audio, and general data. Ability of Interlink Fiber Optic Network is suitable for customization to match with usage of each customer in different areas, while the MPLS IP-VPN service is able to carry big data from 1 Mbps and up to 10 Gbps. Interlink also provide backup network in order to prevent network interruption when main network has been interrupted. And the network switch process is short comparing with international standard.
Interlink MPLS IP-VPN was suitable with variety of user types including industrial estates, Finance & Banking, Telecommunication, and Digital TV service providers. In addition, Interlink also able to provide service to other countries through gateways connecting with Interlink Fiber Optic Network.</p>
												<span class="area"></span><p class="subtitle">Service areas</p>
													<p>Interlink MPLS IP-VPN service was available in Bangkok area and nationwide including business areas and industrial estates.</p>
												<span class="strongpoint"></span><p class="subtitle">Service Strengths</p>
												<ol>
													<li>Variety and ease of use: Users are able to customize the speed from 1Mbps up to 10Gbps</li>
													<li>Interlink Fiber Optic Network is compatible with variety of data type including image, audio, and general data.</li>
													<li>Class of Service: Interlink Fiber Optic Network offers ability to classify the priority of data in different data types.</li>
													<li>Backups: Interlink Fiber Optic Network also offers automatic network switch from main nodes to backup nodes in case of emergency interruption.</li>
													<li>Service area coverage in Bangkok and nationwide including business areas and industrial estates.</li>
												</ol>
                                     </div><!--/.tab-pane--> 

                                     <div class="tab-pane" id="tab2">
											<h4><span class="orangetext">Interlink Domestic Wavelength</span></h4>
                                                 <p>Interlink Wavelength is a connection service covering usage areas of users through Interlink Fiber Optic Network 
                                                 with DWDM technology, which offers ability to collect and combine small data into big data 
                                                 and transmit together with compatible with all data types including image, audio, and general data. 
                                                  Interlink Wavelength is servicing by Lamda unit, which available from 2.5Gbps/Lamda 
                                                   and 10Gbps/Lamda or based on users’ requirement.  Interlink also offer service as Turnkey, 
                                                    which offer staff to take care of customers’ equipment to provide more comfortable to customers.</p>
												<p>  Interlink Wavelength service is suitable with organizations using data connection between main data center site and backup site, 
which including Financial institutions, Telecommunications, Government Agencies, and Multinational companies.</p>
												<span class="area"></span><p class="subtitle">Service areas</p>
													<p>Interlink Wavelength service was available in Bangkok area and nationwide including business areas and industrial estates.</p>

												<span class="strongpoint"></span><p class="subtitle">Service Strengths</p>
													<ol>
														<li>Variety and ease of use: Users are able to customize the speed from 2.5Gbps and 10Gbps or more.</li>
														<li>Interlink Wavelength service offers with lower price, concerning about cost reduction of our customers.</li>
														<li>Class of Service: Interlink Wavelength offers ability to classify the priority of data in different data types.</li>
														<li>Service area coverage in Bangkok and nationwide including business areas and industrial estates.</li>
													</ol>
                                     </div><!--/.tab-pane--> 

                                     <div class="tab-pane" id="tab3">
										 <h4><span class="orangetext">Interlink Domestic Dark Fiber</span></h4>
                                        <p>Interlink Dark Fiber is a high speed network pathway rental service through 
                                        Interlink Fiber Optic Network routes for users requiring big data connection, already own equipments,
                                         and prefer to use own staff to monitor and control data connections. 
                                         Interlink Telecom will be responsible for maintaining Fiber Optic Cables to stay functional for 24/7 or based on agreement. 
                                         Thus, we also offer freedom of selecting users’ own technology to use with our Fiber Optic Network without limitations. </p>
										<p>Interlink Dark Fiber service is suitable with organizations using data connection 
										between main data center site and backup site, which including Financial institutions, 
										Telecommunications, Government Agencies, and Multinational companies.</p>
										<span class="area"></span><p class="subtitle">Service areas</p>
											<p>Interlink Dark Fiber service was available in Bangkok area and nationwide including business areas and industrial estates.</p>
										<span class="strongpoint"></span><p class="subtitle">Service Strengths</p>
											<ol>
												<li>Users have freedom to select technology and transmission speed</li>
												<li>Interlink Dark Fiber service offers with lower price, concerning about cost reduction of our customers.</li>
												<li>Class of Service: Interlink Dark Fiber offers ability to classify the priority of data in different data types.</li>
												<li>Users are able to control own staff and data transmission process.</li>
												<li>Service area coverage in Bangkok and nationwide including business areas and industrial estates.</li>
											</ol>
                                     </div><!--/.tab-pane--> 
                                     
                                     <div class="tab-pane" id="tab4">
										<h4><span class="orangetext">Interlink International IPLC</span></h4>
										<p>Interlink IPLC is a connection service to international destinations through Interlink Fiber Optic Network with DWDM technology,
                                       which offers ability to collect and combine small data into big data and transmit together with compatible with all data types including image, 
                                       audio, and general data.  Users are able to select transmission speed between 1Mbps – 10Gbps (STM-64)</p>
										<p>Interlink IPLC was suitable with users requiring data connection between countries, 
										which presence in  variety of user types including industrial estates, Finance & Banking, Telecommunication, 
										and Digital TV service providers with especially Multi-national companies.</p>
										<span class="area"></span><p class="subtitle">Service areas</p>
											<p>Interlink IPLC service was available to service to variety of countries around the world with suitable to users’ needs.</p>
										<span class="strongpoint"></span><p class="subtitle">Service Strengths</p>
										<ol>
											<li>Variety and ease of use: Users are able to customize the speed from 1Mbps up to 10Gbps (STM-64)</li>
											<li>Interlink IPLC is compatible with variety of data type including image, audio, and general data.</li>
											<li>Backups: Interlink IPLC also offers automatic network switch from main nodes to backup nodes in case of emergency interruption.</li>
											<li>Interlink IPLC was available to service from Bangkok area and nationwide to abroad.</li>
										</ol>
                                     </div><!--/.tab-pane--> 

                                     <div class="tab-pane" id="tab5">
										 <h4><span class="orangetext">Interlink International Wavelength</span></h4>
                                        <p>Interlink Wavelength is a connection service covering usage areas of users through Interlink Fiber Optic Network 
                                        with DWDM technology, which offers ability to collect and combine small data into big data 
                                        and transmit together with compatible with all data types including image, audio, and general data. 
                                         Interlink Wavelength is servicing by Lamda unit, which available from 2.5Gbps/Lamda 
                                          and 10Gbps/Lamda or based on users’ requirement.  Interlink also offer service as Turnkey, 
                                           which offer staff to take care of customers’ equipment to provide more comfortable to customers.</p>
										<p>Interlink Wavelength service is suitable with organizations using data connection between main data center site and backup site,
										which including Financial institutions, Telecommunications, Government Agencies, and Multinational companies.</p>
										<span class="area"></span><p class="subtitle">Service areas</p>
											<p>Interlink Wavelength service was available in Bangkok area and nationwide including business areas and industrial estates.</p>
										<span class="strongpoint"></span><p class="subtitle">Service Strengths</p>
											<ol>
												<li>Variety and ease of use: Users are able to customize the speed from 2.5Gbps and 10Gbps or more.</li>
												<li>Interlink Wavelength service offers with lower price, concerning about cost reduction of our customers.</li>
												<li>Class of Service: Interlink Wavelength offers ability to classify the priority of data in different data types.</li>
												<li>Service area coverage in Bangkok and nationwide including business areas and industrial estates.</li>
											</ol>
                                     </div><!--/.tab-pane-->  
                                </div> <!--/.tab-content-->  
                            </div> <!--/.media-body--> 
                        </div> <!--/.media-->     
                    </div><!--/.tab-wrap-->               
                </div><!--/.col-xs-12-->                   
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#content-->
    





<!-- include footer.php -->
		<?php	
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/footer.php";
		include_once($path) 
	?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
