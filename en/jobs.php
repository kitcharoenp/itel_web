<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Jobs | InterlinkTelecom</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
	<link href="css/item_hover.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="fonts/stylesheet.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

     <!-- Google Analytic Website tracking-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic-->

</head><!--/head-->
<body>

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/header.php";
		include_once($path) ;
		?>
<!--/end  php -->

<section id="content" class="shortcode-item">
    <div class="container">
  <div class="center wow fadeInDown" >
            <p class="lead" align="justify">Interlink Telecom Public Company
                Limited, a wholly owned subsidiary of Interlink Communications
                Public Company Limited is a leading quality Network Service
                provider. Interlink Telecom has fully committed to build a
                fiber optical network for both domestic and International
                transmission service to better serve our customer’s needs
                with the vision of Best Connectivity, Best Service and Best
                price. Come join our team, the world best opportunities are
                waiting for you</p>
            <p class="lead" align="left">
      Come join our team ,The world of opportunities are waiting for you
            </p>
        </div><!--/.center wow-->
        <div class="row ">
            <div class="col-md-8 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                <h2><span class="orangetext">Job Opportunities</span></h2>
                <div class="accordion">
                    <div class="panel-group" id="accordion1">
                      <div class="panel panel-default">
                        <div class="panel-heading active">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
                              1. Project  Manager
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>

                        <div id="collapseOne1" class="panel-collapse collapse">
                          <div class="panel-body">
                              <div class="media accordion-inner">
               <!--
                                    <div class="pull-left">
                                        <img class="img-responsive" src="images/accordion1.png">
                                    </div>
                         -->
                                    <div class="media-body">
                                        <h4><span class="orangetext">Qualification</span></h4>
                                        <ul >
                    <li>Male/Female, Age  30 – 45 Years old.</li>
                                            <li>Bachelor’s degree in Engineering (Telecommunication Engineering) or related fields.</li>
                                            <li>Good leadership, Hard Working, Patient and Good Human Relations.</li>
                                            <li>Advanced knowledge of Computer Skill.</li>
                                            <li>Proficient in  Microsoft Project</li>
                                            <li>Good Command of written, listening and Spoken English</li>
                                            <li>Have own car and driving license</li>
                                            <li>Minimum 2 years of experience in cable system or computer network</li>
                  </ul>
                                    </div>
                              </div>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1">
                              2. Sales Executive
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>
                        <div id="collapseTwo1" class="panel-collapse collapse">
                          <div class="panel-body">
              <h4><span class="orangetext">Qualification</span></h4>
                                 <ul>
                 <li>Female/Male</li>
                 <li>Bachelor's or Master’s Degree in Marketing and Sales , Business Administration, Computer Science, Computer Engineering or related field</li>
                 <li>Experienced in Telecom/ Leased Lines / IT Industry would be advantage</li>
                 <li>At least 2-3  years  experience in selling IT or Corporate Internet Service  is preferable</li>
                                     <li>Excellent skills in Microsoft Office (Power Point / Excel / Word / Outlook)</li>
                                     <li>Proficient in Presentation Skill, Negotiation Skill, and Sales Planning</li>
                                     <li>To manage and develop good interpersonal relations</li>
                                     <li>Ability to work under pressure</li>
                                     <li>Good personality, Customer service minded, proactive and take initiative</li>
                                     <li>Good command in English listening, writing and speaking is preferable</li>
                                     <li>Have own car and driving license and able to work in Bangkok and upcountry.</li>
                                     <!--<li> <span class="orangetext"><b>*ต้องมีใบสมัครพร้อมรูปถ่ายปัจจุบัน</b></span></li>-->
              </ul>
                                <h4><span class="orangetext">Job Descriptions</span></h4>
                                <ul>
                                    <li>To develop and maintain good relations with both new and existing customers</li>
                                    <li>Provide full information on products and services</li>
                                    <li>Handle full selling process from finding prospects, verifying opportunities until closing deals</li>
                                    <li>Communicate with Sales &amp; Marketing team to gather required information for updating database and also keep closely relationship with customers</li>
                                    <li>Prepare and deliver appropriate presentations on products and services/ Forecast Investment and Sales Forecast.</li>
                                </ul>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree1">
                              3. Presales
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>
                        <div id="collapseThree1" class="panel-collapse collapse">
                          <div class="panel-body">
              <h4><span class="orangetext">Qualification</span></h4>
                                 <ul>
                 <li>Male/Female age over 25 years old</li>
                 <li>Bachelor's or Master’s Degree in IT, Computer Science, Telecommunication Engineering or related fields.</li>
                 <li>0-2 years in computer network/ ISP / IT /Telecommunication</li>
                 <li>Experienced and strong knowledge in many IT solutions and systems.</li>
                 <li>Experience in Project Management for IT Infrastructure </li>
                 <li>Self-starter with strong self-management and innovation skills</li>
                 <li>Ability to work well individually or as a team</li>
                 <li>Good command of both spoken and written English</li>
                                     <li>CCNA ,CCNP, CCIE certificate will be advantage</li>
              </ul>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour1">
                              4. Internal Audit Officer / Senior Internal Audit (Urgent!!!!!!!)
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>
                        <div id="collapseFour1" class="panel-collapse collapse">
                          <div class="panel-body">
              <h4><span class="orangetext">Qualification</span></h4>
                                 <ul>
                 <li>Male/female Not Over 35 year old</li>
                 <li>Bachelor's Degree in Accounting, Business Administration or other related fields.</li>
                 <li>Good conscientious, diligent, patient, self-adaptive with high responsibility.</li>
                 <li>Excellent Microsoft Office skill</li>
                 <li>Good command of both spoken and written English</li>
                 <li>Be able to work up country.</li>
                 <li>Have working experience in internal audit jobs for at least 2-3 years.</li>
              </ul>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive1">
                              5. Project  Engineer
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>
                        <div id="collapseFive1" class="panel-collapse collapse">
                          <div class="panel-body">
              <h4><span class="orangetext">Qualification</span></h4>
                                 <ul>
                 <li>Male Not Over 30 year old</li>
                 <li>Bachelor’s Degree in IT, Computer Science, Telecommunication Engineering or related fields.</li>
                 <li>Good leadership, diligent, patient and Good Human Relations.</li>
                 <li>Good command in computer programs</li>
                 <li>Good command of both spoken and written English</li>
                 <li>Have own car and driving license</li>
                 <li>Minimum 2 years experience in cable system or computer networkี</li>
              </ul>
                          </div>
                        </div>
                      </div>

                    
<br>
                          <center>  <a href="http://203.151.143.173:8069/jobs" target = "_blank" type="button"
                            class="btn btn-primary btn-lg">Apply Now</a>
                          </center>
                        </div><!--/#accordion1-->
                    </div>
                </div>

                <div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <h2><span class="orangetext">Please Send your Resume to</span></h2>
                    <p>Human Resource Department<br><br>
Interlink Telecom Public Company Limited<br>
48 INTERLINK Building, Soi Rung-reung<br>
Ratchadaphisek Road, Samsennok, <br>
Huay Khwang, Bangkok 10310<br>
 <br>
Phone : +66 02 693 1222  Ext. 221<br>
 <br>
E-mail : <br>personnel@interlink.co.th ,
<br>jobs@interlinktelecom.co.th
                    </p>
                  </div>

            </div><!--/.row-->

 			<div class="center" >

                <p><br>
                </p>
            </div><!--/.center wow-->

        </div><!--/.container-->
    </section><!--/#content-->

<section id="middle">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 wow fadeInDown">
                    <div class="skill">
                        <h1><span class="orangetext">Benefits</span></h1>
                                     <ul>
										 <li>Provident Fund </li>
										 <li>Allowances for employees in case of marriage, childbirth, and death of employees or their directed family members.</li>
										 <li>Shift allowance </li>
										 <li>Special allowances</li>
										 <li>Health check-up </li>
										 <li>Company’s uniform</li>
										 <li>Group life, accident, and health insurance </li>
										 <li>The company’s Savings and Credit Cooperative</li>
										 <li>Salary and bonus are adjusted</li>
										 <li>In-house and outside training </li>
										 <li>Social Insurance</li>
									</ul>
                </div><!--/.col-sm-6-->

            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#middle-->

<!-- include footer.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/footer.php";
		include_once($path)
	?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
