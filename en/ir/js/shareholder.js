    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
    var randomColorFactor = function() {
        return Math.round(Math.random() * 255);
    };
    var randomColor = function(opacity) {
        return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
    };

    var config_ilink = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    "25.62",
                    "14.35",
                    "12.73",
                    "7.81",
                    "1.88",
                    "37.61",
			
                ],
                backgroundColor: [
                    "#F7464A",
                    "#46BFBD",
                    "#FDB45C",
                    "#949FB1",
                    "#4D5360",
                    "#FFCE56",
                ],
                label: 'ILINK'
            }, ],
            labels: [
                "บริษัท อินเตอร์ลิ้งค์ โฮลดิ้ง จำกัด",
                "นางชลิดา อนันตรัมพร",
                "นายสมบัติ อนันตรัมพร",
                "นายศักดิ์ชัย ศักดิ์ชัยเจริญกุล",
                "นายสุเทพ พัฒนสิน",
                "Other",
            ]
        },
        options: {
            responsive: true,
            legend: {
				display: false,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'Interlink Communication Public Company Limited'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

    
    var config_itel = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    "99.99",
                    "0.01",
			
                ],
                backgroundColor: [
                    "#F7464A",
                    "#46BFBD",
                ],
                label: 'ITEL'
            }, ],
            labels: [
                "Interlink Communication Public Company Limited",
                "Other",
            ]
        },
        options: {
            responsive: true,
            legend: {
				display: false,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'Interlink Telecom Public Company Limited'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };
    
    var config_power = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    "99.99",
                    "0.01",
			
                ],
                backgroundColor: [
                    "#46BFBD",
                    "#FDB45C",
                ],
                label: 'POWER'
            }, ],
            labels: [
                "Interlink Communication Public Company Limited",
                "Other",
            ]
        },
        options: {
            responsive: true,
            legend: {
				display: false,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'Interlink Power & Energy Company Limited'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };
    
    var config_data = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    "99.99",
                    "0.01",
			
                ],
                backgroundColor: [
                    "#949FB1",
                    "#4D5360",
                ],
                label: 'DATA'
            }, ],
            labels: [
                "Interlink Communication Public Company Limited",
                "Other",
            ]
        },
        options: {
            responsive: true,
            legend: {
				display: false,
                position: 'bottom',
            },
            title: {
                display: true,
                text: ' Interlink Data Center Company Limited'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

    window.onload = function() {
        var ctx = document.getElementById("chart_ilink").getContext("2d");
        window.myDoughnut = new Chart(ctx, config_ilink);
        
        var ctx2 = document.getElementById("chart_itel").getContext("2d");
        window.myDoughnut = new Chart(ctx2, config_itel);
        
        var ctx2 = document.getElementById("chart_power").getContext("2d");
        window.myDoughnut = new Chart(ctx2, config_power);
        
        var ctx2 = document.getElementById("chart_data").getContext("2d");
        window.myDoughnut = new Chart(ctx2, config_data);
    };

