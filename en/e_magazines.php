<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premier Fiber Optical Network">
    <meta name="author" content="">
    <title>E-Magazines | InterlinkTelecom</title>

    <!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

<!-- script scrollToTop -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
$(function(){
	$(document).on( 'scroll', function(){

		if ($(window).scrollTop() > 100) {
			$('.scroll-top-wrapper').addClass('show');
		} else {
			$('.scroll-top-wrapper').removeClass('show');
		}
	});

	$('.scroll-top-wrapper').on('click', scrollToTop);
});

function scrollToTop() {
	verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	element = $('body');
	offset = element.offset();
	offsetTop = offset.top;
	$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}
</script>
<!-- /script scrollToTop -->

  <!-- Google Analytic Website tracking-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic-->

</head><!--/head-->

<body>

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/header.php";
		include_once($path) ;
		?>
<!--/end  php -->


    <section class="pricing-page">
        <div class="container">
            <div class="center">
                <h2><span class="orangetext">E-MAGAZINES</span></h2>
                <!--
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
                -->
            </div>

            <section id="portfolio">
        <div class="container">
       
  <ul class="portfolio-filter text-center">
                <li><a class="btn btn-default active" href="#" data-filter="*">All </a></li>
                <li><a class="btn btn-default" href="#" data-filter=".2559">YEAR 2016</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".2558">YEAR 2015</a></li>
              </li>
            </ul><!--/#portfolio-filter--></div></section>
                  <div class="portfolio-items"> 

            <div class="col-md-12">
            <div class="pricing-area text-center">
                <div class="row">

 <div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-one">
                                <h1>I-TEL VOL.17</h1>
                                <span>01-30 NOVEMBER  2016</span>
                            </li>

                            <li>

                                <a href="http://data.axmag.com/data/201611/20161101/U137273_F408530/FLASH/index.html" target="_blank">
                                    <img src="assets/magazines/cover171.jpg" class="" alt="">
                                    <div style="position: absolute;top: 45%; left: 0;width: 100%;">
                                        <h3><u>View Online</u></h3>
                                        </div>
                                </a>
                            </li>

                            <li> One Stop Service Network</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine17.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul>
                    </div></div>




                   <div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-one">
                                <h1>I-TEL VOL.16</h1>
                                <span>01-31 OCTOBER 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201610/20161004/U137273_F404732/index.html" target="_blank">
									<img src="assets/magazines/cover16.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>High Frequency Trading</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine16.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-three-->

					<div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-three">
                                <h1>I-TEL VOL.15</h1>
                                <span>01-30 SEPTEMBER 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201609/20160921/U137273_F401787/FLASH/index.html" target="_blank">
									<img src="assets/magazines/cover15.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>1st Trading Day</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine15.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-three-->

					<div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-two">
                                <h1>I-TEL VOL.14</h1>
                                <span>01-31 AUGUST 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201608/20160810/U137273_F394153/index.html" target="_blank">
									<img src="assets/magazines/cover14.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Dedicate Internet Access</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine14.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul>
                    </div><!--/col-sm-4 plan price-three-->
</div>
				</div><!--/.row-->


				<div class="row">

                    <div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.13</h1>
                                <span>01-31 JULY 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201607/20160708/U137273_F389640/index.html" target="_blank">
									<img src="assets/magazines/cover13.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Business Continuity Planning Network</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine13.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-three-->

					<div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.12</h1>
                                <span>01-30 JUNE 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201606/20160614/U137273_F385049/index.html" target="_blank">
									<img src="assets/magazines/cover12.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>FinTech</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine12.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-three-->

					<div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.11</h1>
                                <span>01-31 MAY 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201605/20160509/U137273_F380861/index.html" target="_blank">
									<img src="assets/magazines/cover11.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Hybrid Connectivity Network</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine11.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul>
                    </div><!--/col-sm-4 plan price-three-->

</div>
                </div><!--/.row-->

                <div class="row">

                  <div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.10</h1>
                                <span>01-31 APRIL 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201604/20160407/U137273_F377086/FLASH/index.html" target="_blank">
									<img src="assets/magazines/cover10.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Business Continuity Planning Network</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine10.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-three-->

				<div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.09</h1>
                                <span>01-31 MARCH 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201603/20160303/U137273_F373267/index.html" target="_blank">
									<img src="assets/magazines/cover09.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Full Redundancy Network</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine09.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-three-->

					<div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.08</h1>
                                <span>01-29 FEBRUARY 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201602/20160211/U137273_F370714/FLASH/index.html" target="_blank">
									<img src="assets/magazines/cover08.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>MPLS Layer 2 And MPLS Layer 3</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine08.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul>
                    </div><!--/col-sm-4 plan price-three-->

</div>
                </div><!--/.row-->

                <div class="row">

                   <div class="portfolio-item 2559 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.07</h1>
                                <span>01-31 JANUARY 2016</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201601/20160107/U137273_F366261/FLASH/index.html" target="_blank">
									<img src="assets/magazines/cover07.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Content Delivery Network</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine07.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-three-->

					<div class="portfolio-item 2558 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.06</h1>
                                <span>01-31 DECEMBER 2015</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201512/20151214/U137273_F363981/FLASH/index.html" target="_blank">
									<img src="assets/magazines/cover06.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Cloud Service</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine06.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-four-->
<div class="portfolio-item 2558 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.05</h1>
                                <span>01-30 NOVEMBER 2015</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201511/20151110/U137273_F360138/index.html" target="_blank">
									<img src="assets/magazines/cover05.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Broadcast Service &amp; 5G</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine05.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-two-->

                </div><!--/.row-->

                <div class="row">

                  <div class="portfolio-item 2558 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                 <h1>I-TEL VOL.04</h1>
                                <span>01-31 OCTOBER 2015</span>
                            </li>

						<li>
								<a href="http://data.axmag.com/data/201510/20151009/U137273_F355761/index.html" target="_blank">
									<img src="assets/magazines/cover04.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>International Private Leased Circuit</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine04.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-four-->

					<div class="portfolio-item 2558 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.03</h1>
                                <span>01-30 SEPTEMBER 2015</span>
                            </li>

                            <li>
								<a href="http://data.axmag.com/data/201509/20150902/U137273_F351389/index.html" target="_blank">
									<img src="assets/magazines/cover03.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Thailand Telecom Market</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine03.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-four-->

					<div class="portfolio-item 2558 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.02</h1>
                                <span>01-31 AUGEST 2015</span>
                            </li>

                            <li>
								<a href="http://data.axmag.com/data/201507/20150730/U137273_F347514/FLASH/index.html" target="_blank">
									<img src="assets/magazines/cover02.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>Internet of Things</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine02.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul>
                    </div><!--/col-sm-4 plan price-four-->
</div>
                </div><!--/.row-->

                <div class="row">

				<div class="portfolio-item 2558 col-xs-12 col-sm-4 col-md-4">
                    <div class="col-sm-12 plan price-two wow fadeInDown">
                        <ul>
                            <li class="heading-four">
                                <h1>I-TEL VOL.01</h1>
                                <span>01-31 JULY 2015</span>
                            </li>

                            <li>

								<a href="http://data.axmag.com/data/201507/20150707/U137273_F344886/index.html" target="_blank">
									<img src="assets/magazines/cover01.jpg" class="" alt="">
									<div style="position: absolute;top: 45%; left: 0;width: 100%;">
										<h1><u>View Online</u></h1>
									</div>
								</a>
							</li>

                            <li>2015 Big Data</li>
                            <li class="plan-action">
                                <a href="assets/magazines/magazine01.pdf" class="btn btn-primary" target="_blank">Download</a>
                            </li>

                        </ul></div>
                    </div><!--/col-sm-4 plan price-four-->

                </div><!--/.row-->
</div></div>






            </div><!--/pricing-area-->
        </div><!--/container-->
    </section><!--/pricing-page-->

 <!-- include footer.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/footer.php";
		include_once($path)
	?>
<!--/end  php -->

 <!-- 14-12-17 -->
<!--.scroll-top-wrapper
<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<i class="fa fa-2x fa-arrow-circle-up"></i>
	</span>
</div>
<!--/.scroll-top-wrapper-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
