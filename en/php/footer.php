   <?php
   echo '
    <footer id="footer" style="padding-bottom:0.5em; padding-top:1em" >
        <div class="container">
            <div class="row">
				
				<div class="col-sm-6" >			
                        <div class="pull-left">
							<img src="images/iso27001.png" class="img-responsive">
                        </div><!--/.pull-left-->
                       
                        <div class="media-body" >
                            &copy; 2016 Interlink Telecom Public Company Limited &nbsp;&nbsp;
                        </div><!--/.media-body-->                      
                </div><!--/.col-sm-6-->
 
 				<div class="col-sm-6">                      
                        <div class="media-body" >
                           <ul class="pull-right" >
								<li ><a  href="index.php">Home</a></li>
								<li><a  href="about-us.php">About Us</a></li>
								<li><a  href="contact-us.php">Contact Us</a></li>
							</ul>
                        </div><!--/.media-body-->
                </div><!--/.col-sm-6-->
                                                               
            </div><!--/.row-->
        </div><!--/.container-->
    </footer><!--/#footer-->
    ';
?>
