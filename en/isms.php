<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premier Fiber Optical Network">
    <meta name="author" content="">
    <title>ISMS | InterlinkTelecom</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
     <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
 
</head><!--/head-->

<body>

<body>
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->

    <section id="isms">
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>ISMS Policy</h2>
                <p class="lead" align="justify">Interlink Telecom  Company Limited had implemented the security of the data. 
According to the information security management standard (ISO / IEC 27001), 
to maintain the confidentiality, accuracy and usability of data is therefore the policy of security of the data.</p>
            </div>
<div class="row">
                <div class="features">
                    <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-umbrella"></i>                          
                            <h3 >1. To encourage employees and supervisors. Implement and manage information security effectively.</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-sitemap"></i>                     
                            <h3 align="justify">2. Requiring supervisors to support and authorize the staff  to manage the security of the data used in the work area.</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-shield"></i>           
                            <h3 align="justify">3. To perform communication Policy or framework for the purpose of information 
security (ISMS Framework) to all employees in the organization.</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-calendar-o"></i>
                            <h3 align="justify">4.  Capacity Plan every year to support business growth.</h3>
                        </div>
                    </div><!--/.col-md-4-->
                
                    <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-bar-chart-o"></i>
                            <h3 align="justify">5. A management measure to ensure the security of information put into practice effectively.</h3>
                        </div>
                    </div><!--/.col-md-4-->



                    <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-heart"></i>
                            <h3 align="justify">6. Information security is part of the strategy of the company. To increase market share</h3>
                        </div>
                    </div><!--/.col-md-4-->
 
                     <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-rocket"></i>
                            <h3 align="justify">7. The continued development as part of the goals of the organization.</h3>
                        </div>
                    </div><!--/.col-md-4-->
  
                      <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-ambulance"></i>
                            <h3 align="justify">8. Management Review every year.In order to improve the quality of service.</h3>
                        </div>
                    </div><!--/.col-md-4-->
  
                      <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-ticket"></i>
                            <h3 align="justify">9. The demands of Customers Interested parties legal or regulatory obligations under the contract. 
Be put into practice If not inconsistent with the business and technical requirements.</h3>
                        </div>
                    </div><!--/.col-md-4-->
  
                      <div class=" col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-medkit"></i>
                            <h3 align="justify">10. Routine maintenance the Information Data Center. And continuously improving quality services.</h3>
                        </div>
                    </div><!--/.col-md-4-->
                       
                </div><!--/.services-->
            </div><!--/.row--> 			
						
			</div><!--section-->
		</div><!--/.container-->
    </section><!--/about-us-->
	

<!-- include footer.php -->
		<?php	
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/footer.php";
		include_once($path) 
	?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
