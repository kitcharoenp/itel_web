<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premier Fiber Optical Network">
    <meta name="author" content="">
    <title>Why Choose Us | InterlinkTelecom</title>
    
    <!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
     <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
 
</head><!--/head-->

<body>

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/en/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->

        <section id="service" class="service-item">
	   <div class="container">

		<div class="clients-area center wow fadeInDown">
			<h2>Why Choose Us?</h2>
                <p class="lead" align="justify">
					Interlink Telecom Public Company Limited is not just another communications provider. 
					We’re a way to connect and a way to move businesses forward. It’s not just about 
					connecting our customers; it’s about helping them find the best way to share information, 
					link locations and share data among the group.</p>
		        <p class="lead" align="justify">
					Our advanced technology and support aim to serve every businesses as the largest 
					cable operators, mobile wireless companies and Internet-based content providers.</p>
                <p class="lead" align="justify">
					Every day, our nationwide team strive to realize our promise to our customers – 
					Solutions You Want, Support You Need. Although we love technology and are an IP 
					services leader, what we are most passionate about is delivering the best customer 
					experience in the industry. We know our customers are not just buying technology—
					they are also getting the people behind it; the entire Interlink Telecom team.</p>
                <p class="lead" align="justify">
					We’re focused on doing the things we do better than anyone. 
					We’re passionate about communications, and we want to pass that passion along to 
					our customers. It’s all about finding the right solutions—not just selling a service. 
					Keeping our customers happy and being true to the spirit of Interlink Telecom is at the heart of everything we do.</p>
                <p class="lead" align="justify">
					We invite you to join our dynamic, talented and focused team of professionals and 
					move your career forward with Interlink Telecom.</p>

		
		</div>
            
            <div class="get-started center wow fadeInDown">
                <h2>Ready to get started</h2>
                <p class="lead">"One secret of success in life is for a man to be ready for his opportunity when it comes"  <br><b>-Benjamin Disraeli, Prime Minister of the United Kingdom-</b> </p>
                <div class="request">
                    <h4><a href="jobs.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apply for Jobs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h4>
                </div>
            </div><!--/.get-started-->
            
            
        </div><!--/.container-->
    </section><!--/#whychooseus-->
    

    <footer id="footer" style="padding-bottom:0.5em; padding-top:1em" >
        <div class="container">
            <div class="row">
				
				<div class="col-sm-6" >			
                        <div class="pull-left">
							<img src="images/iso27001.png" class="img-responsive">
                        </div><!--/.pull-left-->
                       
                        <div class="media-body" >
                            &copy; 2015 Interlink Telecom Public Company Limited &nbsp;&nbsp;
                        </div><!--/.media-body-->                      
                </div><!--/.col-sm-6-->
 
 				<div class="col-sm-6">                      
                        <div class="media-body" >
                           <ul class="pull-right" >
								<li ><a  href="index.html">Home</a></li>
								<li><a  href="about-us.html">About Us</a></li>
								<li><a  href="contact-us.html">Contact Us</a></li>
							</ul>
                        </div><!--/.media-body-->
                </div><!--/.col-sm-6-->
                                                               
            </div><!--/.row-->
        </div><!--/.container-->
    </footer><!--/#footer-->

<!-- include footer.php -->
		<?php //include("php/footer.php"); ?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
