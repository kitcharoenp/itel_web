   <?php
   echo '
<header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="top-number"><p><i class="fa fa-phone-square"></i>  +66 02 693 1122</p></div>
                    </div>
                    <div class="col-sm-6 col-xs-8">
                       <div class="social">
                            <ul class="social-share">
                                <li><a href="https://www.facebook.com/interlinktelecom.co.th"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="../en/index.php">EN</span></i></a></li>
                                <li><a href="#"><span class="orangetext">TH</i></a></li>
                            </ul>
                       </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li ><a href="index.php">Home</a></li>
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
                            <ul class="dropdown-menu">
								<li><a href="about-us.php">Company</a></li> 
								<li><a href="isms.php">ISMS Policy</a></li> 
                            </ul>
                        </li>
                        <li><a href="services.php">Services</a></li>
                        
                        <li>
							<a href="ir/index.html">IR</a>
                        </li>
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">News & Events</a>
                            <ul class="dropdown-menu">
                                <li><a href="portfolio.php">News & Events</a></li>
                                <li><a href="e_magazines.php">E-Magazines</a></li>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Careers</a>
                            <ul class="dropdown-menu">
                                <li><a href="whychooseus.php">Why Choose Us?</a></li>
                                <li><a href="jobs.php">Jobs</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="contact-us.php">Contact</a></li>
                                             
                    </ul>
                </div><!--/.collapse-->
                
            </div><!--/.container-->
        </nav><!--/navbar-->
    </header><!--/header-->
    ';
?>
