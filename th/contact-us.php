<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premier Fiber Optical Network">
    <meta name="author" content="">
    <title>Contact | InterlinkTelecom</title>
    
    <!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
     <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
 
</head><!--/head-->

<body>

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->

    <section id="contact-info">
        <div class="center">                
            <h2><span class="orangetext">จะพบเราได้อย่างไร?</span></h2>
            <p class="lead">หากคุณสนใจผลิตภัณฑ์และบริการของเรา หรือต้องการสอบถามรายละเอียดเพิ่มเติม โปรดอย่างลังเลที่จะติดต่อเรา </p>
        </div>
        <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <div class="gmap">
                            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3874.796463212561!2d100.576514!3d13.791139999999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29dcd6ce56117%3A0x192b9693e3de82a1!2sInterlink+Telecom+Plc!5e0!3m2!1sth!2sth!4v1409286468878"></iframe>
                        </div>
                    </div>

                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-7">
                                <address>
                                    <h5><span class="orangetext">บริษัท อินเตอร์ลิ้งค์เทเลคอม จำกัด (มหาชน)</span></h5>
                                    <p>48 อาคารอินเตอร์ลิ้งค์ ซอยรุ่งเรือง<br>
											ถนนรัชดาภิเษก แขวงสามเสนนอก
											เขตห้วยขวาง กรุงเทพฯ 10310</p>
                                    <p>โทรศัพท์ : 0-2693-1222<br>
                                    อีเมลล์แอดเดรส : info@interlinktelecom.co.th</p>
                                </address>
								<h2><span class="orangetext">ดาวน์โหลดแผนที่</span></h2>
								<ul class="gallery clearfix">
									<li><a href="images/MapInterlinkTelecom.jpg"  title="" target="_blank" rel="prettyPhoto">
										<img src="images/ico/area.png" width="60" height="60" alt="Feel Free to Contact Us" /></a></li>
								</ul>
                            </li>



                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>  <!--/gmap_area -->

    <section id="contact-page">
        <div class="container">
            <div class="center">        
                <h2><span class="orangetext">Drop Your Message</span></h2>
                <p class="lead">หากโซลูชั่นทางธุรกิจของคุณ ไม่พบในหน้าเว็ปเพจเรา 
                ทุกคำถามของคุณมีความหมาย เรายินดีให้ความกระจ่างและช่วยเหลือ กรุณากรอกข้อมูลของคุณ </p>                
            </div> 
            <div class="row contact-wrap"> 
                <div class="status alert alert-success" style="display: none"></div>
                <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="../includes/sendemail.php">
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="form-group">
                            <label>ชื่อ นามสกุล*</label>
                            <input type="text" name="name" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>อีเมลล์แอดเดรส *</label>
                            <input type="email" name="email" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>เบอร์ติดต่อ</label>
                            <input type="number" name="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>บริษัท</label>
                            <input type="text" name="company" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label>หัวข้อ *</label>
                            <input type="text" name="subject" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>ข้อความ *</label>
                            <textarea name="message" id="message" required="required" class="form-control" rows="8"></textarea>
                        </div>                        
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-primary btn-lg" required="required">ส่งข้อความ</button>
                        </div>
                    </div>
                </form> 
            </div><!--/.row-->
     </div><!--/.container-->
 </section><!--/#contact-page-->


<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/footer.php";
		include_once($path) ; 
		?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
