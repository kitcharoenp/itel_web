<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premier Fiber Optical Network">
    <meta name="author" content="">
    <title>Why Choose Us? | InterlinkTelecom</title>
    
    <!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
     <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
 
</head><!--/head-->

<body>

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->

        <section id="service" class="service-item">
	   <div class="container">
            <div class="center wow fadeInDown">
                <h2>ทำไมต้องเลือกเรา?</h2>
                
                <p class="lead" align="justify">เราคือ ผู้ให้บริการ โครงข่ายไฟเบอร์ออฟติคทั่วไทย บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน)
                 ไม่ได้เป็นเพียงแค่ผู้ให้บริการโครงข่ายสื่อสารแต่เรา คือ เส้นทางสู่ความก้าวหน้าในธุรกิจ  
                 และเรายังคงค้นหาเส้นทางในการเชื่อมต่อที่ดีที่สุดในการโอนถ่ายข้อมูลภายในบริษัทลูกค้าของเรา</p>
		        <p class="lead" align="justify">เรามีเทคโนโลยีทันสมัยสามารถรองรับลูกค้าธุรกิจขนาดใหญ่, 
		        ผู้ให้บริการโทรศัพท์มือถือ, ผู้ให้บริการอินเทอร์เน็ตด้วยทีมงานที่กระจายอยู่ทั่วประเทศด้วยคำสัญญาที่ว่า 
		        "Solutions you want, Support you need".</p>
                <p class="lead" align="justify">แม้เราจะมีความเชี่ยวชาญใน ด้าน IPService 
                แต่เราก็ยังมุ่งมั่นที่จะสร้างความพอใจสูงสุดในธุรกิจโครงข่ายไฟเบอร์ออฟติคให้แก่ลูกค้า 
                เพราะลูกค้าของเราไม่เพียงแต่ได้รับบริการด้านโทรคมนาคมและการสื่อสาร แต่ทุกท่านจะ
                ได้รับการดูแลที่ดีเยี่ยม จาก “ทีมอินเตอร์ลิ้งค์เทเลคอม” บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) 
                มุ่งมั่นที่จะพัฒนาบริการด้านเทคโนโลยีระบบการสื่อสารให้ดีที่สุด เราตระหนักในความสำคัญ
                ของระบบสื่อสาร และเราจะให้ความตระหนักเพื่อสร้างผลงานที่ดีที่สุดให้ลูกค้าของเรา</p>
                <p class="lead" align="justify">นั้นเพราะเราเลือกสิ่งที่ดีที่สุด ไม่ใช่เพียงการให้บริการ 
                แต่เราจะมอบความสุขให้ลูกค้าทุกท่านด้วยจิตวิญญาณและหัวใจของอินเตอร์ลิ้งค์เทเลคอม 
                ด้วยเหตุนี้ เราจึงชวนท่านให้มาเป็นส่วนหนึ่งในความก้าวหน้า และความสำเร็จในธุรกิจโครงข่าย
                ไฟเบอร์ออฟติค พร้อมกับเรา ”อินเตอร์ลิ้งค์เทเลคอม"</p>
               
            </div>
            
			<div class="get-started center wow fadeInDown">
                <h2>Ready to get started</h2>
                <p class="lead">"One secret of success in life is for a man to be ready for his opportunity when it comes"  <br><b>-Benjamin Disraeli, Prime Minister of the United Kingdom-</b> </p>
                <div class="request">
                    <h4><a href="jobs.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apply for Jobs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></h4>
                </div>
            </div><!--/.get-started-->

        </div><!--/.container-->
    </section><!--/#whychooseus-->
    
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/footer.php";
		include_once($path) ; 
		?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
