<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premier Fiber Optical Network">
    <meta name="author" content="">
    <title>Home | InterlinkTelecom</title>
	<script src="js/jquery.js"></script>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

  <!-- Google Analytic Website tracking-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic-->

</head><!--/head-->

<body class="homepage">

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/header.php";
		include_once($path) ;
		?>
<!--/end  php -->

    <section id="main-slider" class="no-margin">
        <div id="main-slider" class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
                <li data-target="#main-slider" data-slide-to="3"></li>
                <li data-target="#main-slider" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner">

                <div class="item active" style="background-image: url(images/slider/bg1.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6 col-sm-offset-1">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1">เสถียรภาพและความปลอดภัย</h1>
                                    <h2 class="animation animated-item-2">
										มุ่งเน้นเสถียรภาพและความปลอดภัย แยกวงจรลูกค้าแต่ละรายอิสระจากกัน ทำให้มั่นใจได้ถึงเสถียรภาพ และความปลอดภัยในระดับสูง</h2>
                                    <a class="btn-slide animation animated-item-3" href="about-us.php">อ่านเพิ่มเติม..</a>
                                </div>
                            </div>

                         <!--
							<div class="col-sm-5 hidden-xs animation animated-item-4">
                                <div class="slider-img">
									<div class="embed-responsive embed-responsive-16by9">
										<iframe width="400" height="269" align="right"
						src="//www.youtube.com/embed/2fizuamYads?rel=0&autohide=1&showinfo=0"" frameborder=0
allowfullscreen="true" ></iframe>
									</div>
                                </div><!--/.slider-img
                            </div><!--/.col-sm-5-->

                        </div>
                    </div>
                </div><!--/.item-->

                <div class="item" style="background-image: url(images/slider/bg2.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6 col-sm-offset-1">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1">ความเร็วและประสิทธิภาพ</h1>
                                    <h2 class="animation animated-item-2">
										เชื่อมต่อทุกการใช้งานด้วยสายใยแก้วนำแสง เพื่อรองรับความเร็วที่ไม่จำกัด สำหรับการใช้งานทุกแอปพลิเคชั่น</h2>
                                    <a class="btn-slide animation animated-item-3" href="about-us.php">อ่านเพิ่มเติม..</a>
                                </div>
                            </div>
							<!--
                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img">
                                    <img src="images/slider/img2.png" class="img-responsive">
                                </div>
                            </div>
							-->
                        </div>
                    </div>
                </div><!--/.item-->

                <div class="item" style="background-image: url(images/slider/bg3.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6 col-sm-offset-1">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1">ครอบคลุมทั่วประเทศ</h1>
                                    <h2 class="animation animated-item-2">
										โครงข่ายไฟเบอร์ออฟติคครอบคลุมทั่วประเทศ  เพื่อการเชื่อมต่อที่ไร้ขีดจำกัด</h2>
                                    <a class="btn-slide animation animated-item-3" href="about-us.php">อ่านเพิ่มเติม..</a>
                                </div>
                            </div>
							<!--
                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img">
                                    <img src="images/slider/img3.png" class="img-responsive">
                                </div>
                            </div>
							-->
                        </div>
                    </div>
                </div><!--/.item-->

                <div class="item" style="background-image: url(images/slider/bg4.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6 col-sm-offset-1">
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1">สุดยอดการเชื่อมต่อ</h1>
                                    <h2 class="animation animated-item-2">
											สุดยอดการเชื่อมต่อที่มีคุณภาพ<br>
											ด้วยบริการที่ดีที่สุด<br>
											ราคาที่แข่งขันได้
										</h2>
                                    <a class="btn-slide animation animated-item-3" href="about-us.php">อ่านเพิ่มเติม..</a>
                                </div>
                            </div>
                            <!--
                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img">
                                    <img src="images/slider/img1.png" class="img-responsive">
                                </div>
                            </div>
                            -->
                        </div>
                    </div>
                </div><!--/.item-->



                <div class="item" style="background-image: url(images/slider/bg6.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <div class="col-sm-6  col-sm-offset-1">
                                <div class="carousel-content">
									<h1 class="animation animated-item-1 ">มูลนิธิอินเตอร์ลิงค์ให้ใจ</h1>
									<h2 class="animation animated-item-2">
										"โครงการพี่สอนน้อง" เป็นหนึงในโครงการ มูลนิธิอินเตอร์ลิงค์ให้ใจ เน้นการมอบความรู้ คู่คุณธรรม ให้กับเด็กและเยาวชน</h2>
									<a class="btn-slide animation animated-item-3" href="assets/csrs/crsInfo00.jpg" rel="prettyPhoto[crs00]">อ่านเพิ่มเติม..</a>
                                </div>
                            </div>
                            <!--
                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                                <div class="slider-img">
                                    <img src="images/slider/img1.png" class="img-responsive">
                                </div>
                            </div>
                            -->
                       </div>
                    </div>
                </div><!--/.item-->

            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </section><!--/#main-slider-->



        <div class="container">
		<div class="row">
                <div class="col-sm-4">
                    <div class="media  wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
						<div>
							<h2><span class="orangetext">NEWS & EVENTS</span></h2>
						</div><!--/-->

                        <div class="">
							<a class="preview" href="portfolio/161002/7.jpg" rel="prettyPhoto[1610-1]"
								title="
นายสมบัติ อนันตรัมพร ประธานกรรมการและกรรมการผู้จัดการใหญ่ กลุ่มบริษัทอินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จํากัด(มหาชน) นางชลิดา อนันตรัมพร กรรมการผู้จัดการ และประธานมูลนิธิอินเตอร์ลิ้งค์ให้ใจและนายณัฐนัย อนันตรัมพร  กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จํากัด(มหาชน) นําคณะผู้บริหารและพนักงานกลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพต่อหน้าพระบรมฉายาลักษณ์พระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช มหิตลาธิเบศรรามาธิบดี จักรีนฤบดินทร สยามินทราธิราช เพื่อแสดงออกถึงความจงรักภักดี จากนั้นนายสมบัติ อนันตรัมพร นางชลิดา อนันตรัมพรและนายณัฐนัย อนันตรัมพร พร้อมทั้งพนักงานร่วมยืนไว้อาลัยเป็นเวลา 9 นาที เพื่อแสดงความน้อมรําลึกในพระมหากรุณาธิคุณหาที่สุดมิได้

">
                            <img class="img-responsive img-rounded" src="portfolio/161002/161002-7.jpg" alt="">
                           </a>

                            <div class="overlay">
                                <div class="recent-work-inner">
									<h3><span class="lead"> กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ</span></h3>
                                </div>
                            </div>
                        </div>
						<div>
                            <a class="btn btn-warning readmore" href="portfolio.php">ดูทั้งหมด</a>
                                        <a class="preview" href="portfolio/161002/1.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                         <a class="preview" href="portfolio/161002/2.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>

                                         <a class="preview" href="portfolio/161002/3.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                           <a class="preview" href="portfolio/161002/4.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                                <a class="preview" href="portfolio/161002/5.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>

                                         <a class="preview" href="portfolio/161002/6.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                           <a class="preview" href="portfolio/161002/8.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
						</div><!--/-->
					</div><!--/.media wow-->
                </div><!--/.col-sm-4-->

                <div class="col-sm-4">
                    <div class="media  wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="800ms">
						<div>
							<h2><span class="orangetext">E-MAGAZINES</span></h2>
						</div><!--/-->
                        <div class="pull-left">
                            <img class="img-responsive img-rounded" src="assets/magazines/index_cover17.jpg"  alt="">
                        </div><!--/.pull-left-->

                        <div class="media-body">
                            <h2>ITEL VOL.17</h2>
                            <ul>
                            <li><a class="preview" href="http://data.axmag.com/data/201611/20161101/U137273_F408530/FLASH/index.html" target="_blank" ><i class="fa fa-eye"></i>  อ่านออนไลน์</a></li>
                            <li><a class="preview" href="assets/magazines/magazine17.pdf" ><i class="fa fa-download"></i>  ดาวน์โหลด</a></li>
                        </ul>
                        </div><!--/.media-body-->
                         <div>
                            <a class="btn btn-warning readmore" href="e_magazines.php">ดูทั้งหมด</a>
					</div><!--/-->
                    </div><!--/.media-->
                </div><!--/.col-sm-4-->

                <div class="col-sm-4">
                    <div class="media  wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms">
						<div>
							<h2><span class="orangetext">Video Presentation</span></h2>
						</div><!--/-->
                        <div class="pull-left">

						<video width="400" height="220" controls>
							<source src="assets/videos/presentation_2016_th_mini.mp4" type="video/mp4">
						</video>

                        </div>
					</div><!--/.media-->
                </div><!--/.col-sm-4-->

            </div><!--/.row-->


			</div><!--/.row-->


        </div><!--/.container-->
<br>

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/footer.php";
		include_once($path) ;
		?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
