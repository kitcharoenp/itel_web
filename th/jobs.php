<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="มาร่วมงานกับเรา">
    <meta name="author" content="">
    <title>ตำแหน่งงานเปิดรับ | อินเตอร์ลิงค์เทเลคอม</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
	<link href="css/item_hover.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="fonts/stylesheet.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

     <!-- Google Analytic Website tracking-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic-->

</head><!--/head-->
<body>
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/header.php";
		include_once($path) ;
		?>
<!--/end  php -->

<section id="content" class="shortcode-item">
    <div class="container">
  <div class="center wow fadeInDown" >
            <p class="lead" align="justify">บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน)
            บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน)  ผู้ให้บริการโครงข่ายไฟเบอร์ออฟติคชั้นนำ
            และเป็นบริษัทหนึ่งในกลุ่มบริษัทอินเตอร์ลิ้งค์  คอมมิวนิเคชั่น จำกัด(มหาชน)
            ปัจจุบันให้บริการวงจรสื่อสารความเร็วสูง  ผ่านโครงข่ายเคเบิ้ลใยแก้วนำแสงที่ครอบคลุม
            ทั่วประเทศไทยและเชื่อมโยงไปยังต่างประเทศ  มีความต้องการรับสมัครพนักงานเพื่อร่วม
            เป็นส่วนหนึ่งในการเติบโตไปกับเรา ดังต่อไปนี้
            </p>
        </div><!--/.center wow-->
        <div class="row ">
            <div class="col-md-8 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
                <h2><span class="orangetext">ตำแหน่งงาน</span></h2>
                <div class="accordion">
                    <div class="panel-group" id="accordion1">
                      <div class="panel panel-default">
                        <div class="panel-heading active">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1">
                              1. Project  Manager
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>

                        <div id="collapseOne1" class="panel-collapse collapse">
                          <div class="panel-body">
                              <div class="media accordion-inner">
               <!--
                                    <div class="pull-left">
                                        <img class="img-responsive" src="images/accordion1.png">
                                    </div>
                         -->
                                    <div class="media-body">
                                        <h4><span class="orangetext">คุณสมบัติ</span></h4>
                                        <ul >
                    <li>ชาย/หญิง  อายุ 30 – 45 ปี </li>
                                            <li>วุฒิปริญญาตรี สาขาโทรคมนาคม, เทคโนโลยีสารสนเทศ, คอมพิวเตอร์ หรือสาขาที่เกี่ยวข้อง</li>
                                            <li>มีความเป็นผู้นำ ขยัน อดทน และมีมนุษย์สัมพันธ์ดี</li>
                                            <li>สามารถใช้คอมพิวเตอร์ได้เป็นอย่างดี</li>
                                            <li>สามารถใช้โปรแกรม Microsoft Project ได้เป็นอย่างดี</li>
                                            <li>สามารถ อ่าน เขียน ฟัง และพูดภาษาอังกฤษได้</li>
                                            <li>ต้องมีพาหนะส่วนตัว</li>
                                            <li>ต้องมีประสบการณ์ เรื่องการติดตั้งระบบสายสัญญาณหรือระบบเครือข่ายคอมพิวเตอร์ อย่างน้อย 2 ปี</li>
                  </ul>
                                    </div>
                              </div>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1">
                              2. Sales Executive
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>
                        <div id="collapseTwo1" class="panel-collapse collapse">
                          <div class="panel-body">
              <h4><span class="orangetext">คุณสมบัติ</span></h4>
                                 <ul>
                 <li>หญิง/ชาย</li>
                                     <li>วุฒิปริญญาตรี หรือปริญญาโท สาขาการตลาด,
                                         การขาย, การจัดการ, บริหารธุรกิจ, คอมพิวเตอร์,
                                         เทคโนโลยีสารสนเทศหรือโทรคมนาคม หรือสาขาที่เกี่ยวข้อง</li>
                                     <li>มีประสบการณ์ด้านการขายวงจรสื่อสาร(Leased Lines)  จะได้รับการพิจารณาเป็นพิเศษ</li>
                                     <li>มีประสบการณ์ในการทำงานด้านการขายโครงการ หรือการขายลูกค้ากลุ่มองค์กร อย่างน้อย 2-3ปี  จะได้รับการพิจารณาเป็นพิเศษ</li>
                                     <li>สามารถใช้งานโปรแกรมคอมพิวเตอร์ โดยเฉพาะMicrosoft Office ได้เป็นอย่างดี</li>
                                     <li>มีทักษะการนำเสนอ, ทักษะการเจรจาต่อรองและทักษะด้านการวางแผนงานขาย</li>
                                     <li>มีทักษะในการสร้างและบริหารจัดการความสัมพันธ์ระหว่างบุคคลที่ดี</li>
                                     <li>สามารถทำงานภายใต้แรงกดดันได้</li>
                                     <li>บุคลิกดี, รักงานขาย, มีหัวใจบริการและมีความคล่องตัวสูง</li>
                                     <li>สามารถอ่าน, เขียน, ฟังและพูดภาษาอังกฤษได้ดี</li>
                                     <li>มีรถยนต์เป็นของตนเองและมีใบอนุญาตขับรถยนต์ สามารถปฏิบัติงานในพื้นที่กรุงเทพและปริมณฑล</li>
                                     <li><span class="orangetext"><b>*ต้องมีใบสมัครพร้อมรูปถ่ายปัจจุบัน</b></span></li>

              </ul>
                                <h4><span class="orangetext">หน้าที่ความรับผิดชอบ</span></h4>
                                <ul>
                                    <li>พัฒนาและรักษาความสัมพันธ์อันดีกับลูกค้าใหม่และลูกค้าที่มีอยู่ของบริษัทฯ</li>
                                    <li>นำเสนอข้อมูลด้านผลิตภัณฑ์และบริการให้กับลูกค้าได้อย่างครบถ้วน</li>
                                    <li>บริหารจัดการงานขายเต็มรูปแบบ  เริ่มจากการหาลูกค้า / นำเสนอบริการและปิดงานขาย พร้อมการดูแลหลังการขาย</li>
                                    <li>รวมรวบข้อมูลและปรับปรุงระบบฐานข้อมูลลูกค้าของบริษัทฯ</li>
                                    <li>เตรียมความพร้อมสำหรับการขาย / การคาดการณ์การลงทุนและจัดทำประมาณการขาย</li>
                                </ul>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree1">
                              3. Presales
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>
                        <div id="collapseThree1" class="panel-collapse collapse">
                          <div class="panel-body">
              <h4><span class="orangetext">คุณสมบัติ</span></h4>
                                 <ul>
                 <li>ชาย/หญิง   อายุ 25 ปีขึ้นไป</li>
                                     <li>วุฒิ ปริญญาตรีหรือปริญญาโท สาขาคอมพิวเตอร์, เทคโนโลยีสารสนเทศ, โทรคมนาคม หรือ สาขาที่เกี่ยวข้อง</li>
                                     <li>มีประสบการณ์ในระบบเครือข่ายคอมพิวเตอร์ / ISP / IT / โทรคมนาคม อย่างน้อย 2 ปี</li>
                                     <li>มีทักษะในการออกแบบโซลูชั่นในด้านเครือข่าย</li>
                                     <li>มีทักษะบริหารจัดการโครงการ เพื่อโครงสร้างพื้นฐานด้านไอที (การสื่อสารการประสานงานงานทักษะการเจรจาต่อรอง)</li>
                                     <li>มีความคิดริเริ่มสร้างสรรค์  เป็นนักคิดวิเคราะห์และวางแผน  </li>
                                     <li>สามารถทำงานร่วมกับผู้อื่นได้เป็นอย่างดี   มีความรับผิดชอบ  และความเป็นผู้นำ</li>
                                     <li>สามารถฟัง พูด อ่านและเขียนภาษาอังกฤษได้ดี</li>
                                     <li>หากมีใบรับรอง CCNA,  CCNP,  CCIE   จะได้รับการพิจารณาเป็นพิเศษ</li>
              </ul>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour1">
                              4. Internal Audit Officer / Senior Internal Audit (Urgent!!!!!!!)
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>
                        <div id="collapseFour1" class="panel-collapse collapse">
                          <div class="panel-body">
              <h4><span class="orangetext">คุณสมบัติ</span></h4>
                                 <ul>
                 <li>ชาย/หญิง อายุ ไม่เกิน 35 ปี</li>
                 <li>วุฒิปริญญาตรีขึ้นไป สาขา บัญชี  บริหารธุรกิจ และสาขาอื่นที่เกี่ยวข้อง</li>
                                     <li>มีความละเอียด รอบคอบ ขยัน อดทน ชอบเรียนรู้งาน และมีความรับผิดชอบสูง</li>
                                     <li>สามารถใช้โปรแกรม MS Office ได้เป็นอย่างดี </li>
                                     <li>สามารถอ่าน เขียน ฟัง และพูด ภาษาอังกฤษได้ค่อนข้างดี</li>
                                     <li>สามารถเดินทางไปตรวจงานสาขาในต่างจังหวัดได้</li>
                                     <li>มีประสบการณ์ด้านตรวจสอบภายในอย่างน้อย 2-3 ปี</li>
              </ul>
                          </div>
                        </div>
                      </div>

                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive1">
                              5. Project  Engineer
                              <i class="fa fa-angle-right pull-right"></i>
                            </a>
                          </h3>
                        </div>
                        <div id="collapseFive1" class="panel-collapse collapse">
                          <div class="panel-body">
              <h4><span class="orangetext">คุณสมบัติ</span></h4>
                                 <ul>
                 <li>ชาย  อายุไม่เกิน 30 ปี</li>
                 <li>วุฒิปริญญาตรี สาขาโทรคมนาคม , เทคโนโลยีสารสนเทศ , คอมพิวเตอร์ หรือสาขาที่เกี่ยวข้อง</li>
                 <li>มีความเป็นผู้นำ ขยัน อดทน และมีมนุษยสัมพันธ์ดี</li>
                 <li>สามารถใช้คอมพิวเตอร์ได้เป็นอย่างดี</li>
                 <li>สามารถ อ่าน เขียน ฟัง และพูดภาษาอังกฤษได้</li>
                 <li>ต้องมีพาหนะส่วนตัว</li>
                 <li>ต้องมีประสบการณ์ เรื่องการติดตั้งระบบสายสัญญาณหรือระบบเครือข่ายคอมพิวเตอร์ อย่างน้อย 2 ปี</li>
              </ul>
                          </div>
                        </div>
                      </div>
                          <br>
                      <center>  <a href="http://203.151.143.173:8069/jobs" target = "_blank" type="button"
                        class="btn btn-primary btn-lg">สมัครงาน</a>
                      </center>
                        </div><!--/#accordion1-->
                    </div>
                </div>
                <div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                    <h2><span class="orangetext">ติดต่อได้ที่</span></h2>
                    <p>แผนกทรัพยากรบุคคล
                    <br><br>
บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) <br>
48 อาคารอินเตอร์ลิ้งค์ ซอยรุ่งเรือง  <br>
ถนนรัชดาภิเษก 20  แขวงสามเสนนอก <br>
เขตห้วยขวาง กรุงเทพฯ 10310 <br>
 <br>
เบอร์โทรศัพท์ติดต่อ   <br>
02-693-1222  (อัตโนมัติ) ต่อ 221<br>
<br>
อีเมลล์ :
<br>personnel@interlink.co.th ,
<br>jobs@interlinktelecom.co.th
                    </p>
                  </div>

            </div><!--/.row-->

 			<div class="center" >
                <p><br>
                </p>
            </div><!--/.center wow-->

        </div><!--/.container-->
    </section><!--/#content-->

<section id="middle">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 wow fadeInDown">
                    <div class="skill">
                        <h1><span class="orangetext">สวัสดิการ</span></h1>
                                     <ul>
										 <li>กองทุนสำรองเลี้ยงชีพ</li>
										 <li>เงินช่วยเหลือพนักงาน</li>
										 <li>ค่ากะ</li>
										 <li>เบี้ยภาระงานและเบี้ยขยัน</li>
										 <li>ตรวจสุขภาพประจำปี</li>
										 <li>เครื่องแบบพนักงาน</li>
										 <li>ประกันชีวิต ประกันอุบัติเหตุ และประกันสุขภาพ</li>
										 <li>สหกรณ์ออมทรัพย์</li>
										 <li>การปรับอัตราจ้างประจำปี การจ่ายโบนัส </li>
										 <li>การฝึกอบรม</li>
										 <li>ประกันสังคม </li>
									</ul>
                </div><!--/.col-sm-6-->

            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#middle-->



<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/footer.php";
		include_once($path) ;
		?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
