﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Interlink Telecom - News & Events </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

     <!-- Google Analytic Website tracking-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-57997984-1', 'auto');
        ga('send', 'pageview');

    </script>
 <!--/Google Analytic-->
</head>
<body id="pageBody">
<!-- include header.php -->
        <?php
        $path = $_SERVER['DOCUMENT_ROOT'];
        $path .= "/th/php/header.php";
        include_once($path) ;
        ?>
<!--/end  php -->
<body>
 <section id="portfolio">
        <div class="container">
            <div class="center">
               <h2>News & Events</h2>
            </div>
<hr>

            <div class="row">
                <div class="col-md-10">
                <div class="portfolio-items">


    <div class="portfolio-item 1601  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160106/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">งานแถลงข่าวเปิดตัว PlanetFiber บริการอินเทอร์เน็ตความเร็วแสงผ่านสายเคเบิ้ลใยแก้วนำแสง</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160106/img01.jpg" rel="prettyPhoto[1601]"
                                        title="
คุณศรัญญา กาญจนโอภาส (ที่ 2 จากขวา) Deputy Sale Director บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้เข้าร่วมแสดงความยินดีกับคุณประพัฒน์ รัฐเลิศกานต์ (ที่ 3 จากขวา) กรรมการผู้อำนวยการและหัวหน้าเจ้าหน้าที่บริหารบริษัท แพลนเน็ต คอมมิวนิเคชั่น เอเชีย จำกัด (มหาชน) ในงานแถลงข่าวเปิดตัว PlanetFiber บริการอินเทอร์เน็ตความเร็วแสงผ่านสายเคเบิ้ลใยแก้วนำแสง Fiber To The Room (FTTR) ให้บริการในอาคารสูงรายแรกของไทย จัดขึ้น ณ บริษัท แพลนเน็ต คอมมิวนิเคชั่น เอเชีย จำกัด (มหาชน) เมื่อวันที่ 28 มกราคม ที่ผ่านมา
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160106/img01.jpg" rel="prettyPhoto[1601]" title="งานแถลงข่าวเปิดตัว PlanetFiber บริการอินเทอร์เน็ตความเร็วแสงผ่านสายเคเบิ้ลใยแก้วนำแสง"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1603  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160303/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ศึกษาดูงาน ศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ดาต้า เซ็นเตอร์</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160303/img01.jpg" rel="prettyPhoto[1603]"
                                        title="
คุณชาลี ไชยรัตนตรัย ผู้จัดการทั่วไปศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ดาต้า เซ็นเตอร์ เป็นตัวแทนของบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน) ให้การต้อนรับอาจารย์และนักศึกษาจากคณะวิทยาศาสตร์และเทคโนโลยี หลักสูตรวิทยาการคอมพิวเตอร์และเทคโนโลยีสารสนเทศ มหาวิทยาลัยราชภัฎกำแพงเพชร เพื่อเข้ามาศึกษาดูงาน ณ ศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ที่มีความปลอดภัยและทันสมัยได้มาตรฐาน ISO/IEC 27001:2013 โดยคณะนักศึกษาที่มาดูงานจะเรียนรู้นวัตกรรมที่ทันสมัยของบริษัทฯ และได้สัมผัสประสบการณ์การทำงานจริง ถือเป็นอีกช่องทางในการพัฒนาความรู้และส่งเสริมการศึกษาให้แก่เยาวชนไทย เมื่อวันที่ 29 มีนาคม 2559
">
                                    <i class="fa fa-eye"></i> View More</a> เมื่อวันที่ 29 มีนาคม 2559
                                    <a class="preview" href="portfolio/160303/img01.jpg" rel="prettyPhoto[1603]" title="ศึกษาดูงาน ศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/160303/img02.jpg" rel="prettyPhoto[1603]" title="ศึกษาดูงาน ศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/160303/img03.jpg" rel="prettyPhoto[1603]" title="ศึกษาดูงาน ศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/160303/img04.jpg" rel="prettyPhoto[1603]" title="ศึกษาดูงาน ศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/160303/img05.jpg" rel="prettyPhoto[1603]" title="ศึกษาดูงาน ศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/160303/img06.jpg" rel="prettyPhoto[1603]" title="ศึกษาดูงาน ศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/160303/img07.jpg" rel="prettyPhoto[1603]" title="ศึกษาดูงาน ศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ดาต้า เซ็นเตอร์"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1604  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160402/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">สวัสดีปีใหม่ไทย :Giving & Sharing Together</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160402/img01.jpg" rel="prettyPhoto[1604]"
                                        title="
บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้จัดกิจกรรมสวัสดีปีใหม่ไทย :Giving & Sharing Together คำว่าให้…ไม่มีที่สิ้นสุด โดยการแจกชุดสังฆทานออร์แกนิคให้กับลูกค้าเพื่อนำไปถวายแด่พระภิกษุสงฆ์ในช่วงเทศกาลสงกรานต์ และเมื่อวันที่ 18 เมษายน 2559 ที่ผ่านมา ทางบริษัทฯ ได้เป็นตัวแทนลูกค้าในการทำบุญถวายอาหารและชุดสังฆทานออร์แกนิคแด่พระภิกษุสงฆ์ที่อาพาธ ณ โรงพยาบาลสงฆ์ ถนนศรีอยุธยา หวังเป็นอย่างยิ่งว่าอานิสงค์จากการทำบุญในครั้งนี้จะส่งผลให้ลูกค้าของอินเตอร์ลิ้งค์ เทเลคอมทุกท่านมีความสุขและมีสุขภาพที่แข็งแรงตลอดไป
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160402/img01.jpg" rel="prettyPhoto[1604]" title="สวัสดีปีใหม่ไทย :Giving & Sharing Together"></a>
                                    <a class="preview" href="portfolio/160402/img03.jpg" rel="prettyPhoto[1604]" title="สวัสดีปีใหม่ไทย :Giving & Sharing Together"></a>
                                    <a class="preview" href="portfolio/160402/img04.jpg" rel="prettyPhoto[1604]" title="สวัสดีปีใหม่ไทย :Giving & Sharing Together"></a>
                                    <a class="preview" href="portfolio/160402/img05.jpg" rel="prettyPhoto[1604]" title="สวัสดีปีใหม่ไทย :Giving & Sharing Together"></a>
                                    <a class="preview" href="portfolio/160402/img06.jpg" rel="prettyPhoto[1604]" title="สวัสดีปีใหม่ไทย :Giving & Sharing Together"></a>
                                    <a class="preview" href="portfolio/160402/img07.jpg" rel="prettyPhoto[1604]" title="สวัสดีปีใหม่ไทย :Giving & Sharing Together"></a>
                                    <a class="preview" href="portfolio/160402/img08.jpg" rel="prettyPhoto[1604]" title="สวัสดีปีใหม่ไทย :Giving & Sharing Together"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1602  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160201/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">งานสัมมนา Road to the Star</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160201/img01.jpg" rel="prettyPhoto[1602]"
                                        title="คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้ร่วมเป็นวิทยากรในงานสัมมนา  Road to the Star  ภายใต้หัวข้อ อนาคตสดใส อนาคตหุ้น Digital Economy โดยมีกลุ่มผู้ประกอบการและนักลงทุนเข้าร่วมรับฟังข้อมูลเป็นจำนวนมาก จัดขึ้น ณ อาคาร Now 26 สยามสแควร์ เมื่อวันที่ 10 กุมภาพันธ์ 2559 ที่ผ่านมา
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160201/img01.jpg" rel="prettyPhoto[1602]" title="งานสัมมนา Road to the Star"></a>
                                    <a class="preview" href="portfolio/160201/img02.jpg" rel="prettyPhoto[1602]" title="งานสัมมนา Road to the Star"></a>
                                    <a class="preview" href="portfolio/160201/img03.jpg" rel="prettyPhoto[1602]" title="งานสัมมนา Road to the Star"></a>
                                    <a class="preview" href="portfolio/160201/img04.jpg" rel="prettyPhoto[1602]" title="งานสัมมนา Road to the Star"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1601  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160105/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Happy New Year 2016</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160105/img01.jpg" rel="prettyPhoto[1601]"
                                        title="
เนื่องในโอกาสส่งท้ายปีเก่าต้อนรับปีใหม่ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) นำโดยคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน)และทีมงานเข้าไปพบปะพูดคุยกับลูกค้าพร้อมมอบของขวัญเพื่อสร้างสีสันในธีม Happy New Year 2016 แทนคำขอบคุณที่ไว้วางใจใช้บริการของอินเตอร์ลิ้งค์ เทเลคอม มาอย่างต่อเนื่อง ซึ่งบรรยากาศเต็มไปด้วยรอยยิ้มแห่งความสุขกันถ้วนหน้า
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160105/img02.jpg" rel="prettyPhoto[1601]" title="Happy New Year 2016"></a>
                                    <a class="preview" href="portfolio/160105/img03.jpg" rel="prettyPhoto[1601]" title="Happy New Year 2016"></a>
                                    <a class="preview" href="portfolio/160105/img04.jpg" rel="prettyPhoto[1601]" title="Happy New Year 2016"></a>
                                    <a class="preview" href="portfolio/160105/img05.jpg" rel="prettyPhoto[1601]" title="Happy New Year 2016"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1602  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160202/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Relax and Refresh Thank you Party</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160202/img01.jpg" rel="prettyPhoto[1602]"
                                        title="เพื่อเป็นการขอบคุณลูกค้าบริษัท ทรู อินเทอร์เน็ต จำกัด ที่ใช้บริการวงจรสื่อสารความเร็วสูงอย่างต่อเนื่องตลอดมา บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน)ได้จัดงานเลี้ยงคาราโอเกะปาร์ตี้เพื่อกระชับความสัมพันธ์ ร่วมพูดคุย สังสรรค์ภายใต้ชื่องาน “Relax and Refresh Thank you Party” ณ ร้าน Share Sea Bar & Restaurant สาขา พระราม 9 โดยมีคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด( มหาชน) เป็นประธานกล่าวต้อนรับอย่างเป็นกันเอง ซึ่งกิจกรรมในครั้งนี้ได้มีการรับประทานอาหารร่วมกันและแข่งขันร้องเพลง เล่นเกมรับของรางวัลกลับบ้านกันมากมาย">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160202/img01.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img02.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img03.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img04.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img05.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img06.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img07.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img08.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img09.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img10.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img11.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img12.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img13.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img14.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img15.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img16.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img17.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img18.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img19.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img20.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img21.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>
                                    <a class="preview" href="portfolio/160202/img22.jpg" rel="prettyPhoto[1602]" title="Relax and Refresh Thank you Party"></a>

                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1601  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160104/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Data Center Dynamics</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160104/img01.jpg" rel="prettyPhoto[1601]"
                                        title="
คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้ร่วมเป็นวิทยากรในงาน Data Center Dynamics ภายใต้หัวข้อ FOCUS ON THAILAND MOVING TOWARDS A CLOUD POWERED DIGITAL ECONOMY โดยมีผู้สนใจทั้งชาวไทยและชาวต่างประเทศเข้าร่วมฟังบรรยายเป็นจำนวนมาก งานในครั้งนี้มีวัตถุประสงค์เพื่อให้ผู้เข้าร่วมงานสัมมนาได้รับทราบข้อมูลที่เป็นประโยชน์และอัพเดทเทคโนโลยีของดาต้า เซ็นเตอร์ อันจะช่วยเพิ่มเสถียรภาพและประสิทธิภาพสูงสุดให้กับการทำงานขององค์กร จัดขึ้น ณ Swissotel Le Concorde Hotel เมื่อวันที่ 28 มกราคม ที่ผ่านมา
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160104/img02.jpg" rel="prettyPhoto[1601]" title="Data Center Dynamics"></a>
                                    <a class="preview" href="portfolio/160104/img03.jpg" rel="prettyPhoto[1601]" title="Data Center Dynamics"></a>
                                    <a class="preview" href="portfolio/160104/img04.jpg" rel="prettyPhoto[1601]" title="Data Center Dynamics"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1609  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160901/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ITEL ให้ข้อมูลงานสัมมนา Road to the Star the Vision of 2017</span></a></h3>
                                    <p></p>
                                    <a class="preview"
                                        href="portfolio/160901/img01.jpg"
                                        rel="prettyPhoto[1609]"
                                        title="
นายณัฐนัย อนันตรัมพร (ที่2จากขวา) กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน) หรือ ITEL ผู้ให้บริการโครงข่ายใยแก้วนำแสง ให้บริการติดตั้งโครงข่าย และให้บริการพื้นที่ศูนย์สำรองข้อมูลหรือดาต้าเซ็นเตอร์ (Data Center) ถ่ายภาพเป็นที่ระลึกร่วมกับนายจักรกริช เจริญเมธาชัย (ขวา) กรรมการผู้จัดการอาวุโส ผู้บริหารสูงสุดสายบริหารการลงทุนส่วนบุคคล บริษัทหลักทรัพย์ เคทีบี (ประเทศไทย) จำกัด หรือ KTBST และผู้บริหารระดับสูงของบริษัท มิลล์คอน สตีล จำกัด (มหาชน) และบริษัท กันกุลเอ็นจิเนียริ่ง จำกัด (มหาชน) ภายหลังการเข้าร่วมให้ข้อมูลในงานสัมมนา Road to the Star the Vision of 2017 ณ โรงแรม Grand Centre Point Terminal21 โดยมีนักลงทุนให้ความสนใจและซักถามอย่างใกล้ชิด เมื่อเร็วๆนี้
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160901/img01.jpg"
                                        rel="prettyPhoto[1609]"
                                        title=" ITEL ให้ข้อมูลงานสัมมนา Road to the Star the Vision of 2017"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1605  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160503/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">I-Tel Exclusive Experience : Laser Game</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160503/img01.jpg" rel="prettyPhoto[1605]"
                                        title="
ครั้งแรกกับการจัดกิจกรรม I-Tel Exclusive Experience : Laser Game ที่ทางอินเตอร์ลิ้งค์ เทเลคอม จัดขึ้นเพื่อเป็นการขอบคุณลูกค้าที่ใช้บริการวงจรสื่อสารความเร็วสูงจากทาง อินเตอร์ลิ้งค์ เทเลคอม นำโดยคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) พร้อมทีมงานต้อนรับลูกค้าอย่างเป็นกันเอง เพื่อเปิดประสบการณ์ในแบบ Adventure โดยมีการแบ่งทีมแข่งขันยิงปืน Laser Games สร้างความสนุกสนานและคลายความเครียดจากการทำงานได้เป็นอย่างดี หลังจากนั้นทุกท่านได้ร่วมทานอาหารค่ำและสังสรรค์ในบรรยากาศแบบเป็นกันเอง ณ ห้องอาหาร เซต้า คาเฟ่ (Zeta Cafe) โรงแรมฮอลิเดย์อินน์ ปิดท้ายความประทับใจด้วยการแจกของรางวัลต่างๆ มากมาย กิจกรรมสนุกๆ อย่างนี้จัดขึ้นเมื่อวันพฤหัสบดีที่ 26 พฤษภาคม 2559 ที่ผ่านมา
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160503/img02.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img03.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img04.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img05.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img06.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img07.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img08.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img09.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img10.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img11.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img12.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img13.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img14.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                    <a class="preview" href="portfolio/160503/img15.jpg" rel="prettyPhoto[1605]" title="I-Tel Exclusive Experience : Laser Game"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->


                    <div class="portfolio-item 1607  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160701/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Exclusive Relax Time</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160701/img01.jpg" rel="prettyPhoto[1607]"
                                        title="
เพื่อเป็นการขอบคุณลูกค้า บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้จัดกิจกรรม “Exclusive relax time “ เชิญชวนลูกค้าผู้มีอุปการะคุณมาผ่อนคลายด้วยการพาไปสัมผัสบรรยากาศสปาแบบญี่ปุ่นกับการแช่ Onsen ต้นฉบับแท้ๆจากญี่ปุ่นแบบส่วนตัวที่“ kashikiri soda onsen and spa “ น้ำแร่ที่นำมาบริการจะเป็นโซดาออนเซน ซึ่งเป็นนวัตกรรมล่าสุดในการดูแล ฟื้นฟู สุขภาพและผิวพรรณ ซึ่งหลังจากแช่ออนเซนแล้วจะมีการนวดผ่อนคลายความเมื่อยล้าจากการทำงานให้ลูกค้าผู้มีอุปการะคุณ สร้างประสบการณ์ใหม่ในการพักผ่อนทำให้ลูกค้าทุกท่านลืมความเหนื่อยล้าจากการทำงานมาตลอดสัปดาห์ หลังจากแช่ออนเซ็นและนวดแล้ว ลูกค้าผู้มีอุปการะคุณทุกท่านยังได้สัมผัสกับการจิบชาแบบญี่ปุ่น และเป็นโอกาสอันดีในการพบปะพูดคุยกันแบบใกล้ชิดกับทีมงานของอินเตอร์ลิ้งค์ เทเลคอม จัดขึ้นเมื่อวันเสาร์ที่ 9 กรกฎาคม 2559
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160701/img02.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                    <a class="preview" href="portfolio/160701/img03.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                    <a class="preview" href="portfolio/160701/img04.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                    <a class="preview" href="portfolio/160701/img05.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                    <a class="preview" href="portfolio/160701/img06.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                    <a class="preview" href="portfolio/160701/img07.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                    <a class="preview" href="portfolio/160701/img08.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                    <a class="preview" href="portfolio/160701/img09.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                    <a class="preview" href="portfolio/160701/img10.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                    <a class="preview" href="portfolio/160701/img11.jpg" rel="prettyPhoto[1607]" title="Exclusive Relax Time"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1601  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160103/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Charming Party in Wonderland By Interlink Telecom</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160103/img01.jpg" rel="prettyPhoto[1601]"
                                        title="
เพื่อเป็นการขอบคุณพันธมิตรทางธุรกิจที่ดีต่อกันมาอย่างยาวนาน บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้จัดงานเลี้ยงขอบคุณลูกค้า บริษัท อินเทอร์เน็ต ประเทศไทย จำกัด (มหาชน) หรือ INET ภายใต้ชื่องาน “Charming Party in Wonderland By Interlink Telecom” เพื่อกระชับความสัมพันธ์และมอบความสนุกสนานแก่ลูกค้า โดยมีคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) เป็นประธานกล่าวเปิดงาน ซึ่งพนักงานจากทั้ง 2 บริษัทได้มีโอกาสร่วมรับประทานอาหารพร้อมกันและเล่นเกมเพื่อรับของรางวัลกันอย่างมากมาย ไม่ว่าจะเป็น Apple Watch , ipad mini และ Mini Concert จากคุณปราโมทย์ ปาทาน ที่มาร่วมสร้างสีสันและความบันเทิงอย่างเป็นกันเอง เมื่อวันที่ 21 มกราคม 2559 ณ ร้าน The Hatter อโศก
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160103/img02.jpg" rel="prettyPhoto[1601]" title="Charming Party in Wonderland By Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/160103/img03.jpg" rel="prettyPhoto[1601]" title="Charming Party in Wonderland By Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/160103/img04.jpg" rel="prettyPhoto[1601]" title="Charming Party in Wonderland By Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/160103/img05.jpg" rel="prettyPhoto[1601]" title="Charming Party in Wonderland By Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/160103/img06.jpg" rel="prettyPhoto[1601]" title="Charming Party in Wonderland By Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/160103/img07.jpg" rel="prettyPhoto[1601]" title="Charming Party in Wonderland By Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/160103/img08.jpg" rel="prettyPhoto[1601]" title="Charming Party in Wonderland By Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/160103/img09.jpg" rel="prettyPhoto[1601]" title="Charming Party in Wonderland By Interlink Telecom"></a>

                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1603  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160301/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ITO Solution Day 2016</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160301/img01.jpg" rel="prettyPhoto[1603]"
                                        title="คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้ร่วมเป็นวิทยากรในงาน “ITO Solution Day 2016” บรรยายในหัวข้อ “Internet of Things” จัดขึ้นโดย บริษัท เบทาโกร จำกัด (มหาชน) เมื่อวันศุกร์ที่ 4 มีนาคม 2559 ณ โรงแรม มิราเคิล แกรนด์ คอนเวนชั่น งานในครั้งนี้มีวัตถุประสงค์เพื่อให้ผู้เข้าร่วมงานสัมมนาได้รับทราบข้อมูลที่เป็นประโยชน์และอัพเดทเทคโนโลยีใหม่ๆ อันจะช่วยเพิ่มเสถียรภาพและประสิทธิภาพสูงสุดให้กับการทำงานขององค์กร
                                         ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160301/img02.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                    <a class="preview" href="portfolio/160301/img03.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                    <a class="preview" href="portfolio/160301/img04.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                    <a class="preview" href="portfolio/160301/img05.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                    <a class="preview" href="portfolio/160301/img06.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                    <a class="preview" href="portfolio/160301/img07.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                    <a class="preview" href="portfolio/160301/img08.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                    <a class="preview" href="portfolio/160301/img09.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                    <a class="preview" href="portfolio/160301/img10.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                    <a class="preview" href="portfolio/160301/img11.jpg" rel="prettyPhoto[1603]" title="ITO Solution Day 2016"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1510  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/151006/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Thank You Party For TMB</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/151006/img01.jpg" rel="prettyPhoto[1510]"
                                        title="
เพื่อเป็นการขอบคุณลูกค้าธนาคารทหารไทยที่ให้การสนับสนุนบริษัทฯ ด้วยดีเสมอมาและพร้อมที่จะเติบโตก้าวไปด้วยกันต่อไป บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน)และศูนย์สำรองข้อมูล อินเตอร์ลิ้งค์ ดาต้าเซ็นเตอร์ได้จัดงานเลี้ยง ขอบคุณลูกค้าภายใต้ชื่องาน “Thank You Party For TMB”
ณ ร้าน Marlin cafe โดยมีคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด( มหาชน) เป็นประธานกล่าวเปิดงาน ภายในงานคุณณัฐนัย อนันตรัมพร ได้โชว์ฝีมือในการทำอาหารญี่ปุ่น “ราเมนไข่ออนเซ็น” เมนูพิเศษที่สร้างสรรค์เพื่อลูกค้าธนาคารทหารไทย ซึ่งบรรยากาศเป็นไปด้วยความสนุกสนานและเป็นกันเองอย่างยิ่ง
">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/151006/img02.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img03.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img04.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img05.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img06.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img07.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img08.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img09.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img10.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img11.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img12.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img13.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                    <a class="preview" href="portfolio/151006/img14.jpg" rel="prettyPhoto[1510]" title="Thank You Party For TMB"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1609  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160902/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">พิธีเปิด CNS Convention Center และโครงการ The Angel Biz Challenge (ABC)</span></a></h3>
                                    <p></p>
                                    <a class="preview"
                                        href="portfolio/160902/img01.jpg"
                                        rel="prettyPhoto[1609]"
                                        title="
คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้เข้าร่วมพิธีเปิด CNS Convention Center และโครงการ The Angel Biz Challenge (ABC) โครงการที่เกิดขึ้นจากความร่วมมือของ 4 หน่วยงาน คือ บริษัทหลักทรัพย์ โนมูระ พัฒนสิน จำกัด (มหาชน) (CNS) ตลาดหลักทรัพย์แห่งประเทศไทย (SET) ธนาคารพัฒนาวิสาหกิจขนาดกลาง และขนาดย่อมแห่งประเทศไทย (ธพว. หรือ SME BANK) และ มหาวิทยาลัยหอการค้าไทย (UTCC) เพื่อส่งเสริม สนับสนุนและบ่มเพาะผู้ประกอบการในประเทศไทย อีกทั้งยังเป็นเวทีในการนำเสนอ ความคิดสร้างสรรค์ ไอเดียทางธุรกิจใหม่ จัดขึ้น ณ CNS Convention Center
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160902/img01.jpg"
                                        rel="prettyPhoto[1609]"
                                        title="พิธีเปิด CNS Convention Center และโครงการ The Angel Biz Challenge (ABC)"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1602  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160203/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">RELAX & REFRESH LUNCHEON</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160203/img01.jpg" rel="prettyPhoto[1602]"
                                        title="เพื่อเป็นการขอบคุณลูกค้าและกระชับความสัมพันธ์อันดีกับบริษัท ทรู ยูนิเวอร์แซล คอนเวอร์เจ้นซ์ จำกัด ที่ไว้วางใจใช้บริการวงจรสื่อสารความเร็วสูงของ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ตลอดมา บริษัทฯได้จัดงานเลี้ยงขอบคุณลูกค้าภายใต้ชื่องาน ” RELAX & REFRESH LUNCHEON” โดยมีคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด( มหาชน) เป็นประธานกล่าวต้อนรับอย่างเป็นกันเอง ภายในงานมีการรับประทานอาหารกลางวันร่วมกันอย่างอบอุ่นและยังเป็นโอกาสดีที่ได้ร่วมพบปะพูดคุยกันอย่างใกล้ชิดเป็นกันเอง เมื่อวันพฤหัสบดีที่ 18 กุมภาพันธ์ 2559 ณ ห้องอาหารญี่ปุ่นอาเกฮัง โรงแรม Grand Mercure Fortune">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160203/img02.jpg" rel="prettyPhoto[1602]" title="RELAX & REFRESH LUNCHEON"></a>
                                    <a class="preview" href="portfolio/160203/img03.jpg" rel="prettyPhoto[1602]" title="RELAX & REFRESH LUNCHEON"></a>
                                    <a class="preview" href="portfolio/160203/img04.jpg" rel="prettyPhoto[1602]" title="RELAX & REFRESH LUNCHEON"></a>
                                    <a class="preview" href="portfolio/160203/img05.jpg" rel="prettyPhoto[1602]" title="RELAX & REFRESH LUNCHEON"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1608  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160801/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอม หรือ ITEL แถลงข่าวความพร้อมเข้าจดทะเบียนในตลาดหลักทรัพย์เอ็ม เอ ไอ</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160801/img01.jpg" rel="prettyPhoto[1608]"
                                        title="
คุณณัฐนัย อนันตรัมพร (ที่ 3 จากซ้าย) กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) หรือ ITEL ผู้ให้บริการโครงข่ายใยแก้วนำแสง ให้บริการติดตั้งโครงข่าย และให้บริการพื้นที่ศูนย์สำรองข้อมูลหรือดาต้าเซ็นเตอร์ (Data Center) พร้อมด้วยคุณสมภพ กีระสุนทรพงษ์ (ที่ 2 จากขวา) กรรมการผู้อำนวยการ บริษัทหลักทรัพย์ ฟินันเซีย ไซรัส จำกัด (มหาชน) หรือ FSS ในฐานะที่ปรึกษาทางการเงิน และผู้จัดการการจัดจำหน่ายและรับประกันการจำหน่ายหุ้นสามัญเพิ่มทุน และยังได้รับเกียรติจากคุณประพันธ์ เจริญประวัติ (ที่3จากขวา) ผู้จัดการตลาดหลักทรัพย์ เอ็ม เอ ไอ (mai) ถ่ายภาพเป็นที่ระลึก ภายหลังบมจ.อินเตอร์ลิ้งค์ เทเลคอม หรือ ITEL แถลงข่าวความพร้อมเข้าจดทะเบียนในตลาดหลักทรัพย์เอ็ม เอ ไอ เพื่อเสนอขายหุ้นไอพีโอ 200 ล้านหุ้น โดยเงินระดมทุนที่ได้นำไปใช้ในการขยายบริการพื้นที่ดาต้าเซ็นเตอร์ ขยายโครงข่ายใยแก้วนำแสง ชำระคืนเงินกู้และใช้เป็นเงินทุนหมุนเวียน คาดเข้าจดทะเบียนได้ภายในไตรมาส 3 ปีนี้ ณ ห้องประชุม 603 อาคารตลาดหลักทรัพย์แห่งประเทศไทย ที่ผ่านมา
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160801/img01.jpg" rel="prettyPhoto[1608]" title="อินเตอร์ลิ้งค์ เทเลคอม หรือ ITEL แถลงข่าวความพร้อมเข้าจดทะเบียนในตลาดหลักทรัพย์เอ็ม เอ ไอ"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1601  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160102/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">แพลนเน็ตคอม - อินเตอร์ลิ้งค์ เทเลคอม ลงนาม MOU เพื่อพัฒนาการให้บริการอินเทอร์เน็ต</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160102/img01.jpg" rel="prettyPhoto[1601]"
                                        title="
นายประพัฒน์  รัฐเลิศกานต์  (ที่ 4 จากซ้าย) กรรมการผู้อำนวยการและหัวหน้าเจ้าหน้าที่บริหาร บริษัท   แพลนเน็ต คอมมิวนิเคชั่น เอเชีย จำกัด (มหาชน) หรือ PCA  และนายณัฐนัย  อนันตรัมพร (ที่ 3 จากขวา) กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์  เทเลคอม จำกัด (มหาชน) ถ่ายภาพร่วมกันเนื่องในวาระการลงนามบันทึกข้อตกลงความร่วมมือ (MOU) เพื่อร่วมกันพัฒนาต่อยอดธุรกิจการให้บริการอินเทอร์เน็ตความเร็วสูงในโครงการอาคารชุดพักอาศัยและอาคารสำนักงาน ทั้งนี้ พิธีลงนามได้จัดขึ้น ณ อาคารอินเตอร์ลิ้งค์ ถ.รัชดาภิเษก เมื่อเร็วๆนี้
">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160102/img01.jpg" rel="prettyPhoto[1601]" title="แพลนเน็ตคอม - อินเตอร์ลิ้งค์ เทเลคอม ลงนาม MOU เพื่อพัฒนาการให้บริการอินเทอร์เน็ต"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1509  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150901/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150901/img01.jpg" rel="prettyPhoto[1509]"
                                        title="
                                        คณะผู้บริหารและพนักงานกลุ่มบริษัทอินเตอร์ลิ้งค์ร่วมทำบุญประจำปี 2558 ตักบาตรข้าวสารอาหารแห้ง ถวายแด่คณะสงฆ์วัดพระราม 9 กาญจนาภิเษก พร้อมถวายจตุปัจจัยไทยทาน ภัตตาหารเพล และฟังธรรมบรรยายหัวข้อ “คนดี...คนเก่ง” ทั้งนี้นับเป็นโอกาสพิเศษเนื่องในโอกาสครบรอบวันคล้ายวันเกิด คุณสมบัติ อนันตรัมพร ประธานกรรมการ กลุ่มบริษัทอินเตอร์ลิ้งค์ เมื่อวันที่ 8 กันยายนที่ผ่านมา
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150901/img02.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img03.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img04.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img05.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img06.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img07.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img08.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img09.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img10.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img11.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img12.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150901/img13.jpg" rel="prettyPhoto[1509]" title="งานทำบุญประจำปี 2558 ของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1606  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160604/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Interlink Telecom Privilege Trip : Your Singapore</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160604/img01.jpg" rel="prettyPhoto[1606]"
                                        title="
ร่วมเป็นส่วนหนึ่งในการค้นพบศักยภาพด้านการสื่อสารโทรคมนาคมและเตรียมความพร้อมในการก้าวเข้าสู่ยุคดิจิทัล บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้จัดกิจกรรม Interlink Telecom Privilege Trip : Your Singapore พาลูกค้าผู้มีอุปการคุณที่เป็นพันธมิตรของบริษัทฯกว่า 40 ท่าน บินลัดฟ้าโดยสายการบินไทยเพื่อเข้าร่วมงาน Communic Asia 2016 ‘Connecting The Future Now’ ณ Marina Bay Sands ประเทศสิงคโปร์ ระหว่างวันที่ 1-3 มิถุนายน 2559 ประกอบไปด้วย 2 งานสำคัญ Broadcast Asia 2016 ภายใต้ธีมการประยุกต์ใช้เทคโนโลยีในการถ่ายทอดคอนเทนต์ Communic Asia 2016 ภายใต้ธีมการเชื่อมโยงสู่โลกอนาคต โดยมีผู้ประกอบการชั้นนำมาจัดแสดงบูธเพื่อการต่อยอดธุรกิจตามแนวโน้มเทคโนโลยีสมาร์ทซิตี้ และสมาร์ทเอ็นเตอร์ไพรซ์ รวมทั้งท่องเที่ยวสัมผัสกับแหล่งท่องเที่ยวที่ทันสมัยไม่ว่าจะเป็น Typhoon Theater , Universal Studio, S.E.A Aquarium และการแสดงสุดอลังการ Wing of Time ช้อปปิ้งสินค้าหลากหลายที่ถนนออร์ชาร์ด ภายในทริปครั้งนี้นอกจากจะได้อัพเดทข้อมูลล่าสุดด้านเทคโนโลยีการสื่อสารที่เพื่อใช้ในการต่อยอดธุรกิจแล้ว ลูกค้ายังได้มีโอกาสพบปะ พูดคุยกับคุณณัฐนัย อนันตรัมพร ผู้บริหารของอินเตอร์ลิ้งค์ เทเลคอม และทีมงานที่ให้การต้อนรับและดูแลลูกค้าอย่างอบอุ่น
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160604/img02.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                    <a class="preview" href="portfolio/160604/img03.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                    <a class="preview" href="portfolio/160604/img04.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                    <a class="preview" href="portfolio/160604/img05.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                    <a class="preview" href="portfolio/160604/img06.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                    <a class="preview" href="portfolio/160604/img07.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                    <a class="preview" href="portfolio/160604/img08.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                    <a class="preview" href="portfolio/160604/img09.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                    <a class="preview" href="portfolio/160604/img10.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                    <a class="preview" href="portfolio/160604/img11.jpg" rel="prettyPhoto[1606]" title="Interlink Telecom Privilege Trip : Your Singapore"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1602  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160204/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Relax and Refresh Dinner</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160204/img01.jpg" rel="prettyPhoto[1602]"
                                        title="บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้จัดงานเลี้ยงขอบคุณลูกค้าบริษัท ทรู อินเทอร์เน็ต จำกัด เพื่อกระชับความสัมพันธ์และมอบความสนุกสนานแก่ลูกค้าที่ไว้วางใจบริการของบริษัทเสมอมา ภายใต้ชื่องาน Relax and Refresh Dinner โดยภายในงานมีการรับประทานอาหารร่วมกันอย่างอบอุ่น และยังเป็นโอกาสดีที่ได้ร่วมพบปะ พูดคุยกันอย่างใกล้ชิด เมื่อวันพฤหัสบดีที่ 24 กุมภาพันธ์ 2559 ที่ผ่านมา ณ โรงแรม Swissotel Le Concorde ห้องอาหารญี่ปุ่น TAKUMI">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160204/img02.jpg" rel="prettyPhoto[1602]" title="RELAX & REFRESH LUNCHEON"></a>
                                    <a class="preview" href="portfolio/160204/img03.jpg" rel="prettyPhoto[1602]" title="RELAX & REFRESH LUNCHEON"></a>
                                    <a class="preview" href="portfolio/160204/img04.jpg" rel="prettyPhoto[1602]" title="RELAX & REFRESH LUNCHEON"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1609  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160903/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ITEL เปิดเทรดวันแรก ราคาเปิดบวกกว่า 40% จากราคา IPO 5.20 บ. ต่อหุ้น</span></a></h3>
                                    <p></p>
                                    <a class="preview"
                                        href="portfolio/160903/img01.jpg"
                                        rel="prettyPhoto[1609]"
                                        title="
นายปกรณ์ มาลากุล ณ อยุธยา(ที่4จากซ้าย) ประธานกรรมการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน) หรือ ITEL และนายณัฐนัย อนันตรัมพร (ที่3จากขวา) กรรมการผู้จัดการ พร้อมด้วยนายสมบัติ อนันตรัมพร(ที่3จากซ้าย) รองประธานกรรมการ และนางชลิดา อนันตรัมพร(ที่2จากซ้าย) กรรมการ ถ่ายภาพเป็นที่ระลึกร่วมกับดร.สันติ กีระนันท์ (ที่2จากขวา) รองผู้จัดการ ตลาดหลักทรัพย์แห่งประเทศไทย และคุณสมภพ กีระสุนทรพงษ์(ขวา) กรรมการผู้อำนวยการ บริษัทหลักทรัพย์ฟินันเซีย ไซรัส จำกัด(มหาชน) ในฐานะที่ปรึกษาทางการเงินและผู้จัดการการจัดจำหน่ายและรับประกันการจำหน่าย ในโอกาสที่ ITEL เข้าซื้อขายในตลาดหลักทรัพย์เอ็ม เอไอเป็นวันแรก โดยราคาหุ้น ITEL เปิดการซื้อขายอยู่ที่ 7.50 บาท จากราคาไอพีโอหุ้นละ 5.20 บาท ให้ผลตอบแทนต่อนักลงทุนกว่า40% ณ หอประชุมศุกรีย์ แก้วเจริญ ชั้น 3 อาคารตลาดหลักทรัพย์แห่งประเทศไทย
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160903/img01.jpg"
                                        rel="prettyPhoto[1609]"
                                        title="ITEL เปิดเทรดวันแรก ราคาเปิดบวกกว่า 40% จากราคา IPO 5.20 บ. ต่อหุ้น"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1607  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160702/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอม (ITEL)  เปิดบ้านต้อนรับนักวิเคราะห์</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160702/img01.jpg" rel="prettyPhoto[1607]"
                                        title="
นายณัฐนัย  อนันตรัมพร   กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) หรือ ITEL  ผู้ให้บริการโครงข่ายใยแก้วนำแสง ให้บริการติดตั้งโครงข่าย และให้บริการพื้นที่ศูนย์สำรองข้อมูลหรือดาต้าเซ็นเตอร์ (Data Center)  พร้อมด้วยนายสมภพ กีระสุนทรพงษ์  กรรมการผู้อำนวยการ   บริษัทหลักทรัพย์ ฟินันเซีย ไซรัส จำกัด (มหาชน)  ในฐานะที่ปรึกษาทางการเงิน และผู้จัดการการจัดจำหน่ายและรับประกันการจำหน่ายหุ้นสามัญเพิ่มทุน  ถ่ายภาพร่วมกับนักวิเคราะห์ ในงาน Analyst Meeting  ภายหลังการนำเสนอข้อมูลบริษัท เพื่อเสนอขายหุ้นสามัญเพิ่มทุนให้กับประชาชนเป็นครั้งแรก (IPO) ณ  อินเตอร์ลิ้งค์ ดาต้าเซ็นเตอร์  ถ.กาญจนาภิเษก
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                     <div class="portfolio-item 1512  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/151201/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">5 ธันวา มหาราชา</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/151201/img01.jpg" rel="prettyPhoto[1512]"
                                        title="
เนื่องในวโรกาสมหามงคลเฉลิมพระชนมพรรษาครบ 88 พรรษา 5 ธันวา มหาราช คณะผู้บริหารและพนักงานกลุ่มบริษัทอินเตอร์ลิ้งค์ ได้ร่วมถวายพระพรชัยมงคลพร้อมกัน แด่องค์พระบาทสมเด็จพระเจ้าอยู่หัวภูมิพลอดุลยเดช โดยคุณณัฐนัย อนันตรัมพร ผู้บริหารกลุ่มบริษัทอินเตอร์ลิ้งค์ ได้เปิดกรวยดอกไม้สด และจุดเทียนชัยถวายความจงรักภักดีต่อหน้าพระบรมสาทิศลักษณ์ จากนั้นร่วมร้องเพลงสดุดีมหาราชา-สดุดีมหาราชินี , สรรเสริญพระบารมี เเละพ่อแห่งแผ่นดินพร้อมกัน ณ หน้าอาคารอินเตอร์ลิ้งค์ สำนักงานใหญ่ , ศูนย์กระจายสินค้า R&D เเละสาขาทั้ง 4 ภูมิภาคของบริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน)
">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/151201/img02.jpg" rel="prettyPhoto[1512]" title="5 ธันวา มหาราชา"></a>
                                    <a class="preview" href="portfolio/151201/img03.jpg" rel="prettyPhoto[1512]" title="5 ธันวา มหาราชา"></a>
                                    <a class="preview" href="portfolio/151201/img04.jpg" rel="prettyPhoto[1512]" title="5 ธันวา มหาราชา"></a>
                                    <a class="preview" href="portfolio/151201/img05.jpg" rel="prettyPhoto[1512]" title="5 ธันวา มหาราชา"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1606  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160602/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">เกาะเทรนด์ธุรกิจ ยุคดิจิตอล</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160602/img01.jpg" rel="prettyPhoto[1606]"
                                        title="
คุณณัฐนัย อนันตรัมพร ผู้บริหารกลุ่มบริษัทอินเตอร์ลิ้งค์ ได้ร่วมบรรยายในงานสุดยอดงานสัมมนาประจำไตรมาส 2/59 เกาะเทรนด์ธุรกิจ ยุคดิจิตอล และ ส่องหุ้นเด่นครึ่งหลังปี 59 เพื่อเป็นการแลกเปลี่ยนความคิดเห็นและพัฒนาธุรกิจร่วมกันในการเตรียมความพร้อมด้านธุรกิจโทรคมนาคมให้ทัดเทียมกับนานาชาติ รวมไปถึงแนวทางการทำธุรกิจในโลกยุคดิจิตอลที่ต้องมีความเข้าใจในผู้บริโภคและการใช้งาน ภาคธุรกิจต้องมีการปรับกระบวนความคิดและทัศนคติเพื่อรองรับมูลค่าตลาดสื่อดิจิตอลที่จะมีมูลค่าสูงขึ้น โดยงานนี้จัดขึ้น ณ หอประชุมศาสตราจารย์สังเวียน อินทรวิชัย ชั้น 7 อาคาร B ตลาดหลักทรัพย์แห่งประเทศไทย (ข้างสถานฑูตจีน) เมื่อวันที่ 18 มิถุนายน 2559 ที่ผ่านมา
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160602/img02.jpg" rel="prettyPhoto[1606]" title="เกาะเทรนด์ธุรกิจ ยุคดิจิตอล"></a>
                                    <a class="preview" href="portfolio/160602/img03.jpg" rel="prettyPhoto[1606]" title="เกาะเทรนด์ธุรกิจ ยุคดิจิตอล"></a>
                                    <a class="preview" href="portfolio/160602/img04.jpg" rel="prettyPhoto[1606]" title="เกาะเทรนด์ธุรกิจ ยุคดิจิตอล"></a>
                                    <a class="preview" href="portfolio/160602/img05.jpg" rel="prettyPhoto[1606]" title="เกาะเทรนด์ธุรกิจ ยุคดิจิตอล"></a>
                                    <a class="preview" href="portfolio/160602/img06.jpg" rel="prettyPhoto[1606]" title="เกาะเทรนด์ธุรกิจ ยุคดิจิตอล"></a>
                                    <a class="preview" href="portfolio/160602/img07.jpg" rel="prettyPhoto[1606]" title="เกาะเทรนด์ธุรกิจ ยุคดิจิตอล"></a>
                                    <a class="preview" href="portfolio/160602/img08.jpg" rel="prettyPhoto[1606]" title="เกาะเทรนด์ธุรกิจ ยุคดิจิตอล"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1608  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160803/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์หาดใหญ่</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160803/img01.jpg" rel="prettyPhoto[1608]"
                                        title="
คุณณัฐนัย อนันตรัมพร (ซ้าย) กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) หรือ ITEL ผู้ให้บริการโครงข่ายใยแก้วนำแสง ให้บริการติดตั้งโครงข่าย และให้บริการพื้นที่ศูนย์สำรองข้อมูลหรือดาต้าเซ็นเตอร์ (Data Center) พร้อมด้วยคุณสมภพ กีระสุนทรพงษ์ (ขวา) กรรมการผู้อำนวยการ บริษัทหลักทรัพย์ ฟินันเซีย ไซรัส จำกัด (มหาชน) ฐานะที่ปรึกษาทางการเงิน และผู้จัดการการจัดจำหน่ายและรับประกันการจำหน่ายหุ้นสามัญเพิ่มทุน ควงแขนเดินสายโรดโชว์ นำเสนอข้อมูลบริษัทเพื่อเสนอขายหุ้นสามัญเพิ่มทุนให้กับประชาชนเป็นครั้งแรก  (IPO) จำนวน 200 ล้านหุ้น ในตลาดหลักทรัพย์เอ็ม เอไอ ซึ่ง ITEL เตรียมเดินสายโรดโชว์ที่กรุงเทพฯและเชียงใหม่ต่อไป คาดเข้าจดทะเบียนภายในไตรมาส 3 ปีนี้ โดยมีนักลงทุนสนใจเข้ารับฟังข้อมูลกันอย่างเนืองแน่น ณ ห้องสันติภาพ โรงแรม ลี การ์เดนส์ พลาซ่า อ. หาดใหญ่ จ.สงขลา เมื่อเร็วๆนี้
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160803/img01.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์หาดใหญ่"></a>
                                    <a class="preview" href="portfolio/160803/img02.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์หาดใหญ่"></a>
                                    <a class="preview" href="portfolio/160803/img03.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์หาดใหญ่"></a>
                                    <a class="preview" href="portfolio/160803/img04.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์หาดใหญ่"></a>
                                    <a class="preview" href="portfolio/160803/img05.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์หาดใหญ่"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1512  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/151202/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/151202/img01.jpg" rel="prettyPhoto[1512]"
                                        title="
บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน) ได้จัดงานสัมมนาในหัวข้อ”LINK มีปัญหา INTERLINK มีคำตอบ” โดยได้รับเกียรติจากคุณสมบัติ อนันตรัมพร ประธานกรรมการและกรรมการผู้จัดการใหญ่ บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน )เป็นประธานกล่าวเปิดงานสัมมนาในครั้งนี้ ภายในงานสัมมนา คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ และคุณกฤษฎา แก้ววัดปริง รองผู้อำนวยการฝ่ายพัฒนาธุรกิจ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) เป็นผู้ให้ข้อมูลและตอบคำถามในงานสัมมนา อาทิ การอัพเดทข้อมูลของการให้บริการและการขยายโครงข่ายของอินเตอร์ลิ้งค์ เทเลคอมทั้งในและต่างประเทศ รวมถึงการบริการหลังการขายเพื่อสร้างความมั่นใจในคุณภาพของโครงข่ายอินเตอร์ลิ้งค์ เทเลคอม นอกจากนี้ยังมีการมอบรางวัล Lucky Draw จำนวน 9 รางวัล เพื่อแทนคำขอบคุณแก่ผู้เข้าร่วมงาน งานสัมมนานี้จัดขึ้นเมื่อวันพุธที่ 9 ธันวาคม 2558 ณ โรงแรมเจ้าพระยาปาร์ค ถนนรัชดาภิเษก
">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/151202/img02.jpg" rel="prettyPhoto[1512]" title="งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ"></a>
                                    <a class="preview" href="portfolio/151202/img03.jpg" rel="prettyPhoto[1512]" title="งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ"></a>
                                    <a class="preview" href="portfolio/151202/img04.jpg" rel="prettyPhoto[1512]" title="งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ"></a>
                                    <a class="preview" href="portfolio/151202/img05.jpg" rel="prettyPhoto[1512]" title="งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ"></a>
                                    <a class="preview" href="portfolio/151202/img06.jpg" rel="prettyPhoto[1512]" title="งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ"></a>
                                    <a class="preview" href="portfolio/151202/img07.jpg" rel="prettyPhoto[1512]" title="งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ"></a>
                                    <a class="preview" href="portfolio/151202/img08.jpg" rel="prettyPhoto[1512]" title="งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ"></a>
                                    <a class="preview" href="portfolio/151202/img09.jpg" rel="prettyPhoto[1512]" title="งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ"></a>
                                    <a class="preview" href="portfolio/151202/img10.jpg" rel="prettyPhoto[1512]" title="งานสัมมนาในหัวข้อ LINK มีปัญหา INTERLINK มีคำตอบ"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1510  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/151008/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">คุณณัฐนัย ได้ร่วมเป็น Speaker ในงาน  Cloud Expo Asia 2015</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/151008/img01.jpg" rel="prettyPhoto[1510]"
                                        title="
                                        คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้ร่วมเป็น speaker ในงาน Cloud Expo Asia 2015 ซึ่งเป็นงานที่นำเสนอเกี่ยวกับ Cloud ที่ใหญ่ที่สุดในเอเชีย ภายใต้หัวข้อ What can Cloud Service Providers do to help enterprise organizations move from legacy จัดขึ้น ณ Suntec Singapore International Convention & Exhibition Centre Halls
                                            ">
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1606  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160603/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">CommScope Roadshow 2016</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160602/img01.jpg" rel="prettyPhoto[1606]"
                                        title="
บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมประกาศศักยภาพความเป็นผู้นำแห่งธุรกิจการสื่อสารโทรคมนาคม โดยเข้าร่วมจัดแสดงเทคโนโลยีในนิทรรศการ CommScope Roadshow 2016 Next Generation Data Center Design เมื่อวันที่ 16 มิถุนายน 2559 ณ โรงแรมพลาซ่า แอทธินี กรุงเทพฯ ผู้ร่วมงานจะได้พบกับความเร็วในการเชื่อมต่อที่สำคัญของการทำงานอย่างรวดเร็วและมีประสิทธิภาพ  ความสำเร็จของดาต้าเซ็นเตอร์  มาตรฐานของดาต้าเซ็นเตอร์ ในยุคคลาวด์คอมพิวติ้งและโครงสร้างพื้นฐานดาต้าเซ็นเตอร์ระดับโลกในประเทศไทย ซึ่งในงานนี้มีผู้สนใจศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ที่มีความปลอดภัยและทันสมัยได้มาตรฐาน ISO/IEC 27001:2013 ที่จะช่วยให้ธุรกิจสามารถลดค่าใช้จ่ายและประสิทธิภาพสูงสุดให้กับการทำงานขององค์กรเป็นจำนวนมาก
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160603/img02.jpg" rel="prettyPhoto[1606]" title="CommScope Roadshow 2016"></a>
                                    <a class="preview" href="portfolio/160603/img03.jpg" rel="prettyPhoto[1606]" title="CommScope Roadshow 2016"></a>
                                    <a class="preview" href="portfolio/160603/img04.jpg" rel="prettyPhoto[1606]" title="CommScope Roadshow 2016"></a>
                                    <a class="preview" href="portfolio/160603/img05.jpg" rel="prettyPhoto[1606]" title="CommScope Roadshow 2016"></a>
                                    <a class="preview" href="portfolio/160603/img06.jpg" rel="prettyPhoto[1606]" title="CommScope Roadshow 2016"></a>
                                    <a class="preview" href="portfolio/160603/img07.jpg" rel="prettyPhoto[1606]" title="CommScope Roadshow 2016"></a>
                                    <a class="preview" href="portfolio/160603/img08.jpg" rel="prettyPhoto[1606]" title="CommScope Roadshow 2016"></a>
                                    <a class="preview" href="portfolio/160603/img09.jpg" rel="prettyPhoto[1606]" title="CommScope Roadshow 2016"></a>
                                    <a class="preview" href="portfolio/160603/img10.jpg" rel="prettyPhoto[1606]" title="CommScope Roadshow 2016"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1508  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150812A/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Asia Best Employer Brand Awards 2015</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150812A/img01.jpg" rel="prettyPhoto[1508]"
                                        title="
                                        คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้รับเกียรติในการรับรางวัล
                                        “Asia Best Employer Brand Awards 2015” โดยบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้รับเลือก เป็น 1 ใน
                                        4 บริษัทฯ ไทย ซึ่งรางวัลนี้เป็นรางวัลที่มอบให้กับบริษัทฯที่มีการเติบโตสูงและดูถึงวิธีการบริหารจัดการพนักงาน
                                        มีการส่งเสริมการสร้างสรรค์นวัตกรรมในการทำงาน มีความรับผิดชอบต่อสังคม และมีการพัฒนาพนักงานให้เติบโตอย่างต่อเนื่อง
                                        ภายในงานมีบริษัทฯชั้นนำเข้าร่วมงานมากมาย อาทิเช่น Microsoft Thailand, IRPC, TMB Bank, Interlink Telecom
                                        โดยการมอบรางวัลจัดขึ้น ณ โรงแรมแพนแปซิฟิค , ประเทศสิงคโปร์ ในวันที่ 12 สิงหาคม 2558 ที่ผ่านมา
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150812A/img02.jpg" rel="prettyPhoto[1508]" title="Asia Best Employer Brand Awards 2015"></a>
                                    <a class="preview" href="portfolio/150812A/img03.jpg" rel="prettyPhoto[1508]" title="Asia Best Employer Brand Awards 2015"></a>
                                    <a class="preview" href="portfolio/150812A/img04.jpg" rel="prettyPhoto[1508]" title="Asia Best Employer Brand Awards 2015"></a>
                                    <a class="preview" href="portfolio/150812A/img05.jpg" rel="prettyPhoto[1508]" title="Asia Best Employer Brand Awards 2015"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1609  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160904/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ITEL แต่งตั้งอันเดอร์ไรท์ </span></a></h3>
                                    <p></p>
                                    <a class="preview"
                                        href="portfolio/160904/img01.jpg"
                                        rel="prettyPhoto[1609]"
                                        title="
คุณณัฐนัย อนันตรัมพร (ที่6จากซ้าย) กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) หรือ ITEL ผู้ให้บริการโครงข่ายใยแก้วนำแสง ให้บริการติดตั้งโครงข่าย และให้บริการพื้นที่ศูนย์สำรองข้อมูลหรือดาต้าเซ็นเตอร์ (Data Center) พร้อมด้วยคุณสมภพ กีระสุนทรพงษ์ (ที่4จากขวา) กรรมการผู้อำนวยการ บริษัทหลักทรัพย์ ฟินันเซีย ไซรัส จำกัด (มหาชน) ในฐานะที่ปรึกษาทางการเงิน และผู้จัดการการจัดจำหน่ายและรับประกันการจำหน่ายหุ้นสามัญเพิ่มทุน ถ่ายภาพเป็นที่ระลึกร่วมกับผู้บริหารระดับสูงจากบริษัท อินเตอร์ลิ้ง คอมมิวนิเคชั่น จำกัด(มหาชน) และตัวแทนจากบริษัทหลักทรัพย์ ในฐานะผู้ร่วมจัดจำหน่ายและรับประกันการจำหน่ายอีก 6 แห่ง ภายหลังพิธีลงนามแต่งตั้งผู้จัดจำหน่ายและรับประกันการจำหน่ายหุ้นสามัญเพิ่มทุน โดยกำหนดราคาไอพีโออยู่ที่ 5.20บาท/หุ้น และเปิดจองซื้อในวันที่ 31 ส.ค. และ 1-2 ก.ย. สำหรับผู้ถือหุ้น ILINK (Pre-emptive Right) และวันที่ 7-9 ก.ย. สำหรับประชาชนทั่วไป และคาดว่าจะสามารถเข้าซื้อขายในตลาดหลักทรัพย์ ในวันที่ 14 กันยายน นี้ ณ ห้องประชุม 601-602 อาคารตลาดหลักทรัพย์แห่งประเทศไทย
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160904/img01.jpg"
                                        rel="prettyPhoto[1609]"
                                        title="ITEL แต่งตั้งอันเดอร์ไรท์"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1507  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150701/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150701/img01.jpg" rel="prettyPhoto[1507]"
                                        title="บรรยากาศภายในงาน MAI Forum 2015 มหกรรมรวมพลังคน mai2 จัดขึ้น ณ ศูนย์การประชุมแห่งชาติสิริกิติ์
                                        เมื่อวันที่ 1 กรกฏาคมที่ผ่านมา โดยทางกลุ่มบริษัทอินเตอร์ลิ้งค์ ได้ร่วมออกบูธเพื่อนำเสนอข้อมูลการดำเนินธุรกิจของทั้ง 4 บริษัทในเครือไม่ว่าจะเป็น
<br>1. บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ซึ่งเป็นผู้นำเข้าและจัดจำหน่ายสายสัญญาณที่ใหญ่ที่สุดในประเทศไทย
<br>2. บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน) ผู้นำทางด้านโครงข่ายสื่อสัญญาณความเร็วสูง
<br>3. บริษัท อินเตอร์ลิ้งค์ พาวเวอร์ แอนด์ เอ็นเนอร์ยี่ จำกัด ผู้เชี่ยวชาญด้าน Sub marine Cable และ Transmission line
<br>4. บริษัท อินเตอร์ลิ้งค์ ดาต้าเซ็นเตอร์ จำกัด ศูนย์ดาต้าเซ็นเตอร์ที่ปลอดอภัยและทันสมัย <br>
ซึ่งนโยบายหลักในการทำงานของกลุ่มบริษัทอินเตอร์ลิ้งค์ คือการเติบโตอย่างยั่งยืน
ทำให้ได้รับความเชื่อถือจากทั้งลูกค้าและนักลงทุนที่เข้าเยี่ยมชมภายในบูธเป็นจำนวนมาก">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150701/img02.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img03.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img04.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img05.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img06.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img07.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img08.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img09.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img10.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img11.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img12.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img13.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img14.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img15.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                    <a class="preview" href="portfolio/150701/img16.jpg" rel="prettyPhoto[1507]" title="งาน MAI Forum 2015 มหกรรมรวมพลังคน MAI 2"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->


                    <div class="portfolio-item 1606  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160606/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">PEA เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160606/img01.jpg" rel="prettyPhoto[1606]"
                                        title="
เมื่อวันที่ 23 มิถุนายน 2559 ที่ผ่านมา ตัวแทนของการไฟฟ้าส่วนภูมิภาค (PEA) เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ที่มีความปลอดภัยและทันสมัยระดับโลก โดยมีคุณสมบัติ อนันตรัมพร ประธานกรรมการและกรรมการผู้จัดการใหญ่ บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน )และคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน) และบริษัท อินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ จำกัด ให้การต้อนรับคณะผู้เยี่ยมชมอย่างเป็นกันเอง สำหรับศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ ได้ออกแบบตามมาตรฐาน TIA 942 ระดับ Tier-3มีความปลอดภัยและทันสมัยได้มาตรฐาน ISO/IEC 27001:2013 รวมถึงตัวโครงสร้างอาคารที่ออกแบบมาเพื่อช่วยประหยัดพลังงาน พร้อมให้บริการด้วยทีมงานที่มีความเชี่ยวชาญและดูแลตลอด 24 ชั่วโมง 7 วัน ทำให้มั่นใจได้ว่าลูกค้าที่มาใช้บริการจะได้รับประสิทธิภาพสูงสุด
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160606/img02.jpg" rel="prettyPhoto[1606]" title="PEA เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/160606/img03.jpg" rel="prettyPhoto[1606]" title="PEA เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1510  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/151009/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">คุณณัฐนัย  เป็นหนึ่งในผู้บริหารระดับสูงที่ได้รับมอบโล่เกียรติคุณ “บุคคลคุณภาพแห่งปี 2015”</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/151009/img01.jpg" rel="prettyPhoto[1510]"
                                        title="
มูลนิธิสภาวิทยาศาสตร์และเทคโนโลยีแห่งประเทศไทย (มสวท.) ได้จัดทำโครงการประกาศเกียรติคุณ “บุคคลคุณภาพแห่งปี” (Quality Persons Of The Year) ซึ่งในปี 2015 นี้ คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) เป็นหนึ่งในผู้บริหารระดับสูงที่ได้รับมอบโล่เกียรติคุณ “บุคคลคุณภาพแห่งปี 2015” ภาคธุรกิจเทคโนโลยีสารสนเทศและการสื่อสาร ซึ่งถือเป็นรางวัลที่แสดงการยกย่องบุคคลที่ประสบความสำเร็จในการดำเนินชีวิต รวมถึงการอุทิศตนทำกิจกรรมเพื่อประโยชน์ต่อสังคมและประเทศชาติในด้านต่างๆ ซึ่งสมควรเป็นแบบอย่างที่ดีและควรค่าต่อการส่งเสริมต่อไป พิธีดังกล่าวจัดขึ้น ณ หอประชุมใหญ่ ศูนย์ประชุมสถาบันวิจัยจุฬาภรณ์ โดยมี ฯพณฯ พลเอก พิจิตร กุลละวณิชย์ (องคมนตรี) เป็นประธาน">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/151009/img02.jpg" rel="prettyPhoto[1510]" title="คุณณัฐนัย  เป็นหนึ่งในผู้บริหารระดับสูงที่ได้รับมอบโล่เกียรติคุณ “บุคคลคุณภาพแห่งปี 2015”"></a>

                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1608  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160802/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์ตลาดหลักทรัพย์แห่งประเทศไทย</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160802/img01.jpg" rel="prettyPhoto[1608]"
                                        title="
บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) หรือ ITEL ผู้ให้บริการโครงข่ายใยแก้วนำแสง ให้บริการติดตั้งโครงข่ายและให้บริการพื้นที่ศูนย์สำรองข้อมูลหรือดาต้าเซ็นเตอร์ (Data Center) เดินหน้าพบนักลงทุนอย่างต่อเนื่องในงาน Roadshow ณ ห้องประชุม 704 ชั้น 7 อาคาร B ตลาดหลักทรัพย์แห่งประเทศไทย ถ.รัชดาภิเษก นำโดย คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) พร้อมด้วยคุณสมภพ กีระสุนทรพงษ์” กรรมการผู้อำนวยการ บริษัท หลักทรัพย์ ฟินันเซีย ไซรัส จำกัด (มหาชน)ในฐานะที่ปรึกษาทางการเงินและผู้จัดการการจัดจำหน่ายและรับประกันการจำหน่ายหุ้นสามัญเพิ่มทุน นำเสนอข้อมูลบริษัทเพื่อเสนอขายหุ้นสามัญเพิ่มทุนให้กับประชาชนเป็นครั้งแรก (IPO) จำนวน 200 ล้านหุ้นเพื่อแสดงความพร้อมที่จะเข้ามาเติบโตอย่างแข็งแกร่งเป็นสมาชิกน้องใหม่ในตลาดหลักทรัพย์ เอ็ม เอ ไอ และจะเป็นอีกหุ้นที่สร้างผลตอบแทนที่ดีต่อนักลงทุน
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160802/img01.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์ตลาดหลักทรัพย์แห่งประเทศไทย"></a>
                                    <a class="preview" href="portfolio/160802/img02.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์ตลาดหลักทรัพย์แห่งประเทศไทย"></a>
                                    <a class="preview" href="portfolio/160802/img03.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์ตลาดหลักทรัพย์แห่งประเทศไทย"></a>
                                    <a class="preview" href="portfolio/160802/img04.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์ตลาดหลักทรัพย์แห่งประเทศไทย"></a>
                                    <a class="preview" href="portfolio/160802/img05.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์ตลาดหลักทรัพย์แห่งประเทศไทย"></a>
                                    <a class="preview" href="portfolio/160802/img06.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์ตลาดหลักทรัพย์แห่งประเทศไทย"></a>
                                    <a class="preview" href="portfolio/160802/img07.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์ตลาดหลักทรัพย์แห่งประเทศไทย"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1508  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150819/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150819/img01.jpg" rel="prettyPhoto[1508]"
                                        title="
                                        กลุ่มบริษัท อินเตอร์ลิ้งค์ ขอขอบคุณลูกค้าผู้มีอุปการคุณและผู้ถือหุ้นทุกท่าน ที่ให้เกียรติเข้าร่วมงานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET และร่วมแสดงความยินดีกับบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) เตรียมเข้าจดทะเบียน mai โดยภายในงานคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้แสดงวิสัยทัศน์เกี่ยวกับความก้าวหน้าของเทคโนโลยีในปัจจุบันและแนวทางการดำเนินงานของอินเตอร์ลิ้งค์ เทเลคอมให้เติบโตอย่างยั่งยืน จัด ณ ห้องแกรนด์ บอลรูม , โรงแรมเชอราตัน แกรนด์ สุขุมวิท

                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150819/img02.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img03.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img04.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img05.jpg" rel="prettyPhoto[1508]" title="Aงานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img06.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img07.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img08.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img09.jpg" rel="prettyPhoto[1508]" title="Aงานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img10.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img11.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img12.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img13.jpg" rel="prettyPhoto[1508]" title="Aงานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img14.jpg" rel="prettyPhoto[1508]" title="Aงานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img15.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img16.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img17.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img18.jpg" rel="prettyPhoto[1508]" title="Aงานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img19.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img21.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img22.jpg" rel="prettyPhoto[1508]" title="งานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img23.jpg" rel="prettyPhoto[1508]" title="Aงานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                    <a class="preview" href="portfolio/150819/img24.jpg" rel="prettyPhoto[1508]" title="Aงานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1504  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150429/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150429/img01.jpg" rel="prettyPhoto[1504]"
                                        title="ต้อนรับกระแสภาพยนตร์ซูเปอร์ฮีโร่ตลอดกาล Interlink Telecom
    ได้จัดกิจกรรมชมภาพยนตร์สุด Exclusive รอบแรกก่อนฉายจริงในเมืองไทยวันที่
    30 เมษายน 2558 กับ The Avengers : Age of Ultron มหาศึกอัลตรอนถล่มโลก
    ให้กับลูกค้าที่ใช้บริการของทางบริษัทฯ นอกจากนี้ยังมีกิจกรรมถ่ายภาพที่ระลึกคู่กับตัวละคร
    จากภาพยนตร์และลุ้นไปกับการเล่นเกมชิงรางวัลบัตรรับประทานไอศครีม Swensen
    ณ โรงภาพยนตร์ SF world Cinema, Central World เมื่อวันพุธที่ 29 เมษายน 2558 ที่ผ่านมา">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150429/img02.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img03.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img04.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img05.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img06.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img07.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img08.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img09.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img10.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img11.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img12.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img13.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img14.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img15.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img16.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img17.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img18.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img19.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img20.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img21.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img22.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img23.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                    <a class="preview" href="portfolio/150429/img24.jpg" rel="prettyPhoto[1504]" title="กิจกรรมชมภาพยนตร์ The Avengers : Age of Ultron"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1510  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/151007/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Interlink Telecom Business Review with ISSP</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/151007/img01.jpg" rel="prettyPhoto[1510]"
                                        title="
เพื่อเป็นการขอบคุณลูกค้าและกระชับความสัมพันธ์อันดีกับบริษัท อินเตอร์เนต โซลูชั่น แอนด์ เซอร์วิส โพรวายเดอร์ จำกัด (ISSP) อินเตอร์ลิงค์ เทเลคอม
ได้จัดกิจกรรม Interlink Telecom Business Review with ISSP ทั้งนี้เพื่อให้มีโอกาสได้พบปะสังสรรค์ พูดคุยและรับประทานอาหารว่างร่วมกันในบรรยากาศ
ที่แสนอบอุ่น นับเป็นปาร์ตี้ Afternoon Tea and Talk ที่สนุกสนานและเป็นกันเองอย่างยิ่ง เมื่อวันพุธที่ 7 ตุลาคม 2558 ที่ผ่านมา                                     ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/151007/img02.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img03.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img04.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img05.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img06.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img07.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img08.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img09.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img10.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img11.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img12.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img13.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img14.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>
                                    <a class="preview" href="portfolio/151007/img15.jpg" rel="prettyPhoto[1510]" title="Interlink Telecom Business Review with ISSP"></a>

                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1608  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160804/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Exclusive Golf Club With Guru</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160804/img01.jpg" rel="prettyPhoto[1608]"
                                        title="
อากาศร้อนไม่ใช่อุปสรรคสำคัญของการออกรอบอีกต่อไป บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้ร่วมสร้างความสุขกับออกรอบกับกิจกรรมขอบคุณลูกค้า “Exclusive Golf Club With Guru” เชิญชวนลูกค้าผู้มีอุปการคุณเปิดโอกาสในการออกรอบกับ Golf Simulator เทคโนโลยีอันดับ 1 จากประเทศเกาหลี จำลองสนามชั้นนำทั่วโลกกว่า 140 สนาม ที่ให้อารมณ์เดียวกับกับการออกรอบในสนามจริง ด้วยภาพที่คมชัด และ Swing Plate ที่ปรับระดับได้ตามสภาพของสนาม โดยลูกค้าสามารถเรียกดูการตีย้อนหลัง รวมถึง score ย้อนหลังผ่านแอปพลิเคชั่น golfzon รองรับทั้ง ios และ android ทั้งนี้กิจกรรมได้เป็นการเปิดโอกาสให้นักกอล์ฟมือเก๋าและมือสมัครเล่นแข่งขันดวลวงสวิง สร้างความสนุกสนานและคลายความเครียดจากการทำงานได้เป็นอย่างดี หลังจากนั้นทุกท่านได้ร่วมทานอาหารและสังสรรค์ในบรรยากาศแบบเป็นกันเอง ปิดท้ายความประทับใจด้วยการแจกของรางวัลต่างๆ มากมาย กิจกรรมสนุกๆ สุดพิเศษแบบนี้จัดขึ้นเมื่อวันเสาร์ที่ 6 สิงหาคม 2559 ที่ผ่านมา ณ OB CLUB Esplanade Ratchada
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160804/img01.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img02.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img03.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img04.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img05.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img06.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img07.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img08.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img09.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img10.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img11.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img12.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                    <a class="preview" href="portfolio/160804/img13.jpg" rel="prettyPhoto[1608]"
                                        title="Exclusive Golf Club With Guru"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1509  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150904/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">โครงการ พี่สอนน้อง ปลูกปัญญา...พร้อมมอบความอบอุ่น</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150904/img01.jpg" rel="prettyPhoto[1509]"
                                        title="
                                        บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) และบริษัท TCBB ร่วมสนับสนุน มูลนิธิอินเตอร์ลิ้งค์ให้ใจ ในโครงการ พี่สอนน้อง “ปลูกปัญญา...พร้อมมอบความอบอุ่น”โดยคุณวีกิจ นิมิตเกษมสุภัค ตำแหน่ง O&M Engineer ประจำจังหวัดนครปฐม และ คุณรุ่งเรือง หวนระลึก ผู้จัดการ บริษัท TCBB นำทีมช่างเดินสาย LAN ภายในอาคารเรียนของโรงเรียนบ้านห้วยด้วน ตำบลกำแพงแสน จังหวัดนครปฐม ทั้งสองชั้น เพื่อเชื่อมต่อกับ Wireless access ภายในห้องเรียน
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150904/img02.jpg" rel="prettyPhoto[1509]" title="โครงการ พี่สอนน้อง “ปลูกปัญญา...พร้อมมอบความอบอุ่น”"></a>
                                    <a class="preview" href="portfolio/150904/img03.jpg" rel="prettyPhoto[1509]" title="โครงการ พี่สอนน้อง “ปลูกปัญญา...พร้อมมอบความอบอุ่น”"></a>
                                    <a class="preview" href="portfolio/150904/img04.jpg" rel="prettyPhoto[1509]" title="โครงการ พี่สอนน้อง “ปลูกปัญญา...พร้อมมอบความอบอุ่น”"></a>
                                    <a class="preview" href="portfolio/150904/img05.jpg" rel="prettyPhoto[1509]" title="โครงการ พี่สอนน้อง “ปลูกปัญญา...พร้อมมอบความอบอุ่น”"></a>
                                    <a class="preview" href="portfolio/150904/img06.jpg" rel="prettyPhoto[1509]" title="โครงการ พี่สอนน้อง “ปลูกปัญญา...พร้อมมอบความอบอุ่น”"></a>

                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1608  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160806/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์เชียงใหม่</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160806/img01.jpg" rel="prettyPhoto[1608]"
                                        title="
คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน) หรือ ITEL ผู้ให้บริการโครงข่ายใยแก้วนำแสง ให้บริการติดตั้งโครงข่าย และให้บริการพื้นที่ศูนย์สำรองข้อมูลหรือดาต้าเซ็นเตอร์ (Data Center) พร้อมด้วยคุณสมภพ กีระสุนทรพงษ์ กรรมการผู้อำนวยการ บริษัทหลักทรัพย์ ฟินันเซีย ไซรัส จำกัด (มหาชน) ในฐานะที่ปรึกษาทางการเงิน และผู้จัดการการจัดจำหน่ายและรับประกันการจำหน่ายหุ้นสามัญเพิ่มทุน ปิดท้ายโรดโชว์ที่จังหวัดเชียงใหม่และได้นำเสนอข้อมูลสรุปการเสนอขายหุ้นสามัญเพิ่มทุน เตรียมเสนอขายหุ้นให้กับประชาชนเป็นครั้งแรก (IPO) จำนวน 200 ล้านหุ้น ในตลาดหลักทรัพย์เอ็ม เอ ไอโดยมีนักลงทุนสนใจเข้ารับฟังข้อมูลและซักถามเป็นจำนวนมาก
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160806/img01.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์เชียงใหม่"></a>
                                    <a class="preview" href="portfolio/160806/img02.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์เชียงใหม่"></a>
                                    <a class="preview" href="portfolio/160806/img03.jpg" rel="prettyPhoto[1608]"
                                        title="อินเตอร์ลิ้งค์ เทเลคอมโรดโชว์เชียงใหม่"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1508  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150812/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150812/img01.jpg" rel="prettyPhoto[1508]"
                                        title="
                                         เนื่องในวโรกาส มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี คณะผู้บริหารและพนักงานกลุ่มบริษัทฯอินเตอร์ลิ้งค์
                                         ได้ร่วมถวายพระพรชัยมงคลพร้อมกัน แด่องค์สมเด็จพระนางเจ้าสิริกิติ์พระบรมราชินีนาถ โดยคุณชลิดา อนันตรัมพร กรรมการผู้จัดการ
                                         ได้เปิดกรวยดอกไม้สด และจุดเทียนชัยถวายความจงรักภักดีต่อหน้าพระบรมสาทิศลักษณ์
                                         จากนั้นร่วมร้องเพลงสดุดีมหาราชา-สดุดีมหาราชินี , สรรเสริญพระบารมี เเละเเม่ของแผ่นดินพร้อมกัน
                                         ณ หน้าอาคารอินเตอร์ลิ้งค์ สำนักงานใหญ่ , ศูนย์กระจายสินค้า R&D เเละสาขาทั้ง 4 ภูมิภาคของบริษัท
                                         อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) จากนั้นในเวลาต่อมา คุณชลิดา อนันตรัมพร
                                         กรรมการผู้จัดการกลุ่มบริษัทอินเตอร์ลิ้งค์เเละประธานมูลนิธิอินเตอร์ลิ้งค์ให้ใจ ได้ตัดริบบิ้นเพื่อเปิด
                                          โครงการพัฒนาครู...ครูที่ดี 1 คน สามารถสร้างนักเรียนที่ดีได้หลายคน อีกหนึ่งโครงการเพื่อสังคมดีๆ นอกจาก
                                          โครงการปลูกปัญญา พร้อมมอบความอบอุ่น โดยมูลนิธีอินเตอร์ลิ้งค์ให้ใจ ทั้งนี้คุณชลิดา อนันตรัมพร
                                          ยังได้เล่าที่มาที่ไปเเละจุดประสงค์ของโครงการนี้ ให้แก่ผู้บริหารของกลุ่มบริษัทอินเตอร์ลิ้งค์เเละถ่ายภาพร่วมกัน เมื่อวันที่ 11 สิงหาคมที่ผ่านมา
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150812/img02.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img03.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img04.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img05.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img06.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img07.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img08.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img09.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img10.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img11.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img12.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img13.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                    <a class="preview" href="portfolio/150812/img14.jpg" rel="prettyPhoto[1508]" title="มหามงคลเฉลิมพระชนม์พรรษา 12 สิงหา มหาราชินี"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1511  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/151101/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">BE OUR GUEST RELAX PARTY</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/151101/img01.jpg" rel="prettyPhoto[1511]"
                                        title="
                                        บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้จัดกิจกรรมโยนโบว์ลิ่งเพื่อกระชับความสัมพันธ์และมอบความสนุกสนานแก่ลูกค้าที่ไว้วางใจบริการของบริษัทฯเสมอมา ภายใต้ชื่องาน “BE OUR GUEST RELAX PARTY” โดยมีคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์  เทเลคอม จำกัด (มหาชน) เป็นประธานกล่าวเปิดงาน ภายในงานมีการเชิญลูกค้ากว่า 9 บริษัทเข้าร่วมการแข่งขัน ซึ่งกิจกรรมครั้งนี้ ได้เสียงตอบรับจากลูกค้ามาเข้าร่วมกิจกรรมกันอย่างคับคั่ง ปิดการแข่งขันไปด้วยทีมจาก”ช่องสาม”คว้าถ้วยรางวัลชนะเลิศพร้อมเงินสดมูลค่า 3,000 บาท ไปครอง หลังจากนั้นมีการร่วมรับประทานอาหารเย็นและสนุกสนานกับกิจกรรมลุ้นของรางวัลอื่นๆ อีกมากมาย ถือเป็นโอกาสดีที่ได้ร่วมพบปะ พูดคุยกันอย่างใกล้ชิด ก่อนอำลากันไปด้วยความประทับใจ เมื่อวันพฤหัสบดีที่ 19 พฤศจิกายน 2558 ณ Blu-O , เอสพลานาด รัชดา ที่ผ่านมา
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/151101/img02.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img03.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img04.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img05.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img06.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img07.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img08.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img09.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img10.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img11.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img12.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img13.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img14.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img15.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img16.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img17.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img18.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img19.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img20.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img21.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                    <a class="preview" href="portfolio/151101/img22.jpg" rel="prettyPhoto[1511]" title="BE OUR GUEST RELAX PARTY"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1606  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160601/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอมจับมือผู้ประกอบการต่อยอดธุรกิจเคเบิลดิจิตอล</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160601/img01.jpg" rel="prettyPhoto[1606]"
                                        title="
คุณณัฐนัย อนันตรัมพร (ที่ 5 จากซ้าย) กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) และดร. วารินทร์ ชลหาญ (ที่ 6 จากขวา) ประธานกลุ่มเคเบิล ดิจิตอล ถ่ายภาพร่วมกันเนื่องในวาระการลงนามบันทึกข้อตกลงความร่วมมือ (MOU) ว่าด้วยความร่วมมือในการขับเคลื่อนธุรกิจเคเบิล และดิจิตอลทีวี เพื่อให้เกิดการพัฒนาในเรื่องของคุณภาพการบริการให้สามารถแข่งขันได้ในภาวะเศรษฐกิจปัจจุบันและการขยายพื้นที่ให้บริการของผู้ประกอบการทั่วประเทศ จัดขึ้น ณ อาคารอินเตอร์ลิ้งค์ ถนนรัชดาภิเษก เมื่อวันที่ 7 มิถุนายน 2559 ที่ผ่านมา
">
                                    <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160601/img02.jpg" rel="prettyPhoto[1606]" title="อินเตอร์ลิ้งค์ เทเลคอมจับมือผู้ประกอบการต่อยอดธุรกิจเคเบิลดิจิตอล"></a>
                                    <a class="preview" href="portfolio/160601/img03.jpg" rel="prettyPhoto[1606]" title="อินเตอร์ลิ้งค์ เทเลคอมจับมือผู้ประกอบการต่อยอดธุรกิจเคเบิลดิจิตอล"></a>
                                    <a class="preview" href="portfolio/160601/img04.jpg" rel="prettyPhoto[1606]" title="อินเตอร์ลิ้งค์ เทเลคอมจับมือผู้ประกอบการต่อยอดธุรกิจเคเบิลดิจิตอล"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1511  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/151102/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Thailand Corporate Day</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/151102/img01.jpg" rel="prettyPhoto[1511]"
                                        title="
คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้แสดงวิสัยทัศน์และการบริหารงานของกลุ่มบริษัทอินเตอร์ลิ้งค์กับบริษัทด้านการลงทุนจากประเทศต่างๆเพื่อสร้างความเข้าใจเกี่ยวกับธุรกิจของบริษัทที่เติบโตมาอย่างต่อเนื่องและยั่งยืน ในงาน Thailand Corporate Day ที่จัดขึ้น ณ JW Marriott Hotel Kuala Lumpur
">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/151102/img02.jpg" rel="prettyPhoto[1511]" title="Thailand Corporate Day"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1511  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/151103/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">คุณณัฐนัย อนันตรัมพร ได้รับเกียรติในการรับรางวัล Forbes Asia 200 Best Under a Billion</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/151103/img01.jpg" rel="prettyPhoto[1511]"
                                        title="
คุณณัฐนัย อนันตรัมพร ผู้บริหารกลุ่มบริษัทอินเตอร์ลิ้งค์ ได้รับเกียรติในการรับรางวัล Forbes Asia 200 Best Under a Billion ภายในงาน Forbes Asia Best Under A Billion Award Ceremony and Dinner ซึ่งจัดโดย นิตยสาร Forbes Asia เพื่อคัดเลือกสุดยอดบริษัทจดทะเบียนในภูมิภาคเอเชียแปซิฟิก 200 ราย และในปี 2558 ประเทศไทยได้รับคัดเลือกเพียง 6 บริษัท โดยบริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) เป็น 1 ใน 6 บริษัทในประเทศไทยที่ได้รับรางวัลดังกล่าว ซึ่งแสดงถึงความเป็นบริษัทฯที่มีศักยภาพในการดำเนินงาน การบริหารต้นทุนที่มีประสิทธิภาพและโครงสร้างทางการเงินที่แข็งแรง ทำให้ผลการดำเนินงานเติบโตโดดเด่น และสร้างผลตอบแทนให้แก่ผู้ลงทุนในอัตราที่สูง จัดขึ้น ณ Sunway Resort & Spa ประเทศมาเลเซีย
">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/151103/img02.jpg" rel="prettyPhoto[1511]" title="คุณณัฐนัย อนันตรัมพร ได้รับเกียรติในการรับรางวัล Forbes Asia 200 Best Under a Billion"></a>
                                    <a class="preview" href="portfolio/151103/img03.jpg" rel="prettyPhoto[1511]" title="คุณณัฐนัย อนันตรัมพร ได้รับเกียรติในการรับรางวัล Forbes Asia 200 Best Under a Billion"></a>
                                    <a class="preview" href="portfolio/151103/img04.jpg" rel="prettyPhoto[1511]" title="คุณณัฐนัย อนันตรัมพร ได้รับเกียรติในการรับรางวัล Forbes Asia 200 Best Under a Billion"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1601  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160101/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Bualuang : Exclusive Wealth Forum</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160101/img01.jpg" rel="prettyPhoto[1601]"
                                        title="
คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้ร่วมเป็นวิทยากรงานสัมมนา “Bualuang : Exclusive Wealth Forum” ในหัวข้อเกาะกระแส Mega Trend ไปกับ 3 กลุ่มธุรกิจ Medical Tourism, Digital Economy, Digital Media โดยมีกลุ่มผู้ประกอบการและนักลงทุนเข้าร่วมรับฟังข้อมูล ณ โรงแรม The Okura Prestige
">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160101/img02.jpg" rel="prettyPhoto[1601]" title="Bualuang : Exclusive Wealth Forum"></a>
                                    <a class="preview" href="portfolio/160101/img03.jpg" rel="prettyPhoto[1601]" title="Bualuang : Exclusive Wealth Forum"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1505  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150511/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด จดทะเบียนแปรสภาพเป็น บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน)</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150511/img01.jpg" rel="prettyPhoto[1505]"
                                        title=" บริษัท  อินเตอร์ลิ้งค์  เทเลคอม จำกัด จดทะเบียนแปรสภาพเป็น บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ขอขอบคุณลูกค้าผู้มีอุปการคุณทุกท่านที่ได้ให้การสนับสนุนกิจการของอินเตอร์ลิ้งค์ เทเลคอม ด้วยดีตลอดมา
    ทำให้บริษัทฯมีการเติบโตอย่างมั่นคงเรื่อยมา โดยในวันที่ 11 พฤษภาคม 2558 ที่ผ่านมา บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด
    ได้แปรสภาพเป็น บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน) และเพื่อตอบแทนความไว้วางใจของลูกค้าที่มีให้กับเรา
    ทางผู้บริหารและพนักงานทุกคนของบริษัทฯขอตั้งปณิธานว่าจะดำเนินงานและนำพาองค์กรสู่ความสำเร็จ
    ด้วยหลักธรรมาภิบาลที่ดี ดำเนินธุรกิจด้วยความซื่อสัตย์ โปร่งใส นำมาซึ่งความพึงพอใจสูงสุดของลูกค้า
    รวมทั้งเดินหน้าเต็มที่สู่ความเป็นผู้นำในธุรกิจโทรคมนาคม">
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1501  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150101/300x200.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">สวัสดีปีใหม่ผู้บริหาร ซีเอส ล็อกซอินโฟ</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150101/img01.png" rel="prettyPhoto[1501]"
                                        title="คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์  เทเลคอม จำกัด
                    พร้อมทีมงาน เข้ามอบของขวัญต้อนรับเทศกาลปีใหม่ 2015 ให้กับผู้บริหารระดับสูงของบริษัท
                    บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ณ ไซเบอร์ เวิลด์ ทาวเวอร์  ถ.รัชดาภิเษก
                    เพื่อเป็นการขอบคุณที่ทางซีเอส ล็อกซอินโฟ ได้เลือกใช้บริการคุณภาพจากทางอินเตอร์ลิ้งค์ เทเลคอมด้วยดีเสมอมา"
                  title="มอบของปีใหม่ซีเอส ล็อกอินโฟ">
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1506  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150628/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Bowling Challenge Interlink Telecom VS True Internet</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150628/img01.jpg" rel="prettyPhoto[1506]"
                                        title="เพื่อเป็นการขอบคุณ  บริษัท ทรู อินเทอร์เน็ต จํากัด  ที่ไว้วางใจใช้บริการวงจรสื่อสารความเร็วสูง
                                        ทางบริษัท  อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน) จึงจัดกิจกรรมแข่งขันโบว์ลิ่ง
                                        เพื่อกระชับความสัมพันธ์ระหว่างทั้ง 2 บริษัท  นำโดยคุณศรัญญา กาญจนโอภาส  Deputy Sale Director
                                        บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน)  ซึ่งกิจกรรมครั้งนี้
                                        ได้เสียงตอบรับจากลูกค้าที่มาเข้าร่วมกิจกรรมกันอย่างคับคั่ง
                                        ปิดการแข่งขันไปด้วยทีม Super Bowl  คว้าถ้วยรางวัลชนะเลิศพร้อมเงินสดมูลค่า 3,000 บาท
                                        หลังจากนั้นมีการร่วมรับประทานอาหารเย็นและสนุกสนานกับกิจกรรมลุ้นของรางวัลอื่นๆ
                                        อีกมากมายก่อนอำลากันไปด้วยความประทับใจ   ณ  Blu-O , เอสพลานาด รัชดา เมื่อวันศุกร์ที่ 26  มิถุนายน  ที่ผ่านมา">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150628/img02.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img03.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img04.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img05.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img06.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img07.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img08.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img09.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img10.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img11.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img12.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img13.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img14.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img15.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img16.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                    <a class="preview" href="portfolio/150628/img17.jpg" rel="prettyPhoto[1506]" title="Bowling Challenge Interlink Telecom VS True Internet"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1604  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160403/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">กิจกรรมชมภาพยนตร์ Captain America 3</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160403/img01.jpg" rel="prettyPhoto[1604]"
                                        title="
กลับมาอีกครั้งสำหรับกิจกรรมชมภาพยนตร์เหมาโรงสุด Exclusive ที่บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) จัดให้สำหรับลูกค้าผู้มีอุปการคุณ กับภาพยนตร์ ” Captain America 3 : กัปตัน อเมริกา ศึกฮีโร่ระห่ำโลก” ภาพยนตร์ฟอร์มยักษ์ที่กวาดรายได้ในวันเปิดตัวถล่มทลาย เป็นการเดินทางมาสู่จุดแตกหักที่ต้องเลือกข้าง2ขั้ว ระหว่างกัปตันอเมริกาและไอรอนแมน โดยมหาศึกครั้งนี้มีซุปเปอร์ฮีโร่จากทั่วโลกเข้าร่วมประจัญบานกันด้วย ถือเป็นการเปิดเฟส3 สุดยิ่งใหญ่ของจักรวาล Marvel ที่ให้ทุกคนได้ร่วมลุ้นและเชียร์ไปพร้อมๆ กันกับภาพยนตร์แนวแอคชั่นระดับโลก และก่อนภาพยนตร์จะเริ่มฉาย เรายังมีกิจกรรมการเล่นเกมแจกของรางวัลกันอย่างสนุกสนานรวมถึงได้มีโอกาสพูดคุยกับลูกค้าร่วมกัน ณ The Emquartier เมื่อวันที่ 28 เมษายน 2559 ที่ผ่านมา
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160403/img02.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img03.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img04.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img05.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img06.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img07.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img08.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img09.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img10.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img11.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img12.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img13.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img14.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img15.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                    <a class="preview" href="portfolio/160403/img16.jpg" rel="prettyPhoto[1604]" title="กิจกรรมชมภาพยนตร์ Captain America 3'"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1509  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150902/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">งานสัมมนา Technology of Rack For Datacenter</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150902/img01.jpg" rel="prettyPhoto[1509]"
                                        title="
                                        งานสัมมนา  'Technology of Rack For Datacenter' โดยมีคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด( มหาชน) และ ผู้จัดการทั่วไป บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ให้เกียรติกล่าวเปิดงานสัมมนา ณ โรงแรมเจ้าพระยาปาร์ค รัชดา กทม.โดยภายในงานมีการจัดเเสดง 'Datacenter จำลอง' และเทคโนโลยี Rack ซึ่งมีผู้ลงทะเบียนมาร่วมงานกว่า 250 ท่าน
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150902/img02.jpg" rel="prettyPhoto[1509]" title="งานสัมมนา 'Technology of Rack For Datacenter'"></a>
                                    <a class="preview" href="portfolio/150902/img03.jpg" rel="prettyPhoto[1509]" title="งานสัมมนา 'Technology of Rack For Datacenter'"></a>
                                    <a class="preview" href="portfolio/150902/img04.jpg" rel="prettyPhoto[1509]" title="งานสัมมนา 'Technology of Rack For Datacenter'"></a>
                                    <a class="preview" href="portfolio/150902/img05.jpg" rel="prettyPhoto[1509]" title="งานสัมมนา 'Technology of Rack For Datacenter'"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1508  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150802/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอม และมูลนิธิอินเตอร์ลิ้งค์ชวนกันทำดีเพื่อแม่</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150802/img01.jpg" rel="prettyPhoto[1508]"
                                        title="เมื่อวันที่ 2 สิงหาคมที่ผ่านมา บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน)
                                        และมูลนิธิอินเตอร์ลิ้งค์ให้ใจได้ร่วมบริจาคถุงกระดาษ เพื่อนำไปใช้ประโยชน์ในห้องหนังสือและสื่อธรรม
                                         หอจดหมายเหตุพุทธทาส อินทปัญโญ สวนวชิรเบญทัศ อีกทั้งยังได้ร่วมกิจกรรมกับเครือข่ายชีวิตสิกขา
                                         ในโครงการฉลาดทำบุญ ประดิษฐ์ “ตุ๊กตาล้มแล้วลุก” เพื่อมอบเป็นของขวัญสร้างกำลังใจแก่ผู้ป่วยโรคมะเร็ง
                                         รวมถึงใช้เป็นอุปกรณ์ในการทำกายภาพบำบัดสำหรับผุ้ป่วยซึ่งมีอาการกล้ามเนื้ออ่อนแรงจากการทำเคมีบำบัด
                                         (โดยใช้มือบีบที่ตัวตุ๊กตาแล้วคลายออก ซึ่งเป็นการบริหารกล้ามเนื้อรูปแบบหนึ่ง) นอกจากนี้
                                         ตุ๊กตาดังกล่าวจะถูกใช้เป็นสื่อแทน “ปริศนาธรรม” ที่จะได้ส่งสารไปถึงผู้ป่วยด้วยในคราวเดียวกัน
                                          กิจกรรมดังกล่าวเป็นการส่งเสริมการทำดีเนื่องในวันแม่แห่งชาติสำหรับท่านที่สนใจกิจกรรมดีแบบนี้สามารถเข้า
                                          ไปดูตารางกิจกรรมและลงทะเบียนได้ที่ www.bia.or.th
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150802/img02.jpg" rel="prettyPhoto[1508]" title="อินเตอร์ลิ้งค์ เทเลคอม และมูลนิธิอินเตอร์ลิ้งค์ชวนกันทำดีเพื่อแม่"></a>
                                    <a class="preview" href="portfolio/150802/img03.jpg" rel="prettyPhoto[1508]" title="อินเตอร์ลิ้งค์ เทเลคอม และมูลนิธิอินเตอร์ลิ้งค์ชวนกันทำดีเพื่อแม่"></a>
                                    <a class="preview" href="portfolio/150802/img04.jpg" rel="prettyPhoto[1508]" title="อินเตอร์ลิ้งค์ เทเลคอม และมูลนิธิอินเตอร์ลิ้งค์ชวนกันทำดีเพื่อแม่"></a>
                                    <a class="preview" href="portfolio/150802/img05.jpg" rel="prettyPhoto[1508]" title="อินเตอร์ลิ้งค์ เทเลคอม และมูลนิธิอินเตอร์ลิ้งค์ชวนกันทำดีเพื่อแม่"></a>
                                    <a class="preview" href="portfolio/150802/img06.jpg" rel="prettyPhoto[1508]" title="อินเตอร์ลิ้งค์ เทเลคอม และมูลนิธิอินเตอร์ลิ้งค์ชวนกันทำดีเพื่อแม่"></a>
                                    <a class="preview" href="portfolio/150802/img07.jpg" rel="prettyPhoto[1508]" title="อินเตอร์ลิ้งค์ เทเลคอม และมูลนิธิอินเตอร์ลิ้งค์ชวนกันทำดีเพื่อแม่"></a>
                                    <a class="preview" href="portfolio/150802/img08.jpg" rel="prettyPhoto[1508]" title="อินเตอร์ลิ้งค์ เทเลคอม และมูลนิธิอินเตอร์ลิ้งค์ชวนกันทำดีเพื่อแม่"></a>
                                    <a class="preview" href="portfolio/150802/img09.jpg" rel="prettyPhoto[1508]" title="อินเตอร์ลิ้งค์ เทเลคอม และมูลนิธิอินเตอร์ลิ้งค์ชวนกันทำดีเพื่อแม่"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1503  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150324/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Carriers World Asia 2015 at Grand Millennium Sukhumvit</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150324/img01.jpg" rel="prettyPhoto[1503]"
                                        title="Carriers world Asia 2015 มีผู้เข้าร่วมงานสัมมนากว่า 300 คน จากบริษัทโทรคมนาคมที่มีชื่อเสียงจากจากนานาประเทศ
     เช่น ฮ่องกง สิงคโปร์ จีน เวียดนาม รวมทั้งจากทางฝั่งยุโรป ถือเป็นงานใหญ่ที่มีผู้ให้ความสนใจเข้าร่วมประชุมแลกเปลี่ยนความก้าวหน้าทางการโทรคมนาคมสื่อสาร
      รวมไปถึงการต่อยอดทางธุรกิจร่วมกัน โดยเมื่อตอนเย็นของเมื่อวานนี้( 24 มีนาคม) ทางคุณณัฐนัย อนันตรัมพร
      กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกด ได้รับเชิญเป็นวิทยากรแลกเปลี่ยนความคิดเห็นในการพัฒนาธุรกิจภายใต้หัวข้อ
       How can Carriers differentiate themselves and their offerings? ซึ่งทางอินเตอร์ลิ้งค์เทเลคอมได้นำเสนอในเรื่องของความ Flexible
       ในการให้บริการเพื่อให้ตอบสนองต่อความต้องการของลูกค้าอย่างแท้จริง
       รวมถึงมีการเพิ่มมูลค่าให้กับการบริการโดยเน้นในเรื่องของการให้บริการที่มีคุณภาพ
       มีการดูแลลูกค้าตลอด 24 ชั่งโมง 7 วันและกิจกรรมพิเศษเพื่อสร้างความประทับใจให้กับลูกค้าที่ใช้บริการ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150324/img02.jpg" rel="prettyPhoto[1503]" title="Carriers World Asia 2015 at Grand Millennium Sukhumvit"></a>
                                    <a class="preview" href="portfolio/150324/img03.jpg" rel="prettyPhoto[1503]" title="Carriers World Asia 2015 at Grand Millennium Sukhumvit"></a>
                                    <a class="preview" href="portfolio/150324/img04.jpg" rel="prettyPhoto[1503]" title="Carriers World Asia 2015 at Grand Millennium Sukhumvit"></a>
                                    <a class="preview" href="portfolio/150324/img05.jpg" rel="prettyPhoto[1503]" title="Carriers World Asia 2015 at Grand Millennium Sukhumvit"></a>
                                    <a class="preview" href="portfolio/150324/img06.jpg" rel="prettyPhoto[1503]" title="Carriers World Asia 2015 at Grand Millennium Sukhumvit"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1509  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150906/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150906/img01.jpg" rel="prettyPhoto[1509]"
                                        title="
                                        บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) จัดพิธีทำบุญเลี้ยงพระ ในวันที่ 25 กันยายน  ที่ผ่านมา ด้วยการประกอบพิธีเจริญพระพุทธมนต์ทำบุญบริษัทฯ เพื่อความเป็นสิริมงคลให้กับศูนย์สำรองข้อมูล อินเตอร์ลิ้งค์ ดาต้าเซ็นเตอร์ โดยนิมนต์พระสงฆ์จำนวน 9 รูป จากวัดพระราม 9 กาญจนาภิเษก จังหวัดกรุงเทพมหานคร มาทำพิธีเจริญพระพุทธมนต์ เพื่อความเป็นสิริมงคล จากนั้นคณะผู้บริหารและพนักงาน ได้ร่วมกันถวายจตุปัจจัยไทยทานและถวายภัตตาหารเพลแด่พระภิกษุสงฆ์  เมื่อเสร็จสิ้นพิธีสงฆ์ ผู้ร่วมงานได้รับประทานอาหารร่วมกัน
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150906/img02.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img03.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img04.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img05.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img06.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img07.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img08.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img09.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img10.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img11.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                    <a class="preview" href="portfolio/150906/img12.jpg" rel="prettyPhoto[1509]" title="พิธีทำบุญเลี้ยงพระวันที่ 25 กันยายน  2558"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1506  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150620/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">กิจกรรมจิตอาสาของกลุ่มบริษัทอินเตอร์ลิ้งค์</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150620/img01.jpg" rel="prettyPhoto[1506]"
                                        title="หนึ่งในกิจกรรมจิตอาสาของกลุ่มบริษัทอินเตอร์ลิ้งค์ ที่ร่วมมือกันจัดกิจกรรมเลี้ยงอาหารกลางวัน สันทนาการ
                                        และแบ่งปันสิ่งของให้กับน้องๆ ณ โรงเรียนหมู่บ้านเด็ก มูลนิธิเด็ก จังหวัดกาญจนบุรี เมื่อวันที่ 20 มิถุนายน ที่ผ่านมา
                                        โดยกลุ่มบริษัทอินเตอร์ลิ้งค์ ตั้งใจที่จะส่งเสริมการสร้างจิตสำนึกด้านความรับผิดชอบต่อสังคม
                                        และการเป็นผู้ให้ ทั้งทางตรงและทางอ้อมให้กับพนักงานอย่างต่อเนื่อง">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150620/img02.jpg" rel="prettyPhoto[1506]" title="กิจกรรมจิตอาสาของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150620/img03.jpg" rel="prettyPhoto[1506]" title="กิจกรรมจิตอาสาของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150620/img04.jpg" rel="prettyPhoto[1506]" title="กิจกรรมจิตอาสาของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150620/img05.jpg" rel="prettyPhoto[1506]" title="กิจกรรมจิตอาสาของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150620/img06.jpg" rel="prettyPhoto[1506]" title="กิจกรรมจิตอาสาของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                    <a class="preview" href="portfolio/150620/img07.jpg" rel="prettyPhoto[1506]" title="กิจกรรมจิตอาสาของกลุ่มบริษัทอินเตอร์ลิ้งค์"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1507  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150704/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอม ร่วมกับ ซีเอส ล็อกซอินโฟ  ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150704/img01.jpg" rel="prettyPhoto[1507]"
                                        title="วันที่ 4 กรกฏาคม 2558 บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด(มหาชน) ออกบูธ ณ บริษัท ซูเลียน
                                        สำนักงานใหญ่ โดยภายในงานมีการประชาสัมพันธ์ให้ความรู้ถึงการให้บริการของทั้งสองบริษัท
                                        มีกิจกรรมเล่นเกมต่างๆเพื่อชิงของรางวัลมากมาย ซึ่งได้รับความสนใจจากผู้ร่วมงานเป็นอย่างดี">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150704/img02.jpg" rel="prettyPhoto[1507]" title="บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่"></a>
                                    <a class="preview" href="portfolio/150704/img03.jpg" rel="prettyPhoto[1507]" title="บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่"></a>
                                    <a class="preview" href="portfolio/150704/img04.jpg" rel="prettyPhoto[1507]" title="บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่"></a>
                                    <a class="preview" href="portfolio/150704/img05.jpg" rel="prettyPhoto[1507]" title="บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่"></a>
                                    <a class="preview" href="portfolio/150704/img06.jpg" rel="prettyPhoto[1507]" title="บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่"></a>
                                    <a class="preview" href="portfolio/150704/img07.jpg" rel="prettyPhoto[1507]" title="บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่"></a>
                                    <a class="preview" href="portfolio/150704/img08.jpg" rel="prettyPhoto[1507]" title="บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่"></a>
                                    <a class="preview" href="portfolio/150704/img09.jpg" rel="prettyPhoto[1507]" title="บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่"></a>
                                    <a class="preview" href="portfolio/150704/img10.jpg" rel="prettyPhoto[1507]" title="บริษัทอินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ร่วมกับ บริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ออกบูธ ณ บริษัท ซูเลียน สำนักงานใหญ่"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1505  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150527/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Chic & Chill Private Luncheon by Interlink Telecom</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150527/img01.jpg" rel="prettyPhoto[1505]"
                                        title="เพื่อเป็นการขอบคุณลูกค้า และกระชับความสัมพันธ์อันดีกับบริษัท ทรู ยูนิเวอร์แซล คอนเวอร์เจ้นซ์ จำกัด
                                        Interlink Telecom จึงได้จัดกิจกรรม Chic & Chill private luncheon by Interlink Telecom ณ ร้าน Mix
                                        Restaurant , The Shoppes Grand Rama 9 ทั้งนี้เพื่อให้มีโอกาสได้พบปะสังสรรค์ พูดคุยและรับประทานอาหาร
                                        กลางวันร่วมกันในบรรยากาศที่แสนอบอุ่น นับเป็นปาร์ตี้อาหารกลางวัน ที่สนุกสนานและเป็นกันเองอย่างยิ่ง เมื่อวันพุธที่
                                        27 พฤษภาคม 2558 ที่ผ่านมา">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150527/img02.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Luncheon by Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150527/img03.jpg" rel="prettyPhoto[1505]"  title="Chic & Chill Private Luncheon by Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150527/img04.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Luncheon by Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150527/img05.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Luncheon by Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150527/img06.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Luncheon by Interlink Telecom"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1411  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/141114/300x200.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#">
                                    <span class="lead">ลงนามบันทึกข้อตกลงความร่วมมือในโครงการ  Miracle  Eyes</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/141114/img02.jpg" rel="prettyPhoto[1411]"
                                    title="Interlink Telecom ร่วมมือ บช.น. ในโครงการระบบแจ้งเหตุด้วยกล้องวงจรปิด Miracle Eyes
เมื่อวันที่ 14 พฤศจิกายน 2554  คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์
และ พล.ต.ต.ดร.อดุลย์ ณรงค์ศักดิ์ รองผู้บัญชาการตำรวจนครบาล สำนักงานตำรวจแห่งชาติ
ร่วมลงนามบันทึกข้อตกลงความร่วมมือการดำเนินการโครงการนวัตกรรมศูนย์สั่งการ กล้องทีวีวงจรปิด 
เพื่อป้องกันปราบปรามอาชญากรรมแบบเบ็ดเสร็จ (Miracle Eyes)
เป็นโครงการที่เกิดขึ้นเพื่อดูแลความปลอดภัยในชีวิตและทรัพย์สินของประชาชนและของสังคมส่วนรวม
ในเขตความดูแลของกองบัญชาการตำรวจนครบาล
โดยใช้ระบบกล้องวงจรปิดผสานกับโครงข่ายเทคโนโลยีสารสนเทศของบริษัทฯ
เพื่อป้องกันและปราบปรามอาชญากรรมให้ลดน้อยลง โดยมีผู้บริหารระดับสูงทั้ง 2
หน่วยงานร่วมเป็นสักขีพยาน ณ ห้องประชุมใหญ่กองบัญชาการตำรวจนครบาล(บช.น.)  กรุงเทพฯ">
                                        <i class="fa fa-eye"></i>View More</a>
                                    <a class="preview" href="portfolio/141114/img01.jpg" rel="prettyPhoto[1411]" title="ลงนามบันทึกข้อตกลงความร่วมมือในโครงการ  Miracle  Eyes"></a>
                                    <a class="preview" href="portfolio/141114/img03.jpg" rel="prettyPhoto[1411]" title="ลงนามบันทึกข้อตกลงความร่วมมือในโครงการ  Miracle  Eyes"></a>

                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1605  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160501/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Refresh & Refill Energy Luncheon</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160501/img01.jpg" rel="prettyPhoto[1605]"
                                        title="
นับเป็นอีกหนึ่งกิจกรรมประทับใจ เพื่อสร้างความสุขให้กับ Partner ที่ร่วมมือทางธุรกิจมาอย่างยาวนานกับบริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) กับกิจกรรม “Refresh & Refill Energy Luncheon” ณ ร้าน At Neta Fish & Meat @The Street โดยบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้จัดกิจกรรมนี้ขึ้นเพื่อขอบคุณลูกค้าและกระชับความสัมพันธ์ระหว่างกันในบรรยากาศที่อบอุ่น เมื่อวันพุธที่ 18 พฤษภาคม 2559 ที่ผ่านมา
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160501/img02.jpg" rel="prettyPhoto[1605]" title="Refresh & Refill Energy Luncheon'"></a>
                                    <a class="preview" href="portfolio/160501/img03.jpg" rel="prettyPhoto[1605]" title="Refresh & Refill Energy Luncheon'"></a>
                                    <a class="preview" href="portfolio/160501/img04.jpg" rel="prettyPhoto[1605]" title="Refresh & Refill Energy Luncheon'"></a>
                                    <a class="preview" href="portfolio/160501/img05.jpg" rel="prettyPhoto[1605]" title="Refresh & Refill Energy Luncheon'"></a>
                                    <a class="preview" href="portfolio/160501/img06.jpg" rel="prettyPhoto[1605]" title="Refresh & Refill Energy Luncheon'"></a>
                                    <a class="preview" href="portfolio/160501/img07.jpg" rel="prettyPhoto[1605]" title="Refresh & Refill Energy Luncheon'"></a>
                                    <a class="preview" href="portfolio/160501/img08.jpg" rel="prettyPhoto[1605]" title="Refresh & Refill Energy Luncheon'"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1509  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150905/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">งานสัมมนากลุ่มอาจารย์และที่ปรึกษามหาวิทยาลัย  Open Cabling System For The Future</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150905/img01.jpg" rel="prettyPhoto[1509]"
                                        title="
                                        งานสัมมนากลุ่มอาจารย์และที่ปรึกษามหาวิทยาลัย
Open Cabling System For The Future
จัดขึ้น ณ โรงแรมพูลแมน พัทยา จ.ชลบุรี
โดยมีสถาบันการศึกษาแนวหน้าของไทยให้ความสนใจเป็นจำนวนมาก
ภายในงานพบกับหัวข้อสัมมนาที่น่าสนใจ อาทิ ...
- Advanced Installation Open Cabling
- Advanced Installation Workshop
- Trend & Technology 2015
- INTERLINK Telecom : A Walk Through Future of Data Communication and Data Center
พร้อมร่วมดินเนอร์สุดหรูสัมผัสกับวิวทะเลเเละบรรยากาศเย็นสบายใน ธีม ฮาวาย สุดท้ายร่วมรับประทานกลางวันก่อนกลับ ที่ร้านอาหารชื่อดัง ของเมืองพัทยา ท่ามกลางบรรยากาศริมทะเล
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150905/img02.jpg" rel="prettyPhoto[1509]" title="งานสัมมนากลุ่มอาจารย์และที่ปรึกษามหาวิทยาลัย  “Open Cabling System For The Future “"></a>
                                    <a class="preview" href="portfolio/150905/img03.jpg" rel="prettyPhoto[1509]" title="งานสัมมนากลุ่มอาจารย์และที่ปรึกษามหาวิทยาลัย  “Open Cabling System For The Future “"></a>
                                    <a class="preview" href="portfolio/150905/img04.jpg" rel="prettyPhoto[1509]" title="งานสัมมนากลุ่มอาจารย์และที่ปรึกษามหาวิทยาลัย  “Open Cabling System For The Future “"></a>
                                    <a class="preview" href="portfolio/150905/img05.jpg" rel="prettyPhoto[1509]" title="งานสัมมนากลุ่มอาจารย์และที่ปรึกษามหาวิทยาลัย  “Open Cabling System For The Future “"></a>
                                    <a class="preview" href="portfolio/150905/img06.jpg" rel="prettyPhoto[1509]" title="งานสัมมนากลุ่มอาจารย์และที่ปรึกษามหาวิทยาลัย  “Open Cabling System For The Future “"></a>

                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                     <div class="portfolio-item 1503  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150309/300x200.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">เปิดโครงการ Miracle eyes หรือระบบแจ้งเหตุด้วยกล้องทีวีวงจรปิด ระยะที่ 2</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150309/img01.png" rel="prettyPhoto[1503]"
                                        title="คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด
                  เข้าร่วมงานแถลงข่าวโครงการ Miracle eyes หรือ โครงการระบบแจ้งเหตุด้วยกล้องทีวีวงจรปิด
                  ระยะที่ 2 โดยมีพล.ต.อ สมยศ พุ่มพันธุ์ม่วง ผู้บัญชาการตำรวจแห่งชาติเป็นประธาน
                  ณ ห้องประชุมกองบัญชาการตำรวจนครบาล ถือเป็นโครงการที่จะช่วยดูแลความปลอดภัยของประชาชน
                  จากเหตุอาชญากรรมไม่ว่าจะเป็นเหตุที่เกิดในบ้านเรือนหรือเหตุที่เกิดขึ้นในรถยนต์สาธารณะ
                  สวนสาธารณะ ถนนคนเดิน และพื้นที่สุ่มเสี่ยง โดยจะประสานกับกรมการขนส่งทางบกจัดทำระบบ Smart Taxi
                  เพื่อนำโครงการ Miracle eyes เข้ามาใช้แจ้งเหตุอาชญากรรมในรถแท็กซี่
                  รวมทั้งจะมีการพัฒนาระบบเพื่อรองรับโครงการบ้านปลอดภัยในช่วงเทศกาลต่างๆที่ประชาชนไม่อยู่บ้าน
                  โดยทางอินเตอร์ลิ้งค์ เทเลคอม ได้ร่วมเป็นส่วนหนึ่งในการพัฒนาโครงการนี้
                  ให้มีความก้าวหน้าและสามารถให้บริการกับประชาชนได้อย่างมีประสิทธิภาพต่อไป"
                  title="เปิดโครงการ Miracle eyes หรือระบบแจ้งเหตุด้วยกล้องทีวีวงจรปิด ระยะที่ 2">
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1412  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/141220/300x200.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอม เปิดบ้านต้อนรับทีมวิศวกรรมจากซีเอส ล็อกซอินโฟ</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/141220/img02.png" rel="prettyPhoto[1412]"
                                        title="INTERLINK TELECOM เปิดบ้านต้อนรับ
ทีมวิศวกรรมจาก CS Loxinfo
ทางบริษัท อินเตอร์ลิ้งค์  เทเลคอม จำกัด  นำโดยคุณณัฐนัย อนันตรัมพร
MD Managing Director พร้อมทีมวิศวกรรม ได้เปิดบ้านต้อนรับบริษัท
ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ซึ่งนำทีมโดยคุณสปันนา คงธนาฤทธิ์
Assistant Director Corporate Network Support
เพื่อทำการแลกเปลี่ยนและสร้างแนวทางในการทำงานร่วมกันอันจะเพิ่มประสิทธิภาพการให้บริการของทั้ง 2 บริษัท
 โดยคำนึงถึงผลประโยชน์สูงสุดของลูกค้าผู้ใช้บริการทุกราย
 ในวันที่ 20 พฤศจิกายนที่ผ่านมา ณ อาคารอินเตอร์ลิ้งค์  ถ.รัชดาภิเษก">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/141220/img01.jpg" rel="prettyPhoto[1412]" title="อินเตอร์ลิ้งค์ เทเลคอม เปิดบ้านต้อนรับทีมวิศวกรรมจากซีเอส ล็อกซอินโฟ"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1412  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/141227/300x200.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Interlink Data Center ออกอากาศทางรายการ Hitech Spring</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/141227/img01.jpg" rel="prettyPhoto[1412]"
                                        title="พิธีกรไอทีอารมณ์ดี คุณหนุ่ย พงศ์สุข หิรัญพฤกษ์  พร้อมทีมงานให้ความสนใจในการบริการของ
                    Interlink Data Center  เพื่อถ่ายทำรายการ Hitech Spring   ซึ่งออกอากาศไปเมื่อวันเสาร์ที่
                    27 ธันวาคม 2557  ทางช่อง Spring News  โดยในรายการคุณใหม่  ภัททิยาพรรณ์  โกศลสุวรรณ
                     Sales Executive คนสวยของเราได้พาคุณหนุ่ยเข้าชม Interlink Data Center ที่มีความปลอด
                        ภัยสูงและใช้เทคโนโลยีที่ทันสมัยระดับโลก เพื่อรองรับทุกความต้องการของลูกค้า">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/141227/img02.jpg" rel="prettyPhoto[1412]" title="Interlink Data Center ออกอากาศทางรายการ Hitech Spring"></a>
                                    <a class="preview" href="https://www.youtube.com/watch?v=LVl44cDxT54" rel="prettyPhoto[1412]"  title="Interlink Data Center ออกอากาศทางรายการ Hitech Spring"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1505  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150523/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#" ><span class="lead">คาร์แรลลี่และงานท่องเที่ยวประจำปี 2558 ณ โรงแรมรอยัล คลิฟฟ์ บีช</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150523/img01.jpg" rel="prettyPhoto[1505]"
                                        title=" คาร์ แรลลี่และงานท่องเที่ยวประจำปี 2558 ณ โรงแรมรอยัล คลิฟฟ์ บีช เมื่อวันที่ 23-24 พฤษภาคม 2558 ที่ผ่านมา บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้จัดกิจกรรม
                                        คาร์ แรลลี่และงานท่องเที่ยวประจำปี 2558 ณ โรงแรมรอยัล คลิฟฟ์ บีช โรงแรมระดับ 5 ดาวของพัทยา เพื่อเป็นการ
                                        ขอบคุณพนักงานทุกคนที่ร่วมแรงร่วมใจกันผลักดันให้บริษัทฯเติบโตอย่างต่อเนื่อง โดยมีคุณณัฐนัยและคุณชลิดา
                                        อนันตรัมพร เป็นประธานในพิธีเปิดงานคาร์ แรลลี่ ณ Interlink R&D Center นอกจากนี้ภายในงานเลี้ยงยามค่ำคืน
                                        ได้จัดปาร์ตี้ภายใต้ธีม “Vivid Color of A.E.C.” เพื่อตอกย้ำการให้บริการของบริษัทฯที่ครอบคลุมไปยังหลายประเทศในอาเซียน
                                        ปิดท้ายด้วยคอนเสิร์ตสุดมันส์จากวง Seoul in two พร้อมกิจกรรมลุ้นรับของรางวัลมากมาย
                                        ทั้งหมดนี้เพื่อแสดงให้เห็นว่าอินเตอร์ลิ้งค์ เทเลคอมให้ความสำคัญกับพนักงานทุกคนที่เป็นฟันเฟืองสำคัญของบริษัทฯ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150523/img02.jpg" rel="prettyPhoto[1505]" title="คาร์แรลลี่และงานท่องเที่ยวประจำปี 2558 ณ โรงแรมรอยัล คลิฟฟ์ บีช"></a>
                                    <a class="preview" href="portfolio/150523/img03.jpg" rel="prettyPhoto[1505]" title="คาร์แรลลี่และงานท่องเที่ยวประจำปี 2558 ณ โรงแรมรอยัล คลิฟฟ์ บีช"></a>
                                    <a class="preview" href="portfolio/150523/img04.jpg" rel="prettyPhoto[1505]" title="คาร์แรลลี่และงานท่องเที่ยวประจำปี 2558 ณ โรงแรมรอยัล คลิฟฟ์ บีช"></a>
                                    <a class="preview" href="portfolio/150523/img05.jpg" rel="prettyPhoto[1505]" title="คาร์แรลลี่และงานท่องเที่ยวประจำปี 2558 ณ โรงแรมรอยัล คลิฟฟ์ บีช"></a>
                                    <a class="preview" href="portfolio/150523/img06.jpg" rel="prettyPhoto[1505]" title="คาร์แรลลี่และงานท่องเที่ยวประจำปี 2558 ณ โรงแรมรอยัล คลิฟฟ์ บีช"></a>
                                    <a class="preview" href="portfolio/150523/img07.jpg" rel="prettyPhoto[1505]" title="คาร์แรลลี่และงานท่องเที่ยวประจำปี 2558 ณ โรงแรมรอยัล คลิฟฟ์ บีช"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1605  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160502/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ITEL Exclusive Workshop How to be  Barista</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160502/img01.jpg" rel="prettyPhoto[1605]"
                                        title="
บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) เอาใจคอกาแฟ จัดกิจกรรม ITEL Exclusive Workshop How to be Barista กิจกรรมพิเศษเฉพาะลูกค้าของอินเตอร์ลิ้งค์ เทเลคอม “ ฝึกอบรมการชงกาแฟมืออาชีพ Barista Training Course และ ศิลปะการชงกาแฟ Latte Art” โดยวิทยากรผู้เชี่ยวชาญ อาจารย์กาย Barrio Coffee ภายในงาน ผู้เข้าร่วมอบรมจะได้รับความรู้เชิงลึกมากมาย อาทิ ทิศทางการดำเนินธุรกิจกาแฟ ความรู้เรื่องกาแฟเชิงลึก ไม่ว่าจะเป็นแหล่งเพาะปลูก แหล่งเก็บเกี่ยว การบด การคั่ว การเก็บรักษา และส่วนประกอบที่สำคัญต่างๆยิ่งไปกว่านั้น ผู้ร่วมอบรมจะได้ลงมือปฏิบัติจริงในการชงกาแฟที่เกิดจากฝีมือของตนเองและไอเดียสร้างสรรค์ให้กาแฟแสนธรรมดากลายเป็นกาแฟที่ไม่เหมือนใครจนกลายเป็นศิลปะอีกแบบหนึ่ง โดยวาดลวดลายลงบนฟองนมแต่งแก้วกาแฟให้สวยงามยิ่งขึ้น ที่เรียกว่า ลาเต้อาร์ต (Latte Art) โดยมีวิทยากรและกูรูด้านกาแฟรองแชมป์ประเทศไทย มาต่อแถวให้ความรู้กันเต็มที่ เอาใจผู้อบรมคอกาแฟตัวจริงที่รักในเสน่ห์ของกาแฟสด ได้รู้ลึก รู้จริงเรื่องกาแฟ มากยิ่งขึ้น จัดขึ้น ณ อาคารอินเตอร์ลิ้งค์ เมื่อวันที่ 14 พฤษภาคม 2559 ที่ผ่านมา
                                        ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160502/img02.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img03.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img04.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img05.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img06.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img07.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img08.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img09.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img10.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img11.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img12.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img13.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img14.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img15.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img16.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img17.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img18.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img19.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img20.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img21.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img22.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img23.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                    <a class="preview" href="portfolio/160502/img24.jpg" rel="prettyPhoto[1605]" title=" ITEL Exclusive Workshop How to be  Barista"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->


                    <div class="portfolio-item 1503  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150323/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150323/img06.jpg" rel="prettyPhoto[1503]"
                                        title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom  ผู้บริหารและพนักงานฝ่ายวิศวกรรม
                                        จากบริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ได้เข้าร่วมกิจกรรมฟุตบอลประเพณีระหว่างซีเอส ล็อกซอินโฟ
                                        และอินเตอร์ลิ้งค์ เทเลคอม ณ สนาม Super kick ซึ่งถือเป็นการดวลแข้งกันครั้งแรกเพื่อกระชับความสัมพันธ์
                                        ระหว่างพนักงานฝ่ายวิศวกรรมของทั้งสองบริษัท โดยทางคุณณัฐนัย อนันตรัมพร ผู้บริหารของบริษัทอินเตอร์ลิ้งค์
                                        เทเลคอม จำกัด ได้ร่วมแสดงฝีเท้าในเกมนี้ด้วย ปิดเกมไปด้วยทางซีเอสล็อกซโชว์ลีลาชนะไป 5 ประตูต่อ 3
                                        คว้าถ้วยรางวัลชนะเลิศไปครอง หลังจากนั้นจึงร่วมรับประทานอาหารและสนุกสนานกับกิจกรรมมันส์ๆเพื่อลุ้นของรางวัลที่ระลึก
                                        ก่อนอำลากันไปด้วยความประทับใจ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150323/img02.jpg" rel="prettyPhoto[1503]" title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150323/img03.jpg" rel="prettyPhoto[1503]"  title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150323/img04.jpg" rel="prettyPhoto[1503]" title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150323/img05.jpg" rel="prettyPhoto[1503]" title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150323/img01.jpg" rel="prettyPhoto[1503]" title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150323/img07.jpg" rel="prettyPhoto[1503]" title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150323/img08.jpg" rel="prettyPhoto[1503]" title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150323/img09.jpg" rel="prettyPhoto[1503]" title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom"></a>
                                    <a class="preview" href="portfolio/150323/img10.jpg" rel="prettyPhoto[1503]" title="งานฟุตบอลประเพณี CS Loxinfo & Interlink Telecom"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1506  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150603/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">การไฟฟ้าส่วนภูมิภาค (PEA) เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150603/img01.jpg" rel="prettyPhoto[1506]"
                                        title="เมื่อวันที่ 3 มิถุนายน 2558 ที่ผ่านมา  ตัวแทนของการไฟฟ้าส่วนภูมิภาค (PEA)
                                        เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ที่มีความปลอดภัยและทันสมัยระดับโลก
                                         โดยมีคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด(มหาชน)
                                         และบริษัท อินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ จำกัด  ให้การต้อนรับคณะผู้เยี่ยมชมอย่างเป็นกันเอง
                                         สำหรับอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ ได้ออกแบบตามมาตรฐาน TIA 942 ระดับ Tier-3 การันตี
                                         uptime อยู่ที่ 99.982%  รวมถึงตัวโครงสร้างอาคารที่ออกแบบมาเพื่อช่วยประหยัดพลังงาน
                                         พร้อมให้บริการด้วยทีมงานที่มีความเชี่ยวชาญและดูแลตลอด 24 ชั่วโมง 7 วัน
                                         ทำให้มั่นใจได้ว่าลูกค้าที่มาใช้บริการจะได้รับประสิทธิภาพสูงสุด">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150603/img02.jpg" rel="prettyPhoto[1506]" title="การไฟฟ้าส่วนภูมิภาค (PEA) เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/150603/img03.jpg" rel="prettyPhoto[1506]"  title="การไฟฟ้าส่วนภูมิภาค (PEA) เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/150603/img04.jpg" rel="prettyPhoto[1506]" title="การไฟฟ้าส่วนภูมิภาค (PEA) เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/150603/img05.jpg" rel="prettyPhoto[1506]" title="การไฟฟ้าส่วนภูมิภาค (PEA) เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/150603/img06.jpg" rel="prettyPhoto[1506]" title="การไฟฟ้าส่วนภูมิภาค (PEA) เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/150603/img07.jpg" rel="prettyPhoto[1506]" title="การไฟฟ้าส่วนภูมิภาค (PEA) เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์"></a>
                                    <a class="preview" href="portfolio/150603/img08.jpg" rel="prettyPhoto[1506]" title="การไฟฟ้าส่วนภูมิภาค (PEA) เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1506  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150604/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Interlink Telecom Exclusive Business trip: The Perfect Singapore</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150604/img01.jpg" rel="prettyPhoto[1506]"
                                        title="บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้จัดกิจกรรม Exclusive Business Trip:
                                        The Perfect Singapore Experience พาลูกค้าผู้มีอุปการคุณที่เป็นพันธมิตรของบริษัทฯกว่า 30
                                        ท่าน บินลัดฟ้าไปดูงาน Communic Asia 2015 ณ ประเทศสิงคโปร์
                                        รวมทั้งท่องเที่ยวสัมผัสกับวัฒนธรรมที่ผสานไปอย่างกลมกลืนกับเทคโนโลยีที่ทันสมัย
                                        ไม่ว่าจะเป็น Fountain of Wealth , Universal Studio, Garden by the Bay
                                        และล่องเรือ Bumboat ชมบรรยากาศแสงสียามค่ำคืน รวมทั้งช้อปปิ้งสินค้าหลากหลายที่ถนนออร์ชาร์ด
                                        โดยมีคุณณัฐนัย อนันตรัมพร ผู้บริหารของ Interlink Telecom และทีมงานให้การต้อนรับและดูแลลูกค้าอย่างเป็นกันเอง
                                        เมื่อวันที่ 4-6 มิถุนายน 2558 ที่ผ่านม">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150604/img02.jpg" rel="prettyPhoto[1506]" title="Interlink Telecom Exclusive Business Trip: The Perfect Singapore"></a>
                                    <a class="preview" href="portfolio/150604/img03.jpg" rel="prettyPhoto[1506]"  title="Interlink Telecom Exclusive Business Trip: The Perfect Singapore"></a>
                                    <a class="preview" href="portfolio/150604/img04.jpg" rel="prettyPhoto[1506]" title="Interlink Telecom Exclusive Business Trip: The Perfect Singapore"></a>
                                    <a class="preview" href="portfolio/150604/img05.jpg" rel="prettyPhoto[1506]" title="Interlink Telecom Exclusive Business Trip: The Perfect Singapore"></a>
                                    <a class="preview" href="portfolio/150604/img06.jpg" rel="prettyPhoto[1506]" title="Interlink Telecom Exclusive Business Trip: The Perfect Singapore"></a>
                                    <a class="preview" href="portfolio/150604/img07.jpg" rel="prettyPhoto[1506]" title="Interlink Telecom Exclusive Business Trip: The Perfect Singapore"></a>
                                    <a class="preview" href="portfolio/150604/img08.jpg" rel="prettyPhoto[1506]" title="Interlink Telecom Exclusive Business Trip: The Perfect Singapore"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1503  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150322/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ธนาคารกสิกรไทยเยี่ยมชมอินเตอร์ลิ้งค์ ดาต้าเซ็นเตอร์</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150322/img01.jpg" rel="prettyPhoto[1503]"
                                        title="คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัดและบริษัท อินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ จำกัด
    ได้เป็นตัวแทนในการต้อนรับคุณดำรงค์ เลิศพลังสันติ ผู้ช่วยกรรมการผู้จัดการธนาคารกสิกรไทย จำกัด (มหาชน)
    และทีมงาน เข้าเยี่ยมชมศูนย์สำรองข้อมูลอินเตอร์ลิ้งค์ ดาต้า เซ็นเตอร์ที่มีความปลอดภัยและทันสมัย
    ออกแบบตามมาตรฐาน TIA 942 ระดับ Tier-3 นอกจากนี้ยังผ่านการรับรองมาตรฐานการจัดการด้านความปลอดภัยของข้อมูล
    ISO/IEC 27001:2013 รวมไปถึงเจ้าหน้าที่ของทางศูนย์สำรองข้อมูลที่เพียบพร้อมทั้งประสบการณ์ในการทำงานและความเต็มที่ในการให้บริการ" >
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1411  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/141113/300x200.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#">
                                        <span class="lead">Chic & Chill Exclusive Party with Inet</span></a>
                                    </h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/141113/img01.jpg" rel="prettyPhoto[1411]"
                                        title="Chic & Chill Exclusive Party with Inet
เมื่อวันที่ 13 พฤศจิกายน ที่ผ่านมา บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด
ได้จัดงานเลี้ยงขอบคุณลูกค้า บริษัท อินเทอร์เน็ตประเทศไทย จำกัด (มหาชน) (INET)
ภายใต้ชื่องาน “Chic & Chill Exclusive Party” โดยมีคุณณัฐนัย อนันตรัมพร
กรรมการผู้จัดการ กลุ่มบริษัท อินเตอร์ลิ้งค์ฯ เป็นประธานกล่าวเปิดงาน
ภายในงานมีการจัดกิจกรรมเล่นเกมส์ให้ร่วมสนุกมากมาย พร้อมทั้งแจก
iPhone 6 และ Mini Concert จากวง PlayGround
ที่มาร่วมสร้างสีสันและความบันเทิง ณ ร้าน Spring & Summer สุขุมวิท 49" >
                                        <i class="fa fa-eye"></i> View More</a>
                                        <a class="preview" href="portfolio/141113/img02.jpg" rel="prettyPhoto[1411]" title="Chic & Chill Exclusive Party with Inet" ></a>
                                        <a class="preview" href="portfolio/141113/img03.jpg" rel="prettyPhoto[1411]" title="Chic & Chill Exclusive Party with Inet"></a>
                                        <a class="preview" href="portfolio/141113/img04.png" rel="prettyPhoto[1411]" title="Chic & Chill Exclusive Party with Inet"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1505 col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/150525/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Chic & Chill Private Party with KSC</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/150525/img01.jpg" rel="prettyPhoto[1505]"
                                        title="เพื่อเป็นการสร้างความสัมพันธ์อันดีระหว่างบริษัท เค เอส ซี คอมเมอร์เชียล อินเตอร์เนต จำกัด และบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด
                                        (มหาชน) ทางอินเตอร์ลิ้งค์ เทเลคอมจึงจัดกิจกรรม Chic & Chill Private Party ณ คาราโอเกะ ซิตี้ เลียบทางด่วน
                                        รามอินทรา ซึ่งในงานนอกจากจะมีการเล่นเกมเพื่อชิงรางวัลแล้วยังมีการแข่งขันร้องเพลงคาราโอเกะ โดยทั้งเค เอส ซี
                                        และอินเตอร์ลิ้งค์เองต่างก็ไม่มีใครยอมใคร งัดลีลาและลูกคอออกมาโชว์กันอย่างเต็มที่ ก่อนอำลาจากกันไปด้วย
                                        ความสนุกสนานและเสียงหัวเราะ">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/150525/img02.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img03.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img04.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img05.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img06.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img07.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img08.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img09.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img10.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img11.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img12.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img13.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img14.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img15.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                    <a class="preview" href="portfolio/150525/img16.jpg" rel="prettyPhoto[1505]" title="Chic & Chill Private Party with KSC"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1603 col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160302/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ITEL- Exclusive Workshop</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160302/img01.jpg" rel="prettyPhoto[1603]"
                                        title="กิจกรรมพิเศษเฉพาะลูกค้าของอินเตอร์ลิ้งค์ เทเลคอมกับการ workshop การทำสวนในขวดแก้วกับอาจารย์จุ๊บ จาก Puppy leaf ซึ่งกิจกรรมนี้นอกจากจะช่วยสร้างสมาธิให้กับผู้เข้าร่วมกิจกรรมแล้ว ยังสร้างความสนุกสนานและความภาคภูมิใจในสวนขวดสวยๆที่เกิดจากฝีมือของตนเองด้วย และหลังจากการ workshop แล้ว อินเตอร์ลิ้งค์ยังสร้างประสบการณ์ใหม่ในการพักผ่อนกับการจิบชาที่แต่ละชนิดมีความน่าสนใจและเอกลักษณ์เฉพาะตัว ทำให้ลูกค้าทุกท่านได้รับความประทับใจกลับไปกันทุกท่าน จัดขึ้นเมื่อวันเสาร์ที่ 19 มีนาคม 2559 ณ ร้านชาปัญญา สุขุมวิท 59">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160302/img02.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img03.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img04.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img05.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img06.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img07.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img08.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img09.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img10.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img11.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img12.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img13.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img14.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img15.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img16.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img17.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img18.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img19.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img20.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img21.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img22.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img23.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img24.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img25.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img26.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img27.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img28.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img29.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img30.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img31.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img32.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img33.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img34.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img35.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img36.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img37.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                    <a class="preview" href="portfolio/160302/img38.jpg" rel="prettyPhoto[1603]" title="ITEL- Exclusive Workshop : Terrarium mini garden in the bottle"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1604 col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/160401/300x200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Play Girl & Play Boy : Summer Begin  Party</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/160401/img01.jpg" rel="prettyPhoto[1604]"
                                        title="คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ นำทีมงานบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) จัดกิจกรรมงาน Thank You Party ในธีมงานสุดเก๋ ‘Play Girl & Play Boy : Summer Begin Party’ สร้างความสุขให้กับ Partner ที่ร่วมมือทางธุรกิจมาอย่างยาวนานกับบริษัท ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) เพื่อเป็นโอกาสในการพบปะ สังสรรค์ พูดคุยและสร้างความสัมพันธ์อันดีระหว่างทีมงานทั้งสองบริษัท ภายในงานมี Mini Concert จากวง PlayGround และแขกรับเชิญสุดพิเศษดีเจสาว Sexy ระดับเอเชีย สุดฮอตอย่าง Double G ที่มาร่วมสร้างสีสันและความบันเทิง อีกทั้งยังได้สนุกกับการถ่ายรูปกับ Background และอุปกรณ์ต่างๆ ที่เข้ากับธีมของงานพร้อมรับ Photo Card กลับบ้านพิเศษสำหรับชาว CS Loxinfo เท่านั้น นอกจากนี้ยังมีกิจกรรมร่วมสนุกลุ้นของรางวัลใหญ่มากมาย อาทิเช่น ipad mini , Gift Voucher จาก Centara , Starbucks และรางวัลอื่นๆ อีกมากมาย งานนี้จัดขึ้นเมื่อวันศุกร์ที่ 1 เมษายน 2559 ณ Mix Restaurant&Bar Kaset-Nawamin บรรยากาศในงานเป็นไปด้วยความสนุกสนานและเป็นกันเองอย่างยิ่ง">
                                        <i class="fa fa-eye"></i> View More</a>
                                    <a class="preview" href="portfolio/160401/img02.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img03.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img04.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img05.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img06.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img07.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img08.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img09.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img10.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img11.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img12.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img13.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img14.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img15.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img16.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img17.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img18.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img19.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img20.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img21.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img22.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img23.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img24.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img25.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img26.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img27.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img28.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img29.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img30.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img31.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img32.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img33.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img34.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img35.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img36.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img37.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img38.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img39.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img40.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img41.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img42.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img43.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img44.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img45.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img46.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img47.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img48.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img49.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img50.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img51.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img52.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img53.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img54.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img55.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img56.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img57.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img58.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img59.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img60.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img61.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img62.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img63.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img64.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img65.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img66.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                    <a class="preview" href="portfolio/160401/img67.jpg" rel="prettyPhoto[1604]" title="Play Girl & Play Boy : Summer Begin  Party"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1505  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap" >
                            <img class="img-responsive" src="http://img.youtube.com/vi/-9rBYXWkWgI/mqdefault.jpg" alt="" >
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ออกอากาศทางรายการ Bualuang iChannel</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="https://www.youtube.com/watch?v=-9rBYXWkWgI" rel="prettyPhoto"
                                        title="ภาพรวมการทำธุรกิจของกลุ่มบริษัทอินเตอร์ลิ้งค์ ออกอากาศทางรายการ Bualuang iChannel
                                        คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน)
                                        ได้กล่าวถึงภาพรวมในการทำธุรกิจของกลุ่มบริษัทอินเตอร์ลิ้งค์ รวมไปถึงกลยุทธ์ในการทำธุรกิจ,
                                        แผนการดำเนินงานของบริษัท และแนวทางในการดำเนินงานโครงการใหม่อย่าง Digital Economy
                                        ว่าจะมีทิศทางในการดำเนินงานอย่างไร ผ่านทางรายการ Bualuang iChannel เมื่อวันอังคารที่ 26 พฤษภาคม 2558 ที่ผ่านมา">
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1508  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap" >
                            <img class="img-responsive" src="http://img.youtube.com/vi/6_I21WFj2qQ/mqdefault.jpg" alt="" >
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">CEO Talk โดยคุณณัฐนัย อนันตรัมพร กลุ่มบริษัทอินเตอร์ลิ้งค์</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="https://www.youtube.com/watch?v=6_I21WFj2qQ" rel="prettyPhoto"
                                        title="คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ให้เกียรติถ่ายทอดประสบการณ์  วิสัยทัศน์ในช่วง CEO Talk  ภายในงานเฉลิมฉลองเนื่องในโอกาสที่บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ย้ายไปสู่ SET และร่วมแสดงความยินดีกับบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) เตรียมเข้าจดทะเบียน mai โดยได้บรรยายถึงวิวัฒนาการของเทคโนโลยีในอดีตซึ่งส่งผลถึงเทคโนโลยีที่เราใช้ในปัจจุบันและรวมไปถึงเพื่อเป็นการเตรียมความพร้อมที่จะเข้าสู่ยุดที่มีการใช้เทคโนโลยีแบบเต็มรูปแบบ หรือที่เรารู้จักในนามของ Digital Economy  ปัจจัยใดที่เป็นตัวขับเคลื่อน ปัจจัยใดคือแรงผลักที่สำคัญ แล้วเราจะก้าวไปสู่ยุค Digital Economy  อย่างสมบูรณ์แบบได้หรือไม่">
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1509  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap" >
                            <img class="img-responsive" src="http://img.youtube.com/vi/bDa_-rv4I1Q/mqdefault.jpg" alt="" >
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">การเติบโตของตลาดโทรคมนาคมในภูมิภาคอาเซียน</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="https://www.youtube.com/watch?v=bDa_-rv4I1Q" rel="prettyPhoto"
                                        title="
                                        ทำความเข้าใจกับการเติบโตของตลาดโทรคมนาคมในภูมิภาคอาเซียน และติดตามดูการเตรียมความพร้อมของอินเตอร์ลิ้งค์ เทเลคอม ในการทำธุรกิจในสาธารณรัฐแห่งสหภาพเมียนมาร์ว่าจะมีทิศทางในการดำเนินงานอย่างไร ผ่านการสัมภาษณ์คุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ทาง Nation Channel เมื่อวันอังคารที่ 8 กันยายน 2558 ที่ผ่านมา
                                            "
                                        >
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1601  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap" >
                            <img class="img-responsive" src="http://img.youtube.com/vi/EAQQD209r1k/mqdefault.jpg" alt="" >
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">ภาพรวมการทำธุรกิจของกลุ่มบริษัทอินเตอร์ลิ้งค์</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="https://www.youtube.com/watch?v=EAQQD209r1k" rel="prettyPhoto"
                                        title="
ภาพรวมการทำธุรกิจของกลุ่มบริษัทอินเตอร์ลิ้งค์ ออกอากาศทางรายการ เสือสมาร์ท On TV โดยคุณณัฐนัย อนันตรัมพร กรรมการผู้จัดการบริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้กล่าวถึงภาพรวมในการทำธุรกิจของกลุ่มบริษัทอินเตอร์ลิ้งค์ รวมไปถึงกลยุทธ์ในการทำธุรกิจ, แผนการดำเนินงานของบริษัท และแผนการเติบโตของกลุ่มบริษัทอินเตอร์ลิ้งค์กับการคว้าโอกาสของเศรษฐกิจดิจิตอล ว่าจะมีทิศทางในการดำเนินงานอย่างไร"
                                        >
                                        <i class="fa fa-eye"></i> View More</a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

  <!-- ข่าวเดือนตุลา       -->                        
  <div class="portfolio-item 1610  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/161001/161001300200.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#">
    <span class="lead"><h5>อินเตอร์ลิ้งค์ เทเลคอมฯ รับโล่เกียรติยศ"บุคคลตัวอย่างภาคธุรกิจแห่งปี 2016"ต่อเนื่องปีที่สองติดต่อกัน</h5></span></a>
                                    </h3>
                                    <p>
                                      <a class="preview" href="portfolio/161001/161001.jpg" rel="prettyPhoto[1610]"
                                        title=" ฯพณฯ พลเอกพิจิตร กุลละวณิชย์ (ขวา) (องคมนตรี) และ ประธานในพิธีประกาศเกียรติคุณ มอบโล่เกียรติยศ “บุคคลตัวอย่างภาคธุรกิจแห่งปี 2016” ในภาคธุรกิจเทคโนโลยีสารสนเทศและการสื่อสาร แก่นายณัฐนัย อนันตรัมพร(ซ้าย) กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จํากัด(มหาชน) หรือ ITEL โดยบริษัทฯได้รับรางวัลดังกล่าวเป็นปีที่สองติดต่อกัน นับเป็นความภาคภูมิใจในฐานะผู้บริหารในการประยุกต์ใช้วิทยาศาสตร์และเทคโนโลยีในการทํางาน และทําประโยชน์เพื่อสังคมอย่างต่อเนื่อง งานดังกล่าวจัดขึ้นโดยมูลนิธิสภาวิทยาศาสตร์และเทคโนโลยีแห่งประเทศไทย (มสวท.) มีวัตถุประสงค์จัดงาน เพื่อถวายเป็นพระราชกุศลแด่องค์พระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดชในฐานะพระบิดาแห่งเทคโนโลยีไทย ณ ศูนย์ประชุมสถาบันวิจัยจุฬาภรณ์ ถวิภาวดีรังสิต เมื่อเร็วๆนี้ 


                                        " >
                                        <i class="fa fa-eye"></i> View More</a>
                                       
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->
<!-- 
ข่าวเดือนตุลาคนครั้งที่สอง -->

 <div class="portfolio-item 1610  col-xs-12 col-sm-4 col-md-4">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/161002/161002-7.jpg" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#">
    <span class="lead"><h5> กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ</h5></span></a>
                                    </h3>
                                    <p>
                                      <a class="preview" href="portfolio/161002/7.jpg" rel="prettyPhoto[1610-1]"
                                        title=" นายสมบัติ อนันตรัมพร ประธานกรรมการและกรรมการผู้จัดการใหญ่ กลุ่มบริษัทอินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จํากัด(มหาชน) นางชลิดา อนันตรัมพร กรรมการผู้จัดการ และประธานมูลนิธิอินเตอร์ลิ้งค์ให้ใจและนายณัฐนัย อนันตรัมพร  กรรมการผู้จัดการ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จํากัด(มหาชน) นําคณะผู้บริหารและพนักงานกลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพต่อหน้าพระบรมฉายาลักษณ์พระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช มหิตลาธิเบศรรามาธิบดี จักรีนฤบดินทร สยามินทราธิราช เพื่อแสดงออกถึงความจงรักภักดี จากนั้นนายสมบัติ อนันตรัมพร นางชลิดา อนันตรัมพรและนายณัฐนัย อนันตรัมพร พร้อมทั้งพนักงานร่วมยืนไว้อาลัยเป็นเวลา 9 นาที เพื่อแสดงความน้อมรําลึกในพระมหากรุณาธิคุณหาที่สุดมิได้

                                        " >
                                        <i class="fa fa-eye"></i> View More</a>
                                         <a class="preview" href="portfolio/161002/1.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                         <a class="preview" href="portfolio/161002/2.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>

                                         <a class="preview" href="portfolio/161002/3.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                           <a class="preview" href="portfolio/161002/4.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                            <a class="preview" href="portfolio/161002/5.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                             <a class="preview" href="portfolio/161002/6.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                              <a class="preview" href="portfolio/161002/8.jpg" rel="prettyPhoto[1610-1]" title="กลุ่มบริษัทอินเตอร์ลิ้งค์ฯ ร่วมถวายความเคารพ"></a>
                                       
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->


                    </div></div>

                <aside class="col-md-2">
                    <div class="widget archieve">
                        <h3><center>ARCHIEVE</center></h3>
                        
                        <div class="row">
                            <div class="col-sm-10">

                    <ul class="portfolio-filter ">
                
<li><div><a class="btn btn-default" data-toggle="collapse" href="#collapse2">ปี 2559</a></div></li>
     <div id="collapse2" class="panel-collapse collapse">
            <ul class="portfolio-filter text-center">
                    <li><a class="btn btn-default" href="#" data-filter=".1601">มกราคม 2559</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1602">กุมภาพันธ์ 2559</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1603">มีนาคม 2559</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1604">เมษายน 2559</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1605">พฤษภาคม 2559</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1606">มิถุนายน 2559</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1607">กรกฎาคม 2559</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1608">สิงหาคม 2559</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1609">กันยายน 2559</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1610">ตุลาคม 2559</a></li>
         
            </ul><!--/#portfolio-filter-->
        </div><!--/#collapse2-->




                <li><div><a class="btn btn-default" data-toggle="collapse" href="#collapse1">ปี 2558</a></div></li>

                 <br>
              
        <div id="collapse1" class="panel-collapse collapse">
            <ul class="portfolio-filter text-center">
                <li><a class="btn btn-default" href="#" data-filter=".1512">ธันวาคม 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1511">พฤศจิกายน 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1510">ตุลาคม 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1509">กันยายน 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1508">สิงหาคม 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1507">กรกฎาคม 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1506">มิถุนายน 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1505">พฤษภาคม 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1504">เมษายน 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1503">มีนาคม 2558</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1501">มกราคม 2558</a></li>
         
            </ul><!--/#portfolio-filter-->  


             
        </div><!--/#collapse1-->

<li><div><a class="btn btn-default" data-toggle="collapse" href="#collapse">ปี 2557</a></div></li>
     <div id="collapse" class="panel-collapse collapse">
            <ul class="portfolio-filter text-center">
               <li><a class="btn btn-default" href="#" data-filter=".1412">ธันวาคม 2557</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1411">พฤศจิกายน 2557</a></li>
         
            </ul><!--/#portfolio-filter-->
        </div><!--/#collapse-->
 </ul><!--/#portfolio-filter-->


          </div>
                        </div>                     
                    </div><!--/.archieve-->

                </div>
            </div>
        </div>
    </section><!--/#portfolio-item-->
                    

<!-- include header.php -->
        <?php
        $path = $_SERVER['DOCUMENT_ROOT'];
        $path .= "/th/php/footer.php";
        include_once($path) ;
        ?>
<!--/end  php -->


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
