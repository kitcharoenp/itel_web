<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>What We Do | InterlinkTelecom</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
	<link href="css/item_hover.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="fonts/stylesheet.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
     <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
 
</head><!--/head-->
<body>
    <!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->
    
    <section id="content" class="shortcode-item">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <h2>What We Do ?</h2> 
                    <div class="tab-wrap">
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Interlink Domestic MPLS IP-VPN</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">Interlink  Domestic Wavelength</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Interlink Domestic Dark Fiber</a></li>
                                    <li class=""><a href="#tab4" data-toggle="tab" class="tehnical">Interlink International IPLC</a></li>
                                    <li class=""><a href="#tab5" data-toggle="tab" class="tehnical">Interlink International Wavelength</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">
                                    <div class="tab-pane active in" id="tab1">
                                                 <h4><span class="orangetext">Interlink Domestic MPLS IP-VPN</span></h4>
													<p>Interlink MPLS IP-VPN คือ บริการเชื่อมต่อข้อมูลระหว่างพื้นที่การใช้งานของผู้ใช้บริการผ่านโครงข่ายเคเบิ้ลใยแก้วนำแสง 
Interlink Fiber Optic Network ด้วยเทคโนโลยี MPLS IP-VPN ซึ่งสามารถทำการจัดลำดับความสำคัญของข้อมูล แต่ละ
รูปแบบได้ ไม่ว่าจะเป็น ข้อมูลภาพ เสียงหรือข้อมูลทั่วไป ซึ่งทำให้การใช้งานในแต่ละพื้นที่ใช้บริการของลูกค้า สามารถดำเนินการ
ให้เหมาะสมกับลูกค้าแต่ละรายและแต่ละพื้นที่ โดยบริการนี้สามารถที่จะรองรับการใช้งานเชื่อมต่อข้อมูลที่มีปริมาณมากๆ ตั้งแต่ 
1Mbps – 10Gbps หรือมากกว่า ตามแต่ความต้องการของลูกค้า โดยยังมีโครงข่ายสำรองที่ทำหน้าที่ทดแทนในกรณีโครงข่ายหลัก
เกิดมีปัญหาโดยอัตโนมัติ และสามารถเปลี่ยนไปใช้โครงข่ายสำรองดังกล่าวได้ในระยะเวลาอันสั้นตามมาตราฐานสากล</p>
<p>บริการ Interlink MPLS IP-VPN เหมาะกับลูกค้าทุกประเภท ทั้งลูกค้าที่มีออฟฟิศ สาขา ในกรุงเทพและพื้นที่ทั่วประเทศ และ
ทุกกลุ่มอุตสาหกรรมไม่ว่าจะเป็น อุตสาหกรรมการเงินการธนาคาร อุตสาหกรรมเทคโนโลยีสารสนเทศ อุตสาหกรรมโทรคมนาคม 
อุตสาหกรรมทีวีดาวเทียมและดิจิตอลทีวี หน่วยงานภาครัฐและหน่วยงานเอกชนทุกประเภท รวมถึงบริษัท ข้ามชาติ ต่างๆ อีกด้วย</p>
												<span class="area"></span><p class="subtitle">พื้นที่ให้บริการ</p>
													<p>บริการ Interlink MPLS IP-VPN สามารถให้บริการ ได้ในพื้นที่ กรุงเทพมหานคร และทั่วประเทศ รวมถึงอาคารสำนักงาน -
ต่างๆ และนิคมอุตสาหกรรมต่างๆ</p>
												<span class="strongpoint"></span><p class="subtitle">จุดแข็งของบริการ</p>
												<ol>
													<li>สะดวกในการใช้งาน ผู้ใช้งานสามารถเลือกใช้งานได้ตั้งแต่ 1Mbps – 10Gbps</li>
													<li>สามารถรองรับการส่งข้อมูลได้ทุกรูปแบบ ทั้ง ข้อมูลภาพ เสียงและข้อมูลทั่วไป</li>
													<li>สามารถจัดลำดับความสำคัญของข้อมูลแต่ละรูปแบบ Prioritization บนบริการจัดลำดับความสำคัญ (Class of Service)</li>
													<li>มีโครงข่ายหลัก และสำรองพร้อมที่จะเปลี่ยนไปใช้ในกรณีมีเหตุเสียแบบอัตโนมัติ</li>
													<li>พื้นที่ให้บริการ ทั่วทั้งกรุงเทพ และทั่วประเทศ รวมไปถึงอาคารสำนักงานและนิคมอุตสาหกรรม</li>
												</ol>
                                     </div><!--/.tab-pane--> 

                                     <div class="tab-pane" id="tab2">
                                                 <h4><span class="orangetext">Interlink Domestic Wavelength</span></h4>
                                                 <p>Interlink Wavelength คือ บริการเชื่อมต่อข้อมูลระหว่างพื้นท่ี
การใช้งานของผู้ใช้บริการผ่านโครงข่ายเคเบิ้ลใยแก้วนำแสง 
Interlink Fiber Optic Network ด้วยเทคโนโลยี DWDM 
ซึ่งสามารถรวมข้อมูลขนาดเล็กๆเป็นข้อมูลขนาดใหญ่และนำส่งในแต่ละครั้ง 
โดยสามารถรองรับทั้งข้อมูลภาพ เสียงหรือข้อมูลทั่วไป โดยบริการนี้จะให้บริการในลักษณะการให้บริการเป็น แลมด้า (Lamda) 
ซึ่งมีตั้งแต่2.5Gbps/Lamda และ 10Gbps/Lamda หรือมากกว่า ตามแต่ความต้องการของลูกค้า  
ทั้งนี้บริษัทฯ ดำเนินการดูแลการให้บริการในลักษณะ Turnkey ทั้ง อุปกรณ์และการใช้งานของลูกค้าจึงทำให้ผู้ใช้งานได้รับความสะดวกสบายไรกังวลอีกด้วย</p>
												<p>บริการ Interlink Wavelength เหมาะกับลูกค้าที่มีการใช้งานเชื่อมต่อ ระหว่างศูนย์ข้อมูลหลัก 
และศูนย์ข้อมูลสำรอง ซึ่งจะมีปริมาณข้อมูลเป็นจำนวนมาก ไม่ว่าจะเป็น 
อุตสาหกรรมการเงินการธนาคาร อุตสาหกรรมเทคโนโลยีสารสนเทศ อุตสาหกรรมโทรคมนาคม 
หน่วยงานภาครัฐและหน่วยงานเอกชนทุกประเภท รวมถึงบริษัท ข้ามชาติ ต่างๆ อีกด้วย</p>
												<span class="area"></span><p class="subtitle">พื้นที่ให้บริการ</p>
													<p>บริการ Interlink Wavelength สามารถให้บริการ ได้ในพื้นที่ กรุงเทพมหานคร และทั่วประเทศ รวมถึงอาคารสำนักงานต่างๆ และนิคมอุตสาหกรรมต่างๆ</p>

												<span class="strongpoint"></span><p class="subtitle">Service Strengths</p>
													<ol>
														<li>สะดวกในการใช้งาน ผู้ใช้งานสามารถเลือกใช้งานได้ตั้งแต่ 2.5Gbps และ 10Gbps หรือมากกว่าโดยไม่ต้องลงทุนเพิ่ม</li>
														<li>ต้นทุนของการใช้งานข้อมูลแต่ละ Mbps ประหยัดมากกว่าปกติ</li>
														<li>สามารถรองรับการส่งข้อมูลได้ทุกรูปแบบ ทั้ง ข้อมูลภาพ เสียงและข้อมูลทั่วไป</li>
														<li>พื้นที่ให้บริการ ทั่วทั้งกรุงเทพ และทั่วประเทศ รวมไปถึงอาคารสำนักงานและนิคมอุตสาหกรรม</li>
													</ol>
                                     </div><!--/.tab-pane--> 

                                     <div class="tab-pane" id="tab3">
										 <h4><span class="orangetext">Interlink Domestic Dark Fiber</span></h4>
                                        <p>Interlink Dark Fiber คือบริการให้เช่าโครงข่ายเคเบิ้ลใยแก้วนำแสงของบริษัทฯ Interlink Fiber Optic Network 
แก่ลูกค้าที่มีการใช้งานเชื่อมต่อข้อมูลขนาดใหญ่ มีอุปกรณ์ต้นทาง – ปลายทาง 
และทีมงานในการบริหารจัดการ และต้องการควบคุมการรับ- ส่งข้อมูล โดยทีมงานที่เป็นของตนเอง 
โดยบริษัทฯ จะรับผิดชอบในการดูแลโครงข่ายเคเบิ้ลใยแก้วนำแสง ให้สามารถ ใช้งานได้อยู่ตลอดเวลา 
หรือตามแต่กำหนดภายใต้สัญญาการให้บริการ ทั้งนี้ผู้ใช้บริการสามารถเลือกชนิด 
และเทคโนโลยีที่จะนำมาเชื่อมต่อได้อย่างอิสระ และไม่มีขอบเขตจำกัด</p>
<p>บริการ Interlink Dark Fiber เหมาะกับลูกค้าที่มีการใช้งานเชื่อมต่อ ระหว่างศูนย์ข้อมูลหลัก 
และศูนย์ข้อมูลสำรอง ซึ่งจะมีปริมาณข้อมูลเป็นจำนวนมาก ไม่ว่าจะเป็น 
อุตสาหกรรมการเงินการธนาคาร อุตสาหกรรมเทคโนโลยีสารสนเทศ อุตสาหกรรมโทรคมนาคม
 หน่วยงานภาครัฐและหน่วยงานเอกชนทุกประเภท รวมถึงบริษัท ข้ามชาติ ต่างๆ</p>
										<span class="area"></span><p class="subtitle">พื้นที่ให้บริการ</p>
											<p>บริการ Interlink Dark Fiber สามารถให้บริการ ได้ในพื้นที่ กรุงเทพมหานคร และทั่วประเทศ</p>
										<span class="strongpoint"></span><p class="subtitle">จุดแข็งของบริการ</p>
											<ol>
												<li>ผู้ใช้บริการสามารถเลือกเทคโนโลยีและประสิทธิภาพการส่งข้อมูลได้อย่างไม่จำกัด</li>
												<li>ต้นทุนของการใช้งานข้อมูลแต่ละ Mbps ประหยัดมากกว่าปกติ</li>
												<li>สามารถรองรับการส่งข้อมูลได้ทุกรูปแบบ ทั้ง ข้อมูลภาพ เสียงและข้อมูลทั่วไป ขึ้นอยู่กับเทคโนโลยีที่ผู้ใช้บริการเลือกใช้</li>
												<li>ผู้ใช้บริการสามารถบริหารจัดการการส่งข้อมูลได้เอง</li>
												<li>พื้นที่ให้บริการ ทั่วทั้งกรุงเทพ และทั่วประเทศ</li>
											</ol>
                                     </div><!--/.tab-pane--> 
                                     
                                     <div class="tab-pane" id="tab4">
										<h4><span class="orangetext">Interlink International IPLC</span></h4>
										Interlink IPLC คือ บริการเชื่อมต่อข้อมูลระหว่างพื้นที่การใช้งานของผู้ใช้บริการผ่านโครงข่ายเคเบิ้ลใยแก้วนำแสง 
Interlink Fiber Optic Network ที่เชื่อมต่อกับโครงข่ายของผู้ให้บริการในต่างประเทศ 
ไปยังจุดหมายปลายทางที่อยู่ในต่างประเทศทั่วโลก ด้วยเทคโนโลยี DWDM ซึ่งสามารถรองรับข้อมูล ในทุกรูปแบบได้ 
ไม่ว่าจะเป็น ข้อมูลภาพ เสียงหรือข้อมูลทั่วไป โดยผู้ใช้บริการสามารถเลือกใช้ความเร็วได้ตามความต้องการตั้งแต่ 1Mbps – 10Gbps (STM-64)</p>
<p>บริการ Interlink IPLC เหมาะกับลูกค้าทุกประเภท ที่มีความต้องการใช้บริการเชื่อมต่อข้อมูลระหว่างประเทศทั่วโลก ทั้งที่มีออฟฟิศ สาขา 
ในกรุงเทพและพื้นที่ทั่วประเทศ หรือในต่างประเทศแต่จำเป็นต้องเชื่อมผ่านประเทศไทย 
และทุกกลุ่มอุตสาหกรรมไม่ว่าจะเป็น อุตสาหกรรมการเงินการธนาคาร อุตสาหกรรมเทคโนโลยีสารสนเทศ 
อุตสาหกรรมโทรคมนาคม อุตสาหกรรมทีวีดาวเทียมและดิจิตอลทีวี หน่วยงานภาครัฐและหน่วยงานเอกชนทุกประเภท โดยเฉพาะบริษัท ข้ามชาติ ต่างๆ</p>
										<span class="area"></span><p class="subtitle">พื้นที่ให้บริการ</p>
											<p>บริการ Interlink IPLC สามารถให้บริการ ไปยังหลายๆ ประเทศทั่วโลกตามความต้องการของลูกค้า</p>
										<span class="strongpoint"></span><p class="subtitle">จุดแข็งของบริการ</p>
										<ol>
											<li>สะดวกในการใช้งาน ผู้ใช้งานสามารถเลือกใช้งานได้ตั้งแต่ 1Mbps – 10Gbps (STM-64)</li>
											<li>สามารถรองรับการส่งข้อมูลได้ทุกรูปแบบ ทั้ง ข้อมูลภาพ เสียงและข้อมูลทั่วไป</li>
											<li>มีโครงข่ายหลัก และสำรองสามารถเลือกใช้บริการได้ตามความต้องการของลูกค้า</li>
											<li>สามารถเชื่อมต่อให้กับผู้ใช้บริการที่ต้องการเชื่อมต่อจากพื้นที่ให้บริการ ทั่วทั้งกรุงเทพ และทั่วประเทศ ไปยังต่างประเทศ หรือ ต่างประเทศเชื่อมต่อไปยังต่างประเทศโดยผ่านประเทศไทย</li>
										</ol>
                                     </div><!--/.tab-pane--> 

                                     <div class="tab-pane" id="tab5">
										 <h4><span class="orangetext">Interlink International Wavelength</span></h4>
                                        <p>Interlink Wavelength คือ บริการเชื่อมต่อข้อมูลระหว่างพื้นท่ี
การใช้งานของผู้ใช้บริการผ่านโครงข่ายเคเบิ้ลใยแก้วนำแสง 
Interlink Fiber Optic Network ด้วยเทคโนโลยี DWDM 
ซึ่งสามารถรวมข้อมูลขนาดเล็กๆเป็นข้อมูลขนาดใหญ่และนำส่งในแต่ละครั้ง 
โดยสามารถรองรับทั้งข้อมูลภาพ เสียงหรือข้อมูลทั่วไป โดยบริการนี้จะให้บริการในลักษณะการให้บริการเป็น แลมด้า (Lamda) 
ซึ่งมีตั้งแต่2.5Gbps/Lamda และ 10Gbps/Lamda หรือมากกว่า ตามแต่ความต้องการของลูกค้า  
ทั้งนี้บริษัทฯ ดำเนินการดูแลการให้บริการในลักษณะ Turnkey ทั้ง อุปกรณ์และการใช้งานของลูกค้าจึงทำให้ผู้ใช้งานได้รับความสะดวกสบายไรกังวลอีกด้วย</p>
<p>บริการ Interlink Wavelength เหมาะกับลูกค้าที่มีการใช้งานเชื่อมต่อ ระหว่างศูนย์ข้อมูลหลัก 
และศูนย์ข้อมูลสำรอง ซึ่งจะมีปริมาณข้อมูลเป็นจำนวนมาก ไม่ว่าจะเป็น 
อุตสาหกรรมการเงินการธนาคาร อุตสาหกรรมเทคโนโลยีสารสนเทศ อุตสาหกรรมโทรคมนาคม 
หน่วยงานภาครัฐและหน่วยงานเอกชนทุกประเภท รวมถึงบริษัท ข้ามชาติ ต่างๆ อีกด้วย</p>
										<span class="area"></span><p class="subtitle">พื้นที่ให้บริการ</p>
											<p>บริการ Interlink Wavelength สามารถให้บริการ ได้ในพื้นที่ กรุงเทพมหานคร และทั่วประเทศ รวมถึงอาคารสำนักงานต่างๆ และนิคมอุตสาหกรรมต่างๆ</p>
										<span class="strongpoint"></span><p class="subtitle">จุดแข็งของบริการ</p>
											<ol>
												<li>สะดวกในการใช้งาน ผู้ใช้งานสามารถเลือกใช้งานได้ตั้งแต่ 2.5Gbps และ 10Gbps หรือมากกว่าโดยไม่ต้องลงทุนเพิ่ม</li>
												<li>ต้นทุนของการใช้งานข้อมูลแต่ละ Mbps ประหยัดมากกว่าปกติ</li>
												<li>สามารถรองรับการส่งข้อมูลได้ทุกรูปแบบ ทั้ง ข้อมูลภาพ เสียงและข้อมูลทั่วไป</li>
												<li>พื้นที่ให้บริการ ทั่วทั้งกรุงเทพ และทั่วประเทศ รวมไปถึงอาคารสำนักงานและนิคมอุตสาหกรรม</li>
											</ol>
                                     </div><!--/.tab-pane-->  
                                </div> <!--/.tab-content-->  
                                  
                    </div><!--/.tab-wrap-->               
                </div><!--/.col-xs-12-->                   
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#content-->
    





<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/footer.php";
		include_once($path) ; 
		?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
