    var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
    var randomColorFactor = function() {
        return Math.round(Math.random() * 255);
    };
    var randomColor = function(opacity) {
        return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
    };

    var config_ilink = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    "25.62",
                    "14.35",
                    "12.73",
                    "7.81",
                    "1.88",
                    "37.61",
			
                ],
                backgroundColor: [
                    "#F7464A",
                    "#46BFBD",
                    "#FDB45C",
                    "#949FB1",
                    "#4D5360",
                    "#FFCE56",
                ],
                label: 'Dataset 1'
            }, ],
            labels: [
                "บริษัท อินเตอร์ลิ้งค์ โฮลดิ้ง จำกัด",
                "นางชลิดา อนันตรัมพร",
                "นายสมบัติ อนันตรัมพร",
                "นายศักดิ์ชัย ศักดิ์ชัยเจริญกุล",
                "นายสุเทพ พัฒนสิน",
                "อื่นๆ",
            ]
        },
        options: {
            responsive: true,
            legend: {
				display: false,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน)'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

    
    var config_itel = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    "99.99",
                    "0.01",
			
                ],
                backgroundColor: [
                    "#F7464A",
                    "#46BFBD",
                ],
                label: 'Dataset 1'
            }, ],
            labels: [
                "บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน)",
                "อื่นๆ",
            ]
        },
        options: {
            responsive: true,
            legend: {
				display: false,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'บริษัท อินเตอร์ลิ้งค์เทเลคอม จำกัด (มหาชน)'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

    var config_power = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    "99.99",
                    "0.01",
			
                ],
                backgroundColor: [
                    "#46BFBD",
                    "#FDB45C",
                ],
                label: 'POWER'
            }, ],
            labels: [
                "บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน)",
                "อื่นๆ",
            ]
        },
        options: {
            responsive: true,
            legend: {
				display: false,
                position: 'bottom',
            },
            title: {
                display: true,
                text: 'บริษัท อินเตอร์ลิ้งค์ เพาเวอร์ แอนด์ เอ็นเนอร์ยี่ จำกัด',
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };
    
    var config_data = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    "99.99",
                    "0.01",
			
                ],
                backgroundColor: [
                    "#949FB1",
                    "#4D5360",
                ],
                label: 'DATA'
            }, ],
            labels: [
                "บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน)",
                "อื่นๆ",
            ]
        },
        options: {
            responsive: true,
            legend: {
				display: false,
                position: 'bottom',
            },
            title: {
                display: true,
                text: "บริษัท อินเตอร์ลิ้งค์ ดาต้าเซ็นเตอร์ จำกัด",
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

    window.onload = function() {
        var ctx = document.getElementById("chart_ilink").getContext("2d");
        window.myDoughnut = new Chart(ctx, config_ilink);
        
        var ctx2 = document.getElementById("chart_itel").getContext("2d");
        window.myDoughnut = new Chart(ctx2, config_itel);
        
        var ctx3 = document.getElementById("chart_power").getContext("2d");
        window.myDoughnut = new Chart(ctx3, config_power);
        
        var ctx4 = document.getElementById("chart_data").getContext("2d");
        window.myDoughnut = new Chart(ctx4, config_data);
    };

