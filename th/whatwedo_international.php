<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premier Fiber Optical Network">
    <meta name="author" content="">
    <title>What We Do | InterlinkTelecom</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
	<link href="css/item_hover.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="fonts/stylesheet.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
 <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
   
</head><!--/head-->
<body>
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->
    
    <section id="content" class="shortcode-item">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <h2>What We Do ?</h2> 
                    <div class="tab-wrap">
                        <div class="media">
                            <div class="parrent pull-left">
                                <ul class="nav nav-tabs nav-stacked">
<!--/14-12-15									
                                    <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Interlink Domestic MPLS IP-VPN</a></li>
                                    <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">Interlink  Domestic Wavelength</a></li>
                                    <li class=""><a href="#tab3" data-toggle="tab" class="tehnical">Interlink Domestic Dark Fiber</a></li>
-->                                    
                                    <li class="active"><a href="#tab4" data-toggle="tab" class="tehnical">Interlink  IPLC</a></li>
                                    <li class=""><a href="#tab5" data-toggle="tab" class="tehnical">Interlink  Wavelength</a></li>
                                </ul>
                            </div>

                            <div class="parrent media-body">
                                <div class="tab-content">





                                     
                                     <div class="tab-pane active in" id="tab4">
										<h4><span class="orangetext">Interlink  IPLC</span></h4>
										<p align="justify">
										Interlink  IPLC คือ บริการเชื่อมต่อข้อมูลระหว่างพื้นที่การใช้งานของผู้ใช้บริการผ่านโครงข่ายเคเบิ้ลใยแก้วนำแสง 
Interlink Fiber Optic Network ที่เชื่อมต่อกับโครงข่ายของผู้ให้บริการในต่างประเทศ 
ไปยังจุดหมายปลายทางที่อยู่ในต่างประเทศทั่วโลก ด้วยเทคโนโลยี DWDM ซึ่งสามารถรองรับข้อมูล ในทุกรูปแบบได้ 
ไม่ว่าจะเป็น ข้อมูลภาพ เสียงหรือข้อมูลทั่วไป โดยผู้ใช้บริการสามารถเลือกใช้ความเร็วได้ตามความต้องการตั้งแต่ 1Mbps – 10Gbps (STM-64)</p>
<p align="justify">บริการ Interlink IPLC เหมาะกับลูกค้าทุกประเภท ที่มีความต้องการใช้บริการเชื่อมต่อข้อมูลระหว่างประเทศทั่วโลก ทั้งที่มีออฟฟิศ สาขา 
ในกรุงเทพและพื้นที่ทั่วประเทศ หรือในต่างประเทศแต่จำเป็นต้องเชื่อมผ่านประเทศไทย 
และทุกกลุ่มอุตสาหกรรมไม่ว่าจะเป็น อุตสาหกรรมการเงินการธนาคาร อุตสาหกรรมเทคโนโลยีสารสนเทศ 
อุตสาหกรรมโทรคมนาคม อุตสาหกรรมทีวีดาวเทียมและดิจิตอลทีวี หน่วยงานภาครัฐและหน่วยงานเอกชนทุกประเภท โดยเฉพาะบริษัท ข้ามชาติ ต่างๆ</p>
										<span class="area"></span><p class="subtitle">พื้นที่ให้บริการ</p>

											<p style="padding-left:4em">บริการ Interlink IPLC สามารถให้บริการ ไปยังหลายๆ ประเทศทั่วโลกตามความต้องการของลูกค้า</p>
										<span class="strongpoint"></span><p class="subtitle">จุดแข็งของบริการ</p>
										<ol style="padding-left:4em">
											<p>1. สะดวกในการใช้งาน ผู้ใช้งานสามารถเลือกใช้งานได้ตั้งแต่ 1Mbps – 10Gbps (STM-64)</p>
											<p>2. สามารถรองรับการส่งข้อมูลได้ทุกรูปแบบ ทั้ง ข้อมูลภาพ เสียงและข้อมูลทั่วไป</p>
											<p>3. มีโครงข่ายหลัก และสำรองสามารถเลือกใช้บริการได้ตามความต้องการของลูกค้า</p>
											<p>4. สามารถเชื่อมต่อให้กับผู้ใช้บริการที่ต้องการเชื่อมต่อจากพื้นที่ให้บริการ ทั่วทั้งกรุงเทพ และทั่วประเทศ ไปยังต่างประเทศ หรือ ต่างประเทศเชื่อมต่อไปยังต่างประเทศโดยผ่านประเทศไทย</p>
										</ol>
                                     </div><!--/.tab-pane--> 

                                     <div class="tab-pane" id="tab5">
										 <h4><span class="orangetext">Interlink  Wavelength</span></h4>
                                        <p align="justify">Interlink  Wavelength คือ บริการเชื่อมต่อข้อมูลระหว่างพื้นท่ี
การใช้งานของผู้ใช้บริการผ่านโครงข่ายเคเบิ้ลใยแก้วนำแสง ด้วยเทคโนโลยี DWDM ซึ่งสามารถรวมข้อมูลขนาดเล็กๆเป็นข้อมูลขนาดใหญ่และนำส่งในแต่ละครั้ง 
โดยสามารถรองรับทั้งข้อมูลภาพ เสียงหรือข้อมูลทั่วไป โดยบริการนี้จะให้บริการในลักษณะการให้บริการเป็นแลมด้าซึ่งมีตั้งแต่2.5Gbps/Lamda และ 10Gbps/Lamda หรือมากกว่า ตามแต่ความต้องการของลูกค้า  
ทั้งนี้บริษัทฯ ดำเนินการดูแลการให้บริการในลักษณะ Turnkey ทั้ง อุปกรณ์และการใช้งานของลูกค้าจึงทำให้ผู้ใช้งานได้รับความสะดวกสบายไรกังวลอีกด้วย</p>
<p align="justify">บริการ Interlink Wavelength เหมาะกับลูกค้าที่มีการใช้งานเชื่อมต่อ ระหว่างศูนย์ข้อมูลหลัก 
และศูนย์ข้อมูลสำรอง ซึ่งจะมีปริมาณข้อมูลเป็นจำนวนมาก ไม่ว่าจะเป็น 
อุตสาหกรรมการเงินการธนาคาร อุตสาหกรรมเทคโนโลยีสารสนเทศ อุตสาหกรรมโทรคมนาคม 
หน่วยงานภาครัฐและหน่วยงานเอกชนทุกประเภท รวมถึงบริษัท ข้ามชาติ ต่างๆ อีกด้วย</p>
										<span class="area"></span><p class="subtitle">พื้นที่ให้บริการ</p>

											<p style="padding-left:4em">บริการ Interlink Wavelength สามารถให้บริการ ได้ในพื้นที่ กรุงเทพมหานคร และทั่วประเทศ รวมถึงอาคารสำนักงานต่างๆ และนิคมอุตสาหกรรมต่างๆ</p>
										<span class="strongpoint"></span><p class="subtitle">จุดแข็งของบริการ</p>
											<ol style="padding-left:4em">
												<p>1. สะดวกในการใช้งาน ผู้ใช้งานสามารถเลือกใช้งานได้ตั้งแต่ 2.5Gbps และ 10Gbps หรือมากกว่าโดยไม่ต้องลงทุนเพิ่ม</p>
												<p>2. ต้นทุนของการใช้งานข้อมูลแต่ละ Mbps ประหยัดมากกว่าปกติ</p>
												<p>3. สามารถรองรับการส่งข้อมูลได้ทุกรูปแบบ ทั้ง ข้อมูลภาพ เสียงและข้อมูลทั่วไป</p>
												<p>4. พื้นที่ให้บริการ ทั่วทั้งกรุงเทพ และทั่วประเทศ รวมไปถึงอาคารสำนักงานและนิคมอุตสาหกรรม</p>
											</ol>
                                     </div><!--/.tab-pane-->  
                                </div> <!--/.tab-content-->  
                                  
                    </div><!--/.tab-wrap-->               
                </div><!--/.col-xs-12-->                   
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#content-->
    





<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/footer.php";
		include_once($path) ; 
		?>
<!--/end  php -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
