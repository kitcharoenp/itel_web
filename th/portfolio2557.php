﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Interlink Telecom - News & Events </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">

     <!-- Google Analytic Website tracking-->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic-->
</head>
<body id="pageBody">
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/header.php";
		include_once($path) ;
		?>
<!--/end  php -->
<body>
<?php
        
        include("portfoliomenu.php") ;
        ?>

      


            <div class="row">
                <div class="col-md-10">
                <div class="portfolio-items">



					<div class="portfolio-item 1412  col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/141220/300x200.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">อินเตอร์ลิ้งค์ เทเลคอม เปิดบ้านต้อนรับทีมวิศวกรรมจากซีเอส ล็อกซอินโฟ</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/141220/img02.png" rel="prettyPhoto[1412]"
										title="INTERLINK TELECOM เปิดบ้านต้อนรับ
ทีมวิศวกรรมจาก CS Loxinfo
ทางบริษัท อินเตอร์ลิ้งค์  เทเลคอม จำกัด  นำโดยคุณณัฐนัย อนันตรัมพร
MD Managing Director พร้อมทีมวิศวกรรม ได้เปิดบ้านต้อนรับบริษัท
ซีเอส ล็อกซอินโฟ จำกัด (มหาชน) ซึ่งนำทีมโดยคุณสปันนา คงธนาฤทธิ์
Assistant Director Corporate Network Support
เพื่อทำการแลกเปลี่ยนและสร้างแนวทางในการทำงานร่วมกันอันจะเพิ่มประสิทธิภาพการให้บริการของทั้ง 2 บริษัท
 โดยคำนึงถึงผลประโยชน์สูงสุดของลูกค้าผู้ใช้บริการทุกราย
 ในวันที่ 20 พฤศจิกายนที่ผ่านมา ณ อาคารอินเตอร์ลิ้งค์  ถ.รัชดาภิเษก">
										<i class="fa fa-eye"></i> View More</a>
									<a class="preview" href="portfolio/141220/img01.jpg" rel="prettyPhoto[1412]" title="อินเตอร์ลิ้งค์ เทเลคอม เปิดบ้านต้อนรับทีมวิศวกรรมจากซีเอส ล็อกซอินโฟ"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->

                    <div class="portfolio-item 1412  col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/141227/300x200.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#"><span class="lead">Interlink Data Center ออกอากาศทางรายการ Hitech Spring</span></a></h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/141227/img01.jpg" rel="prettyPhoto[1412]"
										title="พิธีกรไอทีอารมณ์ดี คุณหนุ่ย พงศ์สุข หิรัญพฤกษ์  พร้อมทีมงานให้ความสนใจในการบริการของ
                    Interlink Data Center  เพื่อถ่ายทำรายการ Hitech Spring   ซึ่งออกอากาศไปเมื่อวันเสาร์ที่
                    27 ธันวาคม 2557  ทางช่อง Spring News  โดยในรายการคุณใหม่  ภัททิยาพรรณ์  โกศลสุวรรณ
                     Sales Executive คนสวยของเราได้พาคุณหนุ่ยเข้าชม Interlink Data Center ที่มีความปลอด
						ภัยสูงและใช้เทคโนโลยีที่ทันสมัยระดับโลก เพื่อรองรับทุกความต้องการของลูกค้า">
										<i class="fa fa-eye"></i> View More</a>
									<a class="preview" href="portfolio/141227/img02.jpg" rel="prettyPhoto[1412]" title="Interlink Data Center ออกอากาศทางรายการ Hitech Spring"></a>
									<a class="preview" href="https://www.youtube.com/watch?v=LVl44cDxT54" rel="prettyPhoto[1412]"  title="Interlink Data Center ออกอากาศทางรายการ Hitech Spring"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item-->




                  

                    <div class="portfolio-item 1411  col-xs-12 col-sm-4 col-md-3">
                        <div class="recent-work-wrap">
                            <img class="img-responsive" src="portfolio/141113/300x200.png" alt="">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3><a href="#">
										<span class="lead">Chic & Chill Exclusive Party with Inet</span></a>
									</h3>
                                    <p></p>
                                    <a class="preview" href="portfolio/141113/img01.jpg" rel="prettyPhoto[1411]"
										title="Chic & Chill Exclusive Party with Inet
เมื่อวันที่ 13 พฤศจิกายน ที่ผ่านมา บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด
ได้จัดงานเลี้ยงขอบคุณลูกค้า บริษัท อินเทอร์เน็ตประเทศไทย จำกัด (มหาชน) (INET)
ภายใต้ชื่องาน “Chic & Chill Exclusive Party” โดยมีคุณณัฐนัย อนันตรัมพร
กรรมการผู้จัดการ กลุ่มบริษัท อินเตอร์ลิ้งค์ฯ เป็นประธานกล่าวเปิดงาน
ภายในงานมีการจัดกิจกรรมเล่นเกมส์ให้ร่วมสนุกมากมาย พร้อมทั้งแจก
iPhone 6 และ Mini Concert จากวง PlayGround
ที่มาร่วมสร้างสีสันและความบันเทิง ณ ร้าน Spring & Summer สุขุมวิท 49" >
										<i class="fa fa-eye"></i> View More</a>
										<a class="preview" href="portfolio/141113/img02.jpg" rel="prettyPhoto[1411]" title="Chic & Chill Exclusive Party with Inet" ></a>
										<a class="preview" href="portfolio/141113/img03.jpg" rel="prettyPhoto[1411]" title="Chic & Chill Exclusive Party with Inet"></a>
										<a class="preview" href="portfolio/141113/img04.png" rel="prettyPhoto[1411]" title="Chic & Chill Exclusive Party with Inet"></a>
                                </div>
                            </div>
                        </div>
                    </div><!--/.portfolio-item--></div></div>

                <aside class="col-md-2">
                    <div class="widget archieve">
                        <h3>News & Events</h3>
                        <div class="row">
                            <div class="col-sm-10">

                    <ul class="portfolio-filter ">
                <li><a class="btn btn-default active" href="#" data-filter="*">ทั้งหมด</a></li>
                <li><div><a class="btn btn-default" data-toggle="collapse" href="#collapse1">ปี 2557</a></div></li>
         

        <div id="collapse1" class="panel-collapse collapse">
            <ul class="portfolio-filter text-center">
               <li><a class="btn btn-default" href="#" data-filter=".1412">ธันวาคม 2557</a></li>
                <li><a class="btn btn-default" href="#" data-filter=".1411">พฤศจิกายน 2557</a></li>
            <a  href="portfolio2557.php"><li class="btn btn-default">ปี 2557</li></a></li>
            </ul><!--/#portfolio-filter-->
        </div><!--/#collapse1-->
          </div></ul><!--/#portfolio-filter-->
                        </div>                     
                    </div><!--/.archieve-->

                </div>
            </div>
        </div>
    </section><!--/#portfolio-item-->
              
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/footer.php";
		include_once($path) ;
		?>
<!--/end  php -->


    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
