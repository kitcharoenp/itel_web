<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premier Fiber Optical Network">
    <meta name="author" content="">
    <title>ISMS | InterlinkTelecom</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
     <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
 
</head><!--/head-->

<body>
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->

    <section id="isms">
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>ISMS Policy</h2>
                <p class="lead" align="justify">บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด ได้ดำเนินการรักษาความปลอดภัยของข้อมูล 
ตามมาตรฐานการจัดการด้านความปลอดภัยของข้อมูล (ISO/IEC 27001) เพื่อรักษาไว้ซึ่ง ความลับ  
ความถูกต้อง และ การใช้งานได้ ของข้อมูล  จึงขอกำหนดนโยบายการรักษาความปลอดภัยของข้อมูล ดังนี้</p>
            </div>
<div class="row">
                <div class="features">
                    <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-umbrella"></i>                          
                            <h3>1. สนับสนุนให้พนักงานและหัวหน้างาน ดำเนินการจัดการความปลอดภัยของข้อมูลอย่างมีประสิทธิภาพ</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-sitemap"></i>                     
                            <h3>2. กำหนดให้หัวหน้างานจะต้องสนับสนุน และให้สิทธิ์กับพนักงานที่จะนำการจัดการความปลอดภัยของข้อมูลมาใช้ในพื้นที่ปฏิบัติงาน </h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-shield"></i>           
                            <h3>3. ให้ดำเนินการสื่อสาร นโยบายหรือกรอบในการกำหนดวัตถุประสงค์ความปลอดภัยข้อมูล  (ISMS Framework)ไปยังพนักงานทุกคนในองค์กร</h3>
                        </div>
                    </div><!--/.col-md-4-->

                    <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-calendar-o"></i>
                            <h3>4. วางแผนกำลังการให้บริการ เป็นประจำทุกๆปีเพื่อสนับสนุนการขยายตัวทางธุรกิจ </h3>
                        </div>
                    </div><!--/.col-md-4-->
                
                    <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-bar-chart-o"></i>
                            <h3>5. วัดการจัดการความปลอดภัยของข้อมูล เพื่อให้แน่ใจว่านำมาปฏิบัติได้อย่างมีประสิทธิผล</h3>
                        </div>
                    </div><!--/.col-md-4-->



                    <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-heart"></i>
                            <h3>6. ความปลอดภัยของข้อมูลถือเป็นส่วนหนึ่งในกลยุทธของบริษัทฯ เพื่อเพิ่มส่วนแบ่งการตลาด</h3>
                        </div>
                    </div><!--/.col-md-4-->
 
                     <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-rocket"></i>
                            <h3>7. การพัฒนาอย่างต่อเนื่อง เป็นส่วนหนึ่งในเป้าหมายขององค์กร</h3>
                        </div>
                    </div><!--/.col-md-4-->
  
                      <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-ambulance"></i>
                            <h3>8. ให้มีการทบทวนโดยฝ่ายบริหาร (Management Review) เป็นประจำทุกๆปี  เพื่อการปรับปรุงคุณภาพการให้บริการ</h3>
                        </div>
                    </div><!--/.col-md-4-->
  
                      <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-ticket"></i>
                            <h3>9. ความต้องการของลูกค้า ความต้องการของผู้มีส่วนได้เสีย (Interested parties) 
ข้อกำหนดทางกฎหมาย ข้อบังคับ หรือข้อผูกพันตามสัญญา ต้องนำมาปฏิบัติ หากไม่ขัดกับธุรกิจและทางเทคนิค</h3>
                        </div>
                    </div><!--/.col-md-4-->
  
                      <div class="col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                        <div class="feature-wrap">
                            <i class="fa fa-medkit"></i>
                            <h3>10. ให้มีการ  บำรุงรักษาระบบ (Facilities) ที่ให้บริการในศูนย์รับฝากข้อมูลอยู่เป็นประจำ และปรังปรุงคุณภาพอย่างต่อเนื่องเพื่อบริการที่ดี</h3>
                        </div>
                    </div><!--/.col-md-4-->
                       
                </div><!--/.services-->
            </div><!--/.row--> 			
						
			</div><!--section-->
		</div><!--/.container-->
    </section><!--/about-us-->
	
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/footer.php";
		include_once($path) ; 
		?>
<!--/end  php -->
 
    <script src="js/jquery.js"></script>
    <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
