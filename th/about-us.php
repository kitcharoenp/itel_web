<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="The New ICT Highway">
    <meta name="author" content="">
    <title>เกี่ยวกับเรา | อินเตอร์ลิงค์เทเลคอม</title>
	
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    
     <!-- Google Analytic Website tracking-->   
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-57997984-1', 'auto');
		ga('send', 'pageview');

	</script>
 <!--/Google Analytic--> 
 
</head><!--/head-->

<body>
<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/header.php";
		include_once($path) ; 
		?>
<!--/end  php -->

<!-- mody 11-12-2014 -->   
	<section id="aboutus">
        <div class="container">	
			<div class="row">
			   
				<div class="col-xs-12 col-sm-6 wow fadeInDown">
					<h2>เกี่ยวกับเรา</h2>
					<p class="lead" align="justify">บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด (มหาชน) ได้ถูกก่อตั้งในปี 2550 โดยเป็นบริษัทลูกในเครือของ บริษัท อินเตอร์ลิ้งค์ คอมมิวนิเคชั่น จำกัด (มหาชน) ผู้นำด้านสายสัญญาณของประเทศไทย  บริษัทฯ ได้มีประวัติผลงานทั้งการติดตั้งและการให้บริการวงจร Fiber Optic ให้กับองค์กรชั้นนำต่างๆ เช่น TOT, CAT, PEA และ MEA รวมถึงได้รับอนุญาตให้เป็นผู้ประกอบการธุรกิจโครงข่ายสื่อสารประเภท 3 จาก กรมการสื่อสารโทรคมนาคมแห่งประเทศไทย ทั้งในและนอกประเทศ นอกจากนี้ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด ยังให้บริการด้วยจุดมุ่งหมายของการเติมเต็มความต้องการของลูกค้าด้วยวิสัยทัศน์คือ ดีที่สุดของการเชื่อมต่อ ดีที่สุดของการบริการ และดีที่สุดในด้านราคา</p>
				</div><!--/.col-xs-12 -->
            				<!--
				<div class="col-xs-12 col-sm-6 wow fadeInDown"
					onclick="thevid=document.getElementById('thevideo'); 
					thevid.style.display='block'; this.style.display='none'">
					<img style="cursor: pointer; " src="images/client3.png" alt="" align="right" />
				</div>
				
				<div id="thevideo" style="display: none;">-->
					<div class="col-xs-12 col-sm-6 wow fadeInDown" >
						<h2><br></h2>
						
						<iframe width="400" height="269" align="right" 
						src="//www.youtube.com/embed/2fizuamYads?rel=0&autohide=1&showinfo=0"" frameborder=0
allowfullscreen="true" ></iframe>
					</div><!--/.col-xs-12 col-sm-4-->

				<div class="col-xs-12 col-sm-12 wow fadeInDown">
					<h2>Our Fiber Optical Network</h2>
					<p class="lead" align="justify">
						ปัจจุบัน บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด  (มหาชน) เป็นเจ้าของการให้บริการการสื่อสารทั่วไทยผ่านโครงข่าย Fiber Optic 
						ระหว่างสาย Fiber Optic หุ้มเกราะ และ Switching Technology ด้วยอุปกรณ์ IP network equipment ที่ทันสมัยที่สุด 
						นอกจากนี้การให้บริการยังรวมถึงการใช้งานโครงข่าย Fiber Optic ขั้นสูง, MPLS, SDH และ DWDM 
						ซึ่งได้มีการเดินสายโครงข่ายบนเส้นทางของการรถไฟแห่งประเทศไทย ซึ่งทำให้ลูกค้าของเราสามารถใช้บริการที่เหนือกว่าได้อีกด้วย 
						เช่น Redundancy, Ring Topology access, Corporate data transmission solutions, International Private Leased 
						Circuit Services, Internet Protocol Transit services, MPLS-based IP-VPN service ด้วยการจัดการและดูแลอย่างเต็มประสิทธิภาพ</p>
					<p class="lead" align="justify">
						นอกจากนี้ บริษัท อินเตอร์ลิ้งค์ เทเลคอม จำกัด  (มหาชน) ยังให้บริการบนโครงข่ายของตัวเอง 100%
						 ซึ่งทำให้สามารถรับรองการบริการและการแก้ไขปัญหาในระยะเวลาอันสั้น เพื่อให้ทุกการเชื่อมต่อของลูกค้าไม่สะดุด 
						 และเพื่อเติมเต็มความพึงพอใจของลูกค้า ดังที่เคยกล่าวไว้ในปฎิญญาการให้บริการ</p>			
				</div><!--/.col-xs-12 -->	
							
         </div><!--/.row-->   
		</div><!--/.container-->
	</section><!--/#aboutus-->	

<div class="container">	       
 <div class="row">
                <div class="features">
					
                <div class="col-sm-12" align="middle">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-center">
                            <img class="img-responsive" src="images/aboutus/qualityNetwork.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Quality Network Provider</span></h3>
                            <p align="center">อินเตอร์ลิ้งค์ เทเลคอม ได้รับอนุญาติให้เป็นผู้ประกอบการธุรกิจโครงข่ายสื่อสารประเภท 3</p>
                        </div>
					</div>
                </div><!--/.col-sm-12-->

                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon3.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Technologies</span></h3>
                            <p align="justify">อินเตอร์ลิ้งค์ เทเลคอม ให้บริการด้วยเทคโนโลยีล้ำสมัย เช่น MPLS, DWDM</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->

                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/lastMile.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Last Mile</span></h3>
                            <p align="justify">อินเตอร์ลิ้งค์ เทเลคอม มีการให้บริการ Fiber Optic Last  Mile สำหรับลูกค้าทุกๆประเภทของการใช้งาน</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->
                
                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon2.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Fiber Optic Routing</span></h3>
                            <p align="justify">อินเตอร์ลิ้งค์ เทเลคอม ให้บริการบนโครงข่าย Interlink Fiber Optic Network ที่มีพื้นที่ให้บริการครอบคลุมทั่วประเทศไทย บนเส้นทางของการรถไฟแห่งประเทศไทย</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->

                 <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon6.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Support</span></h3>
                            <p align="justify">อินเตอร์ลิ้งค์ เทเลคอม ให้การดูแลโครงข่ายต่อเนื่องด้วยทีม OMCs ตลอด 24 ชั่วโมง</p>
                            <p><br></p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->
                
                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon7.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best SLA</span></h3>
                            <p align="justify">Interlink support customer under the service level agreement to recover network failure within or less than 4 hours standard nationwide to guarantee best customer experience.</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->

                <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon1.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Maintenace Center</span></h3>
                            <p align="justify"> Interlink provide 28 Operation and Maintenance Centers 
                            nationwide to guarantee on service and ready to support customer whenever its’ need.</p>
                            <p><br></p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->
                
                 <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/icon4.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Product Offering</span></h3>
                            <p align="justify">อินเตอร์ลิ้งค์ เทเลคอม ให้บริการบนพื้นฐานความต้องการของลูกค้า ซึ่งการบริการสามารถปรับเปลี่ยนเพื่อให้เหมาะสมกับความต้องการของลูกค้าได้</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->               
                 
                 <div class="col-sm-6">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="pull-left">
                            <img class="img-responsive" src="images/aboutus/bestVision.png">
                        </div>
                        <div class="media-body">
							<h3 class="media-heading"><span class="orangetext">Best Vision</span></h3>
                            <p align="justify">อินเตอร์ลิ้งค์ เทเลคอม มีจุดมุ่งหมายในการให้บริการ ด้วยวิสัยทัศน์คือ ดีที่สุดของการเชื่อมต่อ ดีที่สุดของการบริการ และดีที่สุดในด้านราคา</p>
                        </div>
                    </div>
                </div><!--/.col-sm-6--> 
                                                                                                                       
                </div><!--/.features-->
            </div><!--/.row--> 

  
 	        	
			<!-- Our Skill -->
			<!--
			<div class="skill-wrap clearfix">
			
				<div class="center wow fadeInDown">
					<h2>Our Skill</h2>
					<p class="lead">"Smart businesses do not look at labor costs alone anymore. They do look at market access, transportation, telecommunications infrastructure and the education and skill level of the workforce, the development of capital and the regulatory market."<br><b>Janet Napolitano</b></p>
				</div>
				
				<div class="row">
		
					<div class="col-sm-3">
						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div class="joomla-skill">                                   
								<p><em>85%</em></p>
								<p>Networking</p>
							</div>
						</div>
					</div>
					
					 <div class="col-sm-3">
						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div class="html-skill">                                  
								<p><em>95%</em></p>
								<p>Fiber Optic</p>
							</div>
						</div>
					</div>
					
					<div class="col-sm-3">
						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">
							<div class="css-skill">                                    
								<p><em>80%</em></p>
								<p>Data Center</p>
							</div>
						</div>
					</div>
					
					 <div class="col-sm-3">
						<div class="sinlge-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms">
							<div class="wp-skill">
								<p><em>90%</em></p>
								<p>Technical Support</p>                                     
							</div>
						</div>
					</div>
					
				</div>
	
            </div><!--/.our-skill-->
			
			</div><!--section-->
</div><!--/.container-->
	

<!-- include header.php -->
		<?php
		$path = $_SERVER['DOCUMENT_ROOT'];
		$path .= "/th/php/footer.php";
		include_once($path) ; 
		?>
<!--/end  php -->
    

    <script src="js/jquery.js"></script>
    <script type="text/javascript">
        $('.carousel').carousel()
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
</body>
</html>
